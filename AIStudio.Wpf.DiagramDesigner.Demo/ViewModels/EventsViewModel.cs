﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using AIStudio.Wpf.Controls;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class EventsViewModel : BaseViewModel
    {
        public EventsViewModel()
        {
            Title = "Events";
            Info = "The current available events are: NodeAdded, NodeRemoved, SelectionChanged, LinkAdded, LinkAttached and LinkRemoved." +
                    "Also, the Diagram and every Model have a Changed event.";

            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.PageSize = new Size(double.NaN, double.NaN);
            DiagramViewModel.ColorViewModel = new ColorViewModel();
            DiagramViewModel.ColorViewModel.FillColor.Color = System.Windows.Media.Colors.Orange;


            DefaultDesignerItemViewModel node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 50, Text = "1" };
            DiagramViewModel.DirectAddItemCommand.Execute(node1);

            DefaultDesignerItemViewModel node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 300, Text = "2" };
            DiagramViewModel.DirectAddItemCommand.Execute(node2);

            DefaultDesignerItemViewModel node3 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 50, Text = "3" };
            DiagramViewModel.DirectAddItemCommand.Execute(node3);

            ConnectionViewModel connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector, DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            DiagramViewModel.DirectAddItemCommand.Execute(connector1);

            ConnectionViewModel connector2 = new ConnectionViewModel(DiagramViewModel, node2.RightConnector, node3.RightConnector, DrawMode.ConnectingLineStraight, RouterMode.RouterOrthogonal);
            DiagramViewModel.DirectAddItemCommand.Execute(connector2);

            DiagramViewModel.ClearSelectedItemsCommand.Execute(null);
            DiagramViewModel.Event += DiagramViewModel_Event;
        }

        private void DiagramViewModel_Event(object sender, DiagramEventArgs e)
        {
            var selectable = sender as SelectableViewModelBase;
            Notice.Show("",
                       $"{sender.GetType().Name}:{selectable?.Id},{e.PropertyName},new:{e.NewValue},old:{e.OldValue}",
                       3,
                       ControlStatus.Mid,
                       NoticeCardStyle.Plain,
                       800,
                       30,
                       HorizontalAlignment.Left,
                       VerticalAlignment.Bottom,
                       false,
                       true,
                       null,
                       "RootWindow");
        }       
    }
}
