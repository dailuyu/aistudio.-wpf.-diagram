﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using AIStudio.Wpf.Flowchart;
using AIStudio.Wpf.Flowchart.ViewModels;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class DragAndDropViewModel: BaseViewModel
    {
        public DragAndDropViewModel()
        {
            Title = "Drag & Drop";
            Info = "A very simple drag & drop of flowchart demo.";

            ToolBoxViewModel = new ToolBoxViewModel();
            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.ShowGrid = true;
            DiagramViewModel.GridCellSize = new Size(100, 100);
            DiagramViewModel.GridMarginSize = new Size(0, 0);
            DiagramViewModel.CellHorizontalAlignment = CellHorizontalAlignment.Center;
            DiagramViewModel.CellVerticalAlignment = CellVerticalAlignment.Center;
            DiagramViewModel.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.PageSize = new Size(double.NaN, double.NaN);

            DiagramViewModel.PropertyChanged += DiagramViewModel_PropertyChanged;
        }

        public ToolBoxViewModel ToolBoxViewModel
        {
            get; private set;
        }

        public SelectableDesignerItemViewModelBase SelectedItem
        {
            get
            {
                return DiagramViewModel.SelectedItems?.FirstOrDefault();
            }
        }

        private List<SelectOption> _users = new List<SelectOption>()
        {
            new SelectOption(){ value = "操作员1",text = "操作员1" },
            new SelectOption(){ value = "操作员2",text = "操作员2" },
            new SelectOption(){ value = "Admin",text = "Admin" },
        };
        public List<SelectOption> Users
        {
            get
            {
                return _users;
            }
            set
            {
                _users = value;
            }
        }

        private List<SelectOption> _roles = new List<SelectOption>()
        {
            new SelectOption(){ value = "操作员",text = "操作员" },
            new SelectOption(){ value = "管理员",text = "管理员" },
        };
        public List<SelectOption> Roles
        {
            get
            {
                return _roles;
            }
            set
            {
                _roles = value;
            }
        }     

        private void DiagramViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsSelected")
            {
                RaisePropertyChanged(nameof(SelectedItem));
            }
        }
    }
}
