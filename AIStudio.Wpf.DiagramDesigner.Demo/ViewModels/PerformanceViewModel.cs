﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class PerformanceViewModel : BaseViewModel
    {
        public PerformanceViewModel()
        {
            Title = "Performance";
            Info = "This diagram contains 100 nodes and 50 links";

            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.PageSize = new Size(1000, 1000);
            DiagramViewModel.ColorViewModel = new ColorViewModel();
            DiagramViewModel.ColorViewModel.FillColor.Color = System.Windows.Media.Colors.Orange;

            for (int r = 0; r < 10; r++)
            {
                for (int c = 0; c < 10; c += 2)
                {
                    var node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 10 + c * 10 + c * 120, Top = 10 + r * 100, Text = $"{r * 10 + c}" };
                    var node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 10 + (c + 1) * 130, Top = 10 + r * 100, Text = $"{r * 10 + c + 1}" };

                    DiagramViewModel.DirectAddItemCommand.Execute(new List<SelectableDesignerItemViewModelBase> { node1, node2 });

                    ConnectionViewModel connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector);
                    DiagramViewModel.DirectAddItemCommand.Execute(connector1);
                }
            }

            DiagramViewModel.ClearSelectedItemsCommand.Execute(null);
        }
    }
}
