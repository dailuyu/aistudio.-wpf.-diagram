﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class MarkersViewModel : BaseViewModel
    {
        public MarkersViewModel()
        {
            Title = "Link Markers";
            Info = "Markers are SVG Paths that you can put at the beginning or at the end of your links.";

            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.PageSize = new Size(double.NaN, double.NaN);
            DiagramViewModel.ColorViewModel = new ColorViewModel();
            DiagramViewModel.ColorViewModel.FillColor.Color = System.Windows.Media.Colors.Orange;

            DefaultDesignerItemViewModel node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 50, Text = "1" };
            DiagramViewModel.DirectAddItemCommand.Execute(node1);

            DefaultDesignerItemViewModel node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 400, Top = 50, Text = "2" };
            DiagramViewModel.DirectAddItemCommand.Execute(node2);

            ConnectionViewModel connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector);
            connector1.ShapeViewModel.SourceMarker = LinkMarker.Arrow;
            connector1.ShapeViewModel.SinkMarker = LinkMarker.Arrow;
            connector1.AddLabel("Arrow");
            DiagramViewModel.DirectAddItemCommand.Execute(connector1);

            node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 160, Text = "1" };
            DiagramViewModel.DirectAddItemCommand.Execute(node1);

            node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 400, Top = 160, Text = "2" };
            DiagramViewModel.DirectAddItemCommand.Execute(node2);

            connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector);
            connector1.ShapeViewModel.SourceMarker = LinkMarker.Circle;
            connector1.ShapeViewModel.SinkMarker = LinkMarker.Circle;
            connector1.AddLabel("Circle");
            DiagramViewModel.DirectAddItemCommand.Execute(connector1);

            node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 270, Text = "1" };
            DiagramViewModel.DirectAddItemCommand.Execute(node1);

            node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 400, Top = 270, Text = "2" };
            DiagramViewModel.DirectAddItemCommand.Execute(node2);

            connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector);
            connector1.ShapeViewModel.SourceMarker = LinkMarker.Square;
            connector1.ShapeViewModel.SinkMarker = LinkMarker.Square;
            connector1.AddLabel("Square");
            DiagramViewModel.DirectAddItemCommand.Execute(connector1);

            node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 380, Text = "1" };
            DiagramViewModel.DirectAddItemCommand.Execute(node1);

            node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 400, Top = 380, Text = "2" };
            DiagramViewModel.DirectAddItemCommand.Execute(node2);

            connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector);
            connector1.ShapeViewModel.SourceMarker = LinkMarker.NewRectangle(10, 20);
            connector1.ShapeViewModel.SinkMarker = LinkMarker.NewArrow(20, 10);
            connector1.AddLabel("Factory");
            DiagramViewModel.DirectAddItemCommand.Execute(connector1);

            node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 490, Text = "1" };
            DiagramViewModel.DirectAddItemCommand.Execute(node1);

            node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 400, Top = 490, Text = "2" };
            DiagramViewModel.DirectAddItemCommand.Execute(node2);

            connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector);
            connector1.ShapeViewModel.SourceMarker = new LinkMarker("M 0 -8 L 3 -8 3 8 0 8 z M 4 -8 7 -8 7 8 4 8 z M 8 -8 16 0 8 8 z", 16, 16, ArrowPathStyle.Arrow, ArrowSizeStyle.ExtraLarge);
            connector1.ShapeViewModel.SinkMarker = new LinkMarker("M 0 -8 L 8 -8 4 0 8 8 0 8 4 0 z", 8, 8, ArrowPathStyle.Arrow, ArrowSizeStyle.Small);
            connector1.AddLabel("Custom");
            DiagramViewModel.DirectAddItemCommand.Execute(connector1);

            DiagramViewModel.ClearSelectedItemsCommand.Execute(null);

        }
    }
}
