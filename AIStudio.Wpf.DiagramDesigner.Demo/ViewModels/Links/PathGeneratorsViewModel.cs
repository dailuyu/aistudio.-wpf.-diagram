﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class PathGeneratorsViewModel : BaseViewModel
    {
        public PathGeneratorsViewModel()
        {
            Title = "Link Path Generators";
            Info = "Path generators are functions that take as input the calculated route and output SVG paths, " +
                "alongside the markers positions and their angles. There are currently two generators: Straight and Smooth.";

            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.PageSize = new Size(double.NaN, double.NaN);
            DiagramViewModel.CellHorizontalAlignment = CellHorizontalAlignment.Center;
            DiagramViewModel.CellVerticalAlignment = CellVerticalAlignment.Center;
            DiagramViewModel.ColorViewModel = new ColorViewModel();
            DiagramViewModel.ColorViewModel.FillColor.Color = System.Windows.Media.Colors.Orange;

            DefaultDesignerItemViewModel node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 80, Text = "1" };
            DiagramViewModel.DirectAddItemCommand.Execute(node1);

            DefaultDesignerItemViewModel node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 100, Top = 300, Text = "2" };
            DiagramViewModel.DirectAddItemCommand.Execute(node2);

            DefaultDesignerItemViewModel node3 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 200, Top = 80, Text = "3" };
            DiagramViewModel.DirectAddItemCommand.Execute(node3);

            ConnectionViewModel connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector, DrawMode.ConnectingLineStraight, RouterMode.RouterNormal);
            connector1.AddLabel("Straight");
            DiagramViewModel.DirectAddItemCommand.Execute(connector1);

            ConnectionViewModel connector2 = new ConnectionViewModel(DiagramViewModel, node2.RightConnector, node3.LeftConnector, DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            connector2.AddLabel("Smooth");
            DiagramViewModel.DirectAddItemCommand.Execute(connector2);

            DefaultDesignerItemViewModel node4 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 450, Top = 300, Text = "4" };
            DiagramViewModel.DirectAddItemCommand.Execute(node4);

            DefaultDesignerItemViewModel node5 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 550, Top = 80, Text = "5" };
            DiagramViewModel.DirectAddItemCommand.Execute(node5);

            ConnectionViewModel connector3 = new ConnectionViewModel(DiagramViewModel, node3.RightConnector, node4.LeftConnector, DrawMode.ConnectingLineBoundary, RouterMode.RouterNormal);
            connector3.AddLabel("Boundary");
            DiagramViewModel.DirectAddItemCommand.Execute(connector3);

            ConnectionViewModel connector4 = new ConnectionViewModel(DiagramViewModel, node4.RightConnector, node5.LeftConnector, DrawMode.ConnectingLineCorner, RouterMode.RouterNormal);
            connector4.AddLabel("Corner");
            DiagramViewModel.DirectAddItemCommand.Execute(connector4);

            DiagramViewModel.ClearSelectedItemsCommand.Execute(null);
        }
    }
}
