﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class LabelsViewModel : BaseViewModel
    {
        public LabelsViewModel()
        {
            Title = "Link Labels";
            Info = "Labels help you show more information through out a link. You can specify a distance or an offset." +
                "The content of the labels is still limited because of Blazor's poor SVG support.";
           
            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.PageSize = new Size(double.NaN, double.NaN);
            DiagramViewModel.ColorViewModel = new ColorViewModel();
            DiagramViewModel.ColorViewModel.FillColor.Color = System.Windows.Media.Colors.Orange;

            DefaultDesignerItemViewModel node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 50, Text = "1" };
            DiagramViewModel.DirectAddItemCommand.Execute(node1);

            DefaultDesignerItemViewModel node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 400, Top = 50, Text = "2" };
            DiagramViewModel.DirectAddItemCommand.Execute(node2);

            ConnectionViewModel connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector);
            connector1.AddLabel("Content");
            DiagramViewModel.DirectAddItemCommand.Execute(connector1);

            node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 160, Text = "1" };
            DiagramViewModel.DirectAddItemCommand.Execute(node1);

            node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 400, Top = 160, Text = "2" };
            DiagramViewModel.DirectAddItemCommand.Execute(node2);

            connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector);
            connector1.AddLabel("0.25", 0.3);
            connector1.AddLabel("0.75", 0.7);
            DiagramViewModel.DirectAddItemCommand.Execute(connector1);

            node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 270, Text = "1" };
            DiagramViewModel.DirectAddItemCommand.Execute(node1);

            node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 400, Top = 270, Text = "2" };
            DiagramViewModel.DirectAddItemCommand.Execute(node2);

            connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector);
            connector1.AddLabel("50", 50);
            connector1.AddLabel("-50", -50);
            DiagramViewModel.DirectAddItemCommand.Execute(connector1);

            node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 380, Text = "1" };
            DiagramViewModel.DirectAddItemCommand.Execute(node1);

            node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 400, Top = 380, Text = "2" };
            DiagramViewModel.DirectAddItemCommand.Execute(node2);

            connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector);
            connector1.AddLabel("(0,-20)", 50, new Point(0, -20));
            connector1.AddLabel("(0,20)", -50, new Point(0, 20));
            DiagramViewModel.DirectAddItemCommand.Execute(connector1);

            DiagramViewModel.ClearSelectedItemsCommand.Execute(null);
        }
    }
}
