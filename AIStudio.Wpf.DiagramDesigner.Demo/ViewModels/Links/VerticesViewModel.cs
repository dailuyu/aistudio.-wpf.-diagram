﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class VerticesViewModel : BaseViewModel
    {
        public VerticesViewModel()
        {
            Title = "Link Vertices";
            Info = "Click on a link to create a vertex. Double click on a vertex to delete it. " +
                "You can drag the vertices around.";
         
            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.PageSize = new Size(double.NaN, double.NaN);
            DiagramViewModel.ColorViewModel = new ColorViewModel();
            DiagramViewModel.ColorViewModel.FillColor.Color = System.Windows.Media.Colors.Orange;

            DefaultDesignerItemViewModel node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 80, Text = "1" };
            DiagramViewModel.DirectAddItemCommand.Execute(node1);

            DefaultDesignerItemViewModel node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 200, Top = 350, Text = "2" };
            DiagramViewModel.DirectAddItemCommand.Execute(node2);

            DefaultDesignerItemViewModel node3 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 400, Top = 100, Text = "3" };
            DiagramViewModel.DirectAddItemCommand.Execute(node3);

            ConnectionViewModel connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector, DrawMode.ConnectingLineStraight, RouterMode.RouterNormal);
            connector1.AddLabel("Content");
            connector1.AddVertex(new Point(221, 112.5));
            connector1.AddVertex(new Point(111, 291));
            DiagramViewModel.DirectAddItemCommand.Execute(connector1);

            ConnectionViewModel connector2 = new ConnectionViewModel(DiagramViewModel, node2.RightConnector, node3.LeftConnector, DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            connector2.AddLabel("Content");
            connector2.AddVertex(new Point(400, 324));
            connector2.AddVertex(new Point(326, 180));
            DiagramViewModel.DirectAddItemCommand.Execute(connector2);


            DiagramViewModel.ClearSelectedItemsCommand.Execute(null);
        }
    }
}
