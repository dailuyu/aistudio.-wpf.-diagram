﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using AIStudio.Wpf.DiagramDesigner.Additionals.Extensions.ViewModels;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels.Texts
{
    class OutlineTextViewModel : BaseViewModel
    {
        public OutlineTextViewModel()
        {
            Title = "OutlineText";
            Info = "A text node of AIStudio.Wpf.DiagramDesigner.";

            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.PageSize = new Size(double.NaN, double.NaN);
            DiagramViewModel.ColorViewModel = new ColorViewModel();

            OutLineTextDesignerItemViewModel node1 = new OutLineTextDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 50, ItemWidth = 600, ItemHeight = 100, Text = "竹外桃花三两枝，春江水暖鸭先知。\r\n蒌蒿满地芦芽短，正是河豚欲上时。" };
            DiagramViewModel.DirectAddItemCommand.Execute(node1);

            OutLineTextDesignerItemViewModel node2 = new OutLineTextDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 160, ItemWidth = 500, ItemHeight = 100, Text = "解落三秋叶，能开二月花。\r\n过江千尺浪，入竹万竿斜。" };
            DiagramViewModel.DirectAddItemCommand.Execute(node2);

            OutLineTextDesignerItemViewModel node3 = new OutLineTextDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 270, ItemWidth = 500, ItemHeight = 100, Text = "一节复一节，千枝攒万叶。\r\n我自不开花，免撩蜂与蝶。" };
            DiagramViewModel.DirectAddItemCommand.Execute(node3);

            OutLineTextDesignerItemViewModel node4 = new OutLineTextDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 380, ItemWidth = 500, ItemHeight = 100, Text = "独坐幽篁里，弹琴复长啸。\r\n深林人不知，明月来相照。" };
            DiagramViewModel.DirectAddItemCommand.Execute(node4);

            DiagramViewModel.ClearSelectedItemsCommand.Execute(null);
        }
    }
}
