﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels.Texts
{
    class FontSizeViewModel : BaseViewModel
    {
        public FontSizeViewModel()
        {
            Title = "FontSize";
            Info = "A text node of AIStudio.Wpf.DiagramDesigner.";

            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.PageSize = new Size(double.NaN, double.NaN);
            DiagramViewModel.ColorViewModel = new ColorViewModel();

            TextDesignerItemViewModel node1 = new TextDesignerItemViewModel(DiagramViewModel) { Left = 30, Top = 50, ItemWidth = 260, Text = "竹外桃花三两枝，春江水暖鸭先知。\r\n蒌蒿满地芦芽短，正是河豚欲上时。" };
            node1.FontViewModel.FontSize = 15;
            DiagramViewModel.DirectAddItemCommand.Execute(node1);

            TextDesignerItemViewModel node2 = new TextDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 50, ItemWidth = 200, Text = "解落三秋叶，能开二月花。\r\n过江千尺浪，入竹万竿斜。" };
            node2.FontViewModel.FontWeight = FontWeights.Bold;
            DiagramViewModel.DirectAddItemCommand.Execute(node2);

            TextDesignerItemViewModel node3 = new TextDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 150, ItemWidth = 200, Text = "一节复一节，千枝攒万叶。\r\n我自不开花，免撩蜂与蝶。" };
            node3.FontViewModel.FontStretch = FontStretches.Expanded;
            DiagramViewModel.DirectAddItemCommand.Execute(node3);

            TextDesignerItemViewModel node4 = new TextDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 150, ItemWidth = 200, Text = "独坐幽篁里，弹琴复长啸。\r\n深林人不知，明月来相照。" };
            node4.FontViewModel.FontFamily = "宋体";
            DiagramViewModel.DirectAddItemCommand.Execute(node4);

            DiagramViewModel.ClearSelectedItemsCommand.Execute(null);
        }
    }
}