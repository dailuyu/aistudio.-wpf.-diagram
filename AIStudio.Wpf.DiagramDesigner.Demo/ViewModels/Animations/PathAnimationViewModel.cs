﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class PathAnimationViewModel : BaseViewModel
    {
        public PathAnimationViewModel()
        {
            Title = "Path Animation";
            Info = "Line flow motion animation.";

            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.PageSize = new Size(double.NaN, double.NaN);
            DiagramViewModel.ColorViewModel = new ColorViewModel();
            DiagramViewModel.ColorViewModel.FillColor.Color = System.Windows.Media.Colors.Orange;

            DefaultDesignerItemViewModel node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 50, Text = "1" };
            DiagramViewModel.DirectAddItemCommand.Execute(node1);

            DefaultDesignerItemViewModel node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 300, Text = "2" };
            DiagramViewModel.DirectAddItemCommand.Execute(node2);

            DefaultDesignerItemViewModel node3 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 50, Text = "3" };
            DiagramViewModel.DirectAddItemCommand.Execute(node3);

            ConnectionViewModel connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector, DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            connector1.ColorViewModel.FillColor.Color = Colors.Red; 
            connector1.ColorViewModel.LineAnimation = LineAnimation.PathAnimation;
            DiagramViewModel.DirectAddItemCommand.Execute(connector1);

            ConnectionViewModel connector2 = new ConnectionViewModel(DiagramViewModel, node2.RightConnector, node3.RightConnector, DrawMode.ConnectingLineStraight, RouterMode.RouterOrthogonal);
            connector2.ColorViewModel.FillColor.Color = Colors.Red; 
            connector2.ColorViewModel.LineAnimation = LineAnimation.PathAnimation;
            DiagramViewModel.DirectAddItemCommand.Execute(connector2);

            DiagramViewModel.ClearSelectedItemsCommand.Execute(null);
        }
    }
}
