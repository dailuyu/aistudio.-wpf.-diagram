﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Algorithms;
using AIStudio.Wpf.DiagramDesigner.Geometrys;
using AIStudio.Wpf.DiagramDesigner.Helpers;
using AIStudio.Wpf.Mind.ViewModels;

namespace AIStudio.Wpf.Mind.Helpers
{
    public class FishBoneLayout : IMindLayout
    {
        public void Appearance(MindNode mindNode)
        {
            if (mindNode == null) return;

            mindNode.GetLevel1Node().LayoutUpdating = true;

            switch (mindNode.NodeLevel)
            {
                case 0:
                    {
                        mindNode.ItemWidth = 110;
                        mindNode.ItemHeight = 40;
                        mindNode.ClearConnectors();

                        var port = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.Right, true) { XRatio = 1, YRatio = 0.5 };
                        mindNode.AddConnector(port);

                        mindNode.IsInnerConnector = true;

                        mindNode.ColorViewModel.FillColor.Color = Color.FromRgb(0x73, 0xa1, 0xbf);
                        mindNode.ColorViewModel.LineColor.Color = Color.FromRgb(0x73, 0xa1, 0xbf);
                        mindNode.FontViewModel.FontColor = Colors.White;
                        mindNode.FontViewModel.FontSize = 15;
                        mindNode.Spacing = new SizeBase(50, 15);
                        mindNode.ShapeViewModel.SinkMarker.PathStyle = ArrowPathStyle.None;
                        mindNode.ShapeViewModel.SinkMarker.SizeStyle = ArrowSizeStyle.VerySmall;

                        mindNode.ConnectorOrientation = ConnectorOrientation.None;
                        break;
                    }
                case 1:
                    {
                        mindNode.ItemWidth = 80;
                        mindNode.ItemHeight = 25;
                        mindNode.ClearConnectors();

                        var port1 = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.BottomLeft, true) { XRatio = 0, YRatio = 1 };
                        mindNode.AddConnector(port1);

                        var port2 = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.TopLeft, true) { XRatio = 0, YRatio = 0 };
                        mindNode.AddConnector(port2);

                        mindNode.IsInnerConnector = true;

                        mindNode.ColorViewModel.LineColor.Color = Color.FromRgb(0x73, 0xa1, 0xbf);
                        mindNode.ShapeViewModel.SinkMarker.PathStyle = ArrowPathStyle.None;
                        mindNode.ShapeViewModel.SinkMarker.SizeStyle = ArrowSizeStyle.VerySmall;
                        break;
                    }
                default:
                    {
                        mindNode.ItemWidth = 80;
                        mindNode.ItemHeight = 25;
                        mindNode.ClearConnectors();

                        var port1 = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.Left, true) { XRatio = 0, YRatio = 0.5 };
                        mindNode.AddConnector(port1);
                        var port2 = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.Bottom, true) { XRatio = 0.25, YRatio = 1 };
                        mindNode.AddConnector(port2);
                        var port3 = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.Top, true) { XRatio = 0.25, YRatio = 0 };
                        mindNode.AddConnector(port3);

                        mindNode.IsInnerConnector = true;
                        mindNode.ColorViewModel.FillColor.Color = Colors.Transparent;
                        mindNode.ColorViewModel.LineColor.Color = Color.FromRgb(0x73, 0xa1, 0xbf);
                        mindNode.ShapeViewModel.SinkMarker.PathStyle = ArrowPathStyle.None;
                        mindNode.ShapeViewModel.SinkMarker.SizeStyle = ArrowSizeStyle.VerySmall;
                        mindNode.CornerRadius = new System.Windows.CornerRadius(0);
                        mindNode.BorderThickness = new System.Windows.Thickness(0, 0, 0, 0);
                        break;
                    }
            }

            mindNode.GetLevel1Node().LayoutUpdating = false;
        }

        public ConnectionViewModel GetOrSetConnectionViewModel(MindNode source, MindNode sink, ConnectionViewModel connector = null)
        {
            DrawMode drawMode;
            RouterMode routerMode;
            if (source.NodeLevel == 0)
            {
                drawMode = DrawMode.ConnectingLineStraight;
                routerMode = RouterMode.RouterFishBone;
            }
            else if (source.NodeLevel == 1)
            {
                drawMode = DrawMode.ConnectingLineStraight;
                routerMode = RouterMode.RouterNormal;
            }
            else
            {
                drawMode = DrawMode.ConnectingLineStraight;
                routerMode = RouterMode.RouterOrthogonal;
            }

            if (connector == null)
            {
                connector = new ConnectionViewModel(source.Root, source.FirstConnector, sink.FirstConnector, drawMode, routerMode);
            }
            else
            {
                connector?.SetSourcePort(source.FirstConnector);
                connector?.SetSinkPort(sink.FirstConnector);
                connector.PathMode = drawMode.ToString();
                connector.RouterMode = routerMode.ToString();
            }
            connector.ColorViewModel.LineColor = source.ColorViewModel.LineColor;
            connector.SmoothMargin = 20;
            connector.SmoothAutoSlope = 0.2;
            connector.ShapeViewModel.SinkMarker.PathStyle = source.ShapeViewModel.SinkMarker.PathStyle;
            connector.ShapeViewModel.SinkMarker.SizeStyle = source.ShapeViewModel.SinkMarker.SizeStyle;

            return connector;
        }

        public void LayoutUpdated(MindNode mindNode)
        {
            if (mindNode == null) return;

            mindNode.GetLevel1Node().LayoutUpdating = true;
            var size = MeasureOverride(mindNode);
            ArrangeOverride(mindNode);

            mindNode.Root.BringToFrontCommand.Execute(mindNode.Root.Items.OfType<DesignerItemViewModelBase>());

            mindNode.GetLevel1Node().LayoutUpdating = false;
        }

        public SizeBase MeasureOverride(MindNode mindNode, bool isExpanded = true)
        {
            var sizewithSpacing = mindNode.SizeWithSpacing;
            var bottomoffset = mindNode.Spacing.Width / 2;
            if (mindNode.Children?.Count > 0)
            {
                if (mindNode.NodeLevel == 0)
                {
                    var tops = mindNode.Children.Where((p, index) => index % 2 == 0).ToList();
                    tops.ForEach(p => p.ConnectorOrientation = ConnectorOrientation.BottomLeft);
                    var topsizes = tops.Select(p => MeasureOverride(p, mindNode.IsExpanded && isExpanded)).ToArray();
                    var bottoms = mindNode.Children.Where((p, index) => index % 2 == 1).ToList();
                    bottoms.ForEach(p => p.ConnectorOrientation = ConnectorOrientation.TopLeft);
                    var bottomsizes = bottoms.Select(p => MeasureOverride(p, mindNode.IsExpanded && isExpanded)).ToArray();
                    sizewithSpacing = new SizeBase(sizewithSpacing.Width + bottomoffset + Math.Max(topsizes.SumOrDefault(p => p.Width), bottomsizes.SumOrDefault(p => p.Width)), sizewithSpacing.Height + topsizes.MaxOrDefault(p => p.Height) + bottomsizes.MaxOrDefault(p => p.Height));
                }
                else if (mindNode.NodeLevel == 1)
                {
                    var childrensizes = mindNode.Children.Select(p => MeasureOverride(p, mindNode.IsExpanded && isExpanded)).ToArray();
                    var lastchildsize = childrensizes.LastOrDefault();
                    sizewithSpacing = new SizeBase(Math.Max(sizewithSpacing.Width, sizewithSpacing.Height + childrensizes.SumOrDefault(p => p.Height) - lastchildsize.Height / 2 + lastchildsize.Width), sizewithSpacing.Height + childrensizes.SumOrDefault(p => p.Height));
                }
                else
                {
                    var childrensizes = mindNode.Children.Select(p => MeasureOverride(p, mindNode.IsExpanded && isExpanded)).ToArray();
                    sizewithSpacing = new SizeBase(Math.Max(sizewithSpacing.Width, sizewithSpacing.Width * 0.5 + childrensizes.MaxOrDefault(p => p.Width)), sizewithSpacing.Height + childrensizes.SumOrDefault(p => p.Height));
                }
            }
            mindNode.DesiredSize = isExpanded ? sizewithSpacing : new SizeBase(0, 0);
            mindNode.Visible = isExpanded;

            return mindNode.DesiredSize;
        }

        public void ArrangeOverride(MindNode mindNode)
        {
            if (mindNode.NodeLevel == 0)
            {
                var tops = mindNode.Children.Where(p => p.ConnectorOrientation == ConnectorOrientation.BottomLeft).ToList();
                double topleft = mindNode.MiddlePosition.X + mindNode.ItemWidth / 2 + mindNode.Spacing.Width;
                double toptop = mindNode.MiddlePosition.Y - mindNode.ItemHeight / 2 - mindNode.Spacing.Height;

                if (mindNode.Children?.Count > 0)
                {
                    foreach (var child in tops)
                    {
                        child.Left = topleft + child.Spacing.Width + child.Offset.X;
                        child.Top = toptop - child.ItemHeight - child.Spacing.Height + child.Offset.Y;
                        child.DesiredPosition = child.Position;
                        topleft += child.DesiredSize.Width;

                        ArrangeOverride(child);

                        var connector = mindNode.Root?.Items.OfType<ConnectionViewModel>().FirstOrDefault(p => p.SourceConnectorInfo?.DataItem == mindNode && p.SinkConnectorInfoFully?.DataItem == child);
                        connector?.SetSourcePort(mindNode.RightConnector);
                        connector?.SetSinkPort(child.BottomLeftConnector);
                        connector?.SetVisible(child.Visible);
                    }
                }

                var bottomoffset = mindNode.Spacing.Width / 2;
                var bottoms = mindNode.Children.Where(p => p.ConnectorOrientation == ConnectorOrientation.TopLeft).ToList();
                double bottomleft = mindNode.MiddlePosition.X + mindNode.ItemWidth / 2 + mindNode.Spacing.Width + bottomoffset;
                double bottomtop = mindNode.MiddlePosition.Y + mindNode.ItemHeight / 2 + mindNode.Spacing.Height;

                if (mindNode.Children?.Count > 0)
                {
                    foreach (var child in bottoms)
                    {
                        child.Left = bottomleft + child.Spacing.Width + child.Offset.X;
                        child.Top = bottomtop + child.Spacing.Height + child.Offset.Y;
                        child.DesiredPosition = child.Position;
                        bottomleft += child.DesiredSize.Width;

                        ArrangeOverride(child);

                        var connector = mindNode.Root?.Items.OfType<ConnectionViewModel>().FirstOrDefault(p => p.SourceConnectorInfo?.DataItem == mindNode && p.SinkConnectorInfoFully?.DataItem == child);
                        connector?.SetSourcePort(mindNode.RightConnector);
                        connector?.SetSinkPort(child.TopLeftConnector);
                        connector?.SetVisible(child.Visible);
                    }
                }
            }
            else if (mindNode.NodeLevel == 1)
            {
                if (mindNode.ConnectorOrientation == ConnectorOrientation.BottomLeft)
                {
                    double x0 = mindNode.Left;
                    double y0 = mindNode.Top + mindNode.ItemHeight;
                    double h = mindNode.ItemHeight + mindNode.Spacing.Height;
                    if (mindNode.Children?.Count > 0)
                    {
                        foreach (var child in mindNode.Children)
                        {
                            child.Left = x0 + (h + child.DesiredSize.Height - child.ItemHeight / 2 - child.Spacing.Height) + child.Offset.X;
                            child.Top = y0 - (h + child.DesiredSize.Height - child.Spacing.Height) + child.Offset.Y;
                            child.DesiredPosition = child.Position;
                            h += child.DesiredSize.Height;

                            ArrangeOverride(child);

                            var connector = mindNode.Root?.Items.OfType<ConnectionViewModel>().FirstOrDefault(p => p.SourceConnectorInfo?.DataItem == mindNode && p.SinkConnectorInfoFully?.DataItem == child);
                            connector?.SetSourcePort(mindNode.BottomLeftConnector);
                            connector?.SetSinkPort(child.LeftConnector);
                            connector?.SetVisible(child.Visible);
                        }
                    }
                }
                else
                {
                    double x0 = mindNode.Left;
                    double y0 = mindNode.Top;
                    double h = mindNode.ItemHeight + mindNode.Spacing.Height;
                    if (mindNode.Children?.Count > 0)
                    {
                        foreach (var child in mindNode.Children)
                        {
                            child.Left = x0 + (h + child.DesiredSize.Height - child.ItemHeight / 2 - child.Spacing.Height) + child.Offset.X;
                            child.Top = y0 + (h + child.DesiredSize.Height - child.ItemHeight - child.Spacing.Height) + child.Offset.Y;
                            child.DesiredPosition = child.Position;
                            h += child.DesiredSize.Height;

                            ArrangeOverride(child);

                            var connector = mindNode.Root?.Items.OfType<ConnectionViewModel>().FirstOrDefault(p => p.SourceConnectorInfo?.DataItem == mindNode && p.SinkConnectorInfoFully?.DataItem == child);
                            connector?.SetSourcePort(mindNode.TopLeftConnector);
                            connector?.SetSinkPort(child.LeftConnector);
                            connector?.SetVisible(child.Visible);
                        }
                    }
                }
            }
            else
            {
                if (mindNode.GetLevel2Node().ConnectorOrientation == ConnectorOrientation.BottomLeft)
                {
                    double left = mindNode.MiddlePosition.X;
                    double top = mindNode.MiddlePosition.Y + mindNode.ItemHeight / 2 + mindNode.Spacing.Height;
                    if (mindNode.Children?.Count > 0)
                    {
                        foreach (var child in mindNode.Children)
                        {
                            child.Left = left + child.Offset.X;
                            child.Top = top + child.Spacing.Height + child.Offset.Y;
                            child.DesiredPosition = child.Position;
                            top += child.DesiredSize.Height;

                            ArrangeOverride(child);

                            var connector = mindNode.Root?.Items.OfType<ConnectionViewModel>().FirstOrDefault(p => p.SourceConnectorInfo?.DataItem == mindNode && p.SinkConnectorInfoFully?.DataItem == child);
                            connector?.SetSourcePort(mindNode.BottomConnector);
                            connector?.SetSinkPort(child.LeftConnector);
                            connector?.SetVisible(child.Visible);
                        }
                    }
                }
                else
                {
                    double left = mindNode.MiddlePosition.X;
                    double bottom = mindNode.MiddlePosition.Y - mindNode.ItemHeight / 2 - mindNode.Spacing.Height;
                    if (mindNode.Children?.Count > 0)
                    {
                        foreach (var child in mindNode.Children)
                        {
                            child.Left = left + child.Offset.X;
                            child.Top = bottom - child.Spacing.Height - child.ItemHeight + child.Offset.Y;
                            child.DesiredPosition = child.Position;
                            bottom -= child.DesiredSize.Height;

                            ArrangeOverride(child);

                            var connector = mindNode.Root?.Items.OfType<ConnectionViewModel>().FirstOrDefault(p => p.SourceConnectorInfo?.DataItem == mindNode && p.SinkConnectorInfoFully?.DataItem == child);
                            connector?.SetSourcePort(mindNode.TopConnector);
                            connector?.SetSinkPort(child.LeftConnector);
                            connector?.SetVisible(child.Visible);
                        }
                    }
                }
            }
        }
    }

}
