﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Algorithms;
using AIStudio.Wpf.DiagramDesigner.Geometrys;
using AIStudio.Wpf.DiagramDesigner.Helpers;
using AIStudio.Wpf.Mind.ViewModels;

namespace AIStudio.Wpf.Mind.Helpers
{
    public class LogicalLayout : IMindLayout
    {
        public void Appearance(MindNode mindNode)
        {
            switch (mindNode.NodeLevel)
            {
                case 0:
                    {
                        mindNode.ItemWidth = 110;
                        mindNode.ItemHeight = 40;
                        mindNode.ClearConnectors();

                        var port = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.None, true) { XRatio = 0.5, YRatio = 0.5 };
                        mindNode.AddConnector(port);

                        mindNode.IsInnerConnector = true;

                        mindNode.ColorViewModel.FillColor.Color = Color.FromRgb(0x73, 0xa1, 0xbf);
                        mindNode.ColorViewModel.LineColor.Color = Color.FromRgb(0x73, 0xa1, 0xbf);
                        mindNode.FontViewModel.FontColor = Colors.White;
                        mindNode.FontViewModel.FontSize = 15;
                        mindNode.Spacing = new SizeBase(50, 15);
                        mindNode.ShapeViewModel.SinkMarker.PathStyle = ArrowPathStyle.Circle;
                        mindNode.ShapeViewModel.SinkMarker.SizeStyle = ArrowSizeStyle.VerySmall;

                        mindNode.ConnectorOrientation = ConnectorOrientation.None;
                        break;
                    }
                case 1:
                    {
                        mindNode.ItemWidth = 80;
                        mindNode.ItemHeight = 25;
                        mindNode.ClearConnectors();
                        var port1 = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.Left, true) { XRatio = 0, YRatio = 0.5 };
                        mindNode.AddConnector(port1);
                        var port2 = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.Right, true) { XRatio = 1, YRatio = 0.5 };
                        mindNode.AddConnector(port2);

                        mindNode.IsInnerConnector = true;

                        mindNode.ColorViewModel.LineColor.Color = Color.FromRgb(0x73, 0xa1, 0xbf);
                        mindNode.ShapeViewModel.SinkMarker.PathStyle = ArrowPathStyle.None;
                        mindNode.ShapeViewModel.SinkMarker.SizeStyle = ArrowSizeStyle.VerySmall;
                        break;
                    }
                case 2:
                    {
                        mindNode.ItemWidth = 80;
                        mindNode.ItemHeight = 25;
                        mindNode.ClearConnectors();

                        var port1 = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.Left, true) { XRatio = 0, YRatio = 1 };
                        mindNode.AddConnector(port1);
                        var port2 = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.Right, true) { XRatio = 1, YRatio = 1 };
                        mindNode.AddConnector(port2);

                        mindNode.IsInnerConnector = true;
                        mindNode.ColorViewModel.LineColor.Color = Color.FromRgb(0x73, 0xa1, 0xbf);
                        mindNode.ShapeViewModel.SinkMarker.PathStyle = ArrowPathStyle.None;
                        mindNode.ShapeViewModel.SinkMarker.SizeStyle = ArrowSizeStyle.VerySmall;
                        mindNode.CornerRadius = new System.Windows.CornerRadius(0);
                        mindNode.BorderThickness = new System.Windows.Thickness(0, 0, 0, 1);
                        break;
                    }
            }
        }
        public ConnectionViewModel GetOrSetConnectionViewModel(MindNode source, MindNode sink, ConnectionViewModel connector = null)
        {
            if (connector == null)
            {
                connector = new ConnectionViewModel(source.Root, source.FirstConnector, sink.FirstConnector, DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            }
            else
            {
                connector?.SetSourcePort(source.FirstConnector);
                connector?.SetSinkPort(sink.FirstConnector);
                connector.PathMode = DrawMode.ConnectingLineSmooth.ToString();
                connector.RouterMode = RouterMode.RouterNormal.ToString();
            }
            connector.ColorViewModel.LineColor = source.ColorViewModel.LineColor;
            connector.SmoothMargin = 20;
            connector.SmoothAutoSlope = 0.2;
            connector.ShapeViewModel.SinkMarker.PathStyle = source.ShapeViewModel.SinkMarker.PathStyle;
            connector.ShapeViewModel.SinkMarker.SizeStyle = source.ShapeViewModel.SinkMarker.SizeStyle;

            return connector;
        }

        public void LayoutUpdated(MindNode mindNode)
        {
            if (mindNode == null) return;

            mindNode.GetLevel1Node().LayoutUpdating = true;
            var size = MeasureOverride(mindNode);
            ArrangeOverride(mindNode);

            mindNode.Root.BringToFrontCommand.Execute(mindNode.Root.Items.OfType<DesignerItemViewModelBase>());

            mindNode.GetLevel1Node().LayoutUpdating = false;
        }

        public SizeBase MeasureOverride(MindNode mindNode, bool isExpanded = true)
        {
            var sizewithSpacing = mindNode.SizeWithSpacing;
            if (mindNode.Children?.Count > 0)
            {
                var childrensizes = mindNode.Children.Select(p => MeasureOverride(p, mindNode.IsExpanded && isExpanded)).ToArray();
                sizewithSpacing = new SizeBase(sizewithSpacing.Width + childrensizes.MaxOrDefault(p => p.Width), Math.Max(sizewithSpacing.Height, childrensizes.SumOrDefault(p => p.Height)));
            }
            mindNode.DesiredSize = isExpanded ? sizewithSpacing : new SizeBase(0, 0);
            mindNode.Visible = isExpanded;

            return mindNode.DesiredSize;
        }

        public void ArrangeOverride(MindNode mindNode)
        {
            double left = mindNode.MiddlePosition.X + mindNode.ItemWidth / 2 + mindNode.Spacing.Width;
            double top = mindNode.MiddlePosition.Y - Math.Min(mindNode.DesiredSize.Height, mindNode.Children.SumOrDefault(p => p.DesiredSize.Height)) / 2;
            if (mindNode.Children?.Count > 0)
            {
                foreach (var child in mindNode.Children)
                {
                    child.Left = left + child.Spacing.Width + child.Offset.X;
                    child.Top = top + child.DesiredSize.Height / 2 - child.ItemHeight / 2 + child.Offset.Y;
                    child.DesiredPosition = child.Position;
                    top += child.DesiredSize.Height;

                    ArrangeOverride(child);

                    var connector = mindNode.Root?.Items.OfType<ConnectionViewModel>().FirstOrDefault(p => p.SourceConnectorInfo?.DataItem == mindNode && p.SinkConnectorInfoFully?.DataItem == child);
                    connector?.SetSourcePort(mindNode.RightConnector ?? mindNode.FirstConnector);
                    connector?.SetSinkPort(child.LeftConnector);
                    connector?.SetVisible(child.Visible);
                }
            }

        }
    }
}
