﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows;
using System.Xml.Linq;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Geometrys;
using AIStudio.Wpf.Mind.ViewModels;

namespace AIStudio.Wpf.Mind.Controls
{
    /// <summary>
    /// MindEditor.xaml 的交互逻辑
    /// </summary>
    [TemplatePart(Name = PART_DiagramControl, Type = typeof(DiagramControl))]
    public partial class MindEditor : UserControl
    {
        public const string PART_DiagramControl = "PART_DiagramControl";
        private DiagramControl _diagramControl;

        private MindDiagramViewModel _diagramViewModel;

        static MindEditor()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MindEditor), new FrameworkPropertyMetadata(typeof(MindEditor)));
        }

        public MindEditor()
        {
            _diagramViewModel = new MindDiagramViewModel();
            _diagramViewModel.GridMarginSize = new Size(0, 0);
            _diagramViewModel.PageSizeType = PageSizeType.Custom;
            _diagramViewModel.PageSize = new SizeBase(double.NaN, double.NaN);           

            _diagramViewModel.PropertyChanged += DiagramViewModel_PropertyChanged;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _diagramControl = GetTemplateChild(PART_DiagramControl) as DiagramControl;
            _diagramControl.HorizontalAlignment = HorizontalAlignment.Stretch;
            _diagramControl.VerticalAlignment = VerticalAlignment.Stretch;
            this.DataContext = _diagramViewModel;

            GetDataFunc = GetData;
        }

        private void DiagramViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsSelected")
            {
                SelectedObject = _diagramViewModel.SelectedItems?.FirstOrDefault();
            }
        }

        //一点要绑定不为空的FlowchartModel才能用，即便为空的也要new一个再来绑定
        public static readonly DependencyProperty DataProperty =
          DependencyProperty.Register(nameof(Data),
                                     typeof(string),
                                     typeof(MindEditor),
                                     new FrameworkPropertyMetadata(null, OnDataChanged));

        public string Data
        {
            get
            {
                return (string)GetValue(DataProperty);
            }
            set
            {
                SetValue(DataProperty, value);
            }
        }
        private static void OnDataChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var view = d as MindEditor;
            var json = e.NewValue as string;
            if (json != null)
            {
                view.CreateFlowchartModel(json);
            }
        }

        private void CreateFlowchartModel(string json)
        {
            //_diagramViewModel.IsLoading = true;
            //_diagramViewModel.Items.Clear();
            //MindNode level1node = new MindNode(_diagramViewModel) { Text = "思维导图" };
            //_diagramViewModel.DirectAddItemCommand.Execute(level1node);
            //level1node.Left = 200;
            //level1node.Top = 200;
            //_diagramViewModel.DoCommandManager.Init();
            //_diagramViewModel.IsLoading = false;
        }

        public static readonly DependencyProperty GetDataFuncProperty =
        DependencyProperty.Register(nameof(GetDataFunc),
                                   typeof(Func<string>),
                                   typeof(MindEditor),
                                   new FrameworkPropertyMetadata(null));
        public Func<string> GetDataFunc
        {
            get
            {
                return (Func<string>)this.GetValue(GetDataFuncProperty);
            }
            set
            {
                this.SetValue(GetDataFuncProperty, value);
            }
        }

        public Func<string> GetData
        {
            get
            {
                return new Func<string>(() => _diagramViewModel.ToJson());
            }
        }


        public static readonly DependencyProperty ModeProperty =
         DependencyProperty.Register(nameof(Mode),
                                    typeof(string),
                                    typeof(MindEditor),
                                    new FrameworkPropertyMetadata("Edit", OnModeChanged));

        public string Mode
        {
            get
            {
                return (string)GetValue(ModeProperty);
            }
            set
            {
                SetValue(ModeProperty, value);
            }
        }

        private static void OnModeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var view = d as MindEditor;
            var mode = e.NewValue as string;
            if (mode != "Edit")
            {
                view._diagramViewModel.IsReadOnly = true;
            }
            else
            {
                view._diagramViewModel.IsReadOnly = false;
            }
        }

        #region ToolBox
        /// <summary>
        /// 附加组件模板
        /// </summary>
        public static readonly DependencyProperty ToolBoxProperty = DependencyProperty.Register(
            nameof(ToolBox), typeof(ControlTemplate), typeof(MindEditor), new FrameworkPropertyMetadata(default(ControlTemplate)));

        public ControlTemplate ToolBox
        {
            get
            {
                return (ControlTemplate)GetValue(ToolBoxProperty);
            }
            set
            {
                SetValue(ToolBoxProperty, value);
            }
        }
        #endregion

        #region SelectedObject

        public static readonly DependencyProperty SelectedObjectProperty = DependencyProperty.Register(nameof(SelectedObject), typeof(object), typeof(MindEditor), new UIPropertyMetadata(default(object)));
        public object SelectedObject
        {
            get
            {
                return (object)GetValue(SelectedObjectProperty);
            }
            set
            {
                SetValue(SelectedObjectProperty, value);
            }
        }

        #endregion 

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            base.OnPreviewKeyDown(e);

            e.Handled = _diagramViewModel.ExecuteShortcut(e);
        }

    }
}
