﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIStudio.Wpf.DiagramDesigner;

namespace AIStudio.Wpf.Mind.ViewModels
{
    public interface IMindDiagramViewModel : IDiagramViewModel
    {
        MindType MindType
        {
            get; set;
        }

        MindNode RootItem
        {
            get;
        }

        SimpleCommand AddParentCommand
        {
            get;
        }

        SimpleCommand AddChildCommand
        {
            get;
        }

        SimpleCommand AddPeerCommand
        {
            get;
        }

        SimpleCommand DeleteSelfCommand
        {
            get;
        }

        SimpleCommand MoveForwardCommand
        {
            get;
        }

        SimpleCommand MoveBackCommand
        {
            get;
        }

        SimpleCommand ChangeMindTypeCommand
        {
            get;
        }

        SimpleCommand ChangeMindThemeCommand
        {
            get;
        }
    }
}
