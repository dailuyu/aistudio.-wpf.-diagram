﻿using System.Windows;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramModels;
using AIStudio.Wpf.DiagramModels.ViewModels;
using AIStudio.Wpf.Mind;
using AIStudio.Wpf.Mind.ViewModels;

namespace AIStudio.Wpf.Flowchart.Models
{

    public class MindNodeModel : DiagramNode
    {
        public Size Spacing
        {
            get; set;
        }


        public Point Offset
        {
            get; set;
        }

        public bool IsExpanded
        {
            get; set;
        }

        public override DiagramItemViewModel ToNodel(IDiagramViewModel diagramViewModel)
        {
            MindNode mindNode = new MindNode(diagramViewModel);

            mindNode.Spacing = Spacing;
            mindNode.Offset = Offset;
            mindNode.IsExpanded = IsExpanded;

            return mindNode;
        }


    }
}
