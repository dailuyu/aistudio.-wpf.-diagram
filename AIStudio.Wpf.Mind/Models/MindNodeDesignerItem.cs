﻿using System.Windows;
using System.Xml.Serialization;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.Mind.ViewModels;
using Newtonsoft.Json;

namespace AIStudio.Wpf.Mind.Models
{
    public class MindNodeDesignerItem : DesignerItemBase
    {
        public MindNodeDesignerItem()
        {

        }
        public MindNodeDesignerItem(MindNode item) : base(item)
        {
            Spacing = item.Spacing;
            Offset = item.Offset;
            IsExpanded = item.IsExpanded;
        }

        [XmlIgnore]
        public Size Spacing
        {
            get; set;
        }

        [JsonIgnore]
        [XmlAttribute("Spacing")]
        public string XmlSpacing
        {
            get
            {
                return SerializeHelper.SerializeSize(Spacing);
            }
            set
            {
                Spacing = SerializeHelper.DeserializeSize(value);
            }
        }


        [XmlIgnore]
        public Point Offset
        {
            get; set;
        }

        [JsonIgnore]
        [XmlAttribute("Offset")]
        public string XmlOffset
        {
            get
            {
                return SerializeHelper.SerializePoint(Offset);
            }
            set
            {
                Offset = SerializeHelper.DeserializePoint(value);
            }
        }

        [XmlAttribute]
        public bool IsExpanded
        {
            get; set;
        }
    }
}
