﻿using System;
using System.Collections.Generic;
using System.Text;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.SFC.ViewModels
{
    public class SFCNodeNode : SFCNode
    {
        public SFCNodeNode() : base(SFCNodeKinds.Node)
        {
            ExecuteAddTopInput(null);
            ExecuteAddBottomOutput(null);
            ExecuteAddActionOutput(null);
        }

        public SFCNodeNode(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {
        }

        public SFCNodeNode(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

    }
}
