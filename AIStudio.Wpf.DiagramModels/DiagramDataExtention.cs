﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramModels.ViewModels;
using Newtonsoft.Json;


namespace AIStudio.Wpf.DiagramModels
{
    public static class DiagramDataExtention
    {
        #region ToJson
        public static string ToJson(this IDiagramViewModel diagram)
        {
            var json = JsonConvert.SerializeObject(new {
                Nodes = diagram.Items.OfType<DiagramItemViewModel>().Select(p => p.ToDiagramNode()).Where(p => p != null),
                Links = diagram.Items.OfType<ConnectionViewModel>().Select(p => p.ToDiagramLink()).Where(p => p != null)
            }, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            return json;
        }

        public static DiagramNode ToDiagramNode(this DiagramItemViewModel nodeModel)
        {
            DiagramNode diagramNode = nodeModel.ToDiagram();           

            diagramNode.Id = nodeModel.Id.ToString();
            if (nodeModel.ParentId != Guid.Empty)
            {
                diagramNode.ParentId = nodeModel.ParentId.ToString();
            }
            diagramNode.Label = nodeModel.Text;
            diagramNode.Width = nodeModel.ItemWidth * ScreenHelper.ScreenScale;
            diagramNode.Height = nodeModel.ItemHeight * ScreenHelper.ScreenScale;
            diagramNode.X = nodeModel.Left * ScreenHelper.ScreenScale;
            diagramNode.Y = nodeModel.Top * ScreenHelper.ScreenScale;
            diagramNode.ZIndex = nodeModel.ZIndex;
            diagramNode.Type = diagramNode.GetType().Name;
            diagramNode.PortAlignmentList = nodeModel.Connectors.Select(p => p.Orientation.ToString()).ToList();

            return diagramNode;
        }

        public static DiagramLink ToDiagramLink(this ConnectionViewModel linkModel)
        {
            DiagramLink diagramLink = new DiagramLink();

            diagramLink.Id = linkModel.Id.ToString();
            diagramLink.Color = SerializeHelper.SerializeColor(linkModel.ColorViewModel.LineColor.Color);
            diagramLink.SelectedColor = SerializeHelper.SerializeColor(Colors.Black);
            diagramLink.Width = linkModel.ColorViewModel.LineWidth;
            diagramLink.Label = linkModel.Text;

            if (linkModel.IsFullConnection)
            {
                diagramLink.SourceId = linkModel.SourceConnectorInfo.DataItem.Id.ToString();
                diagramLink.TargetId = linkModel.SinkConnectorInfoFully.DataItem.Id.ToString();

                //线条形状与箭头待处理
                //diagramLink.Router = baseLinkModel.Router?.Method.Name;
                //diagramLink.PathGenerator = baseLinkModel.PathGenerator?.Method.Name;
                //diagramLink.SourceMarkerPath = baseLinkModel.SourceMarker?.Path;
                //diagramLink.SourceMarkerWidth = baseLinkModel.SourceMarker?.Width;
                //diagramLink.TargetMarkerPath = baseLinkModel.TargetMarker?.Path;
                //diagramLink.TargetMarkerWidth = baseLinkModel.TargetMarker?.Width;

                diagramLink.Type = diagramLink.GetType().Name;

                diagramLink.SourcePortAlignment = linkModel.SourceConnectorInfo.Orientation.ToString();
                diagramLink.TargetPortAlignment = linkModel.SinkConnectorInfoFully.Orientation.ToString();
            }
            else
            {
                return null;
            }
            return diagramLink;
        }
        #endregion

        #region ToObject
        public static void ToObject(this IDiagramViewModel diagram, string json)
        {
            var data = JsonConvert.DeserializeObject<DiagramData>(json, new JsonConverter[] { new DiagramNodeConverter(), new DiagramLinkConverter() });
            if (data != null)
            {
                ToObject(diagram, data);
            }
        }
        public static void ToObject(this IDiagramViewModel diagram, DiagramData data)
        {
            diagram.Items.Clear();

            List<DiagramItemViewModel> nodes = new List<DiagramItemViewModel>();
            if (data.Nodes != null)
            {
                foreach (var node in data.Nodes)
                {
                    var nodemodel = node.ToNodelModel(diagram);
                    nodes.Add(nodemodel);
                    diagram.Items.Add(nodemodel);
                }
            }
            if (data.Links != null)
            {
                foreach (var link in data.Links)
                {
                    var source = nodes.FirstOrDefault(p => p.Id == new Guid(link.SourceId));
                    var target = nodes.FirstOrDefault(p => p.Id == new Guid(link.TargetId));
                    var linkmodel = link.ToLinkModel(diagram, source, target);
                    diagram.Items.Add(linkmodel);
                }
            }
        }

        private static DiagramItemViewModel ToNodelModel(this DiagramNode diagramNode, IDiagramViewModel diagram)
        {
            DiagramItemViewModel nodeModel = diagramNode.ToNodel(diagram);          

            nodeModel.Id = new Guid(diagramNode.Id);
            if (!string.IsNullOrEmpty(diagramNode.ParentId))
            {
                nodeModel.ParentId = new Guid(diagramNode.ParentId);
            }
            nodeModel.Root = diagram;
            nodeModel.Text = diagramNode.Label;
            nodeModel.ItemWidth = diagramNode.Width / ScreenHelper.ScreenScale;
            nodeModel.ItemHeight = diagramNode.Height / ScreenHelper.ScreenScale;
            nodeModel.Left = diagramNode.X / ScreenHelper.ScreenScale;
            nodeModel.Top = diagramNode.Y / ScreenHelper.ScreenScale;
            nodeModel.ZIndex = diagramNode.ZIndex;
            diagramNode.PortAlignmentList?.ForEach(p => nodeModel.AddConnector(new FullyCreatedConnectorInfo(nodeModel, p.ToEnum<ConnectorOrientation>())));

            return nodeModel;
        }

        public static ConnectionViewModel ToLinkModel(this DiagramLink diagramLink, IDiagramViewModel diagram, DiagramItemViewModel sourceNode, DiagramItemViewModel targetNode)
        {
            FullyCreatedConnectorInfo sourceConnectorInfo = sourceNode.Connectors.FirstOrDefault(p => p.Orientation.ToString() == diagramLink.SourcePortAlignment);
            FullyCreatedConnectorInfo sinkConnectorInfo = targetNode.Connectors.FirstOrDefault(p => p.Orientation.ToString() == diagramLink.TargetPortAlignment);
            ConnectionViewModel linkModel = new ConnectionViewModel(diagram, sourceConnectorInfo, sinkConnectorInfo, diagram.DrawModeViewModel?.LineDrawMode ?? DrawMode.ConnectingLineSmooth, diagram.DrawModeViewModel?.LineRouterMode ?? RouterMode.RouterNormal);
            linkModel.Id = new Guid(diagramLink.Id);
            linkModel.ColorViewModel.LineColor.Color = SerializeHelper.DeserializeColor(diagramLink.Color);
            linkModel.ColorViewModel.LineWidth = diagramLink.Width;
            if (!string.IsNullOrEmpty(diagramLink.Label))
            {
                linkModel.AddLabel(diagramLink.Label);
            }
            //线条形状与箭头待处理
            //switch (diagramLink.Router)
            //{
            //    case "Normal": linkModel.Router = Routers.Normal; break;
            //    case "Orthogonal": linkModel.Router = Routers.Orthogonal; break;
            //}
            //switch (diagramLink.PathGenerator)
            //{
            //    case "Smooth": linkModel.PathGenerator = PathGenerators.Smooth; break;
            //    case "Straight": linkModel.PathGenerator = PathGenerators.Straight; break;

            //}

            //if (!string.IsNullOrEmpty(diagramLink.SourceMarkerPath))
            //{
            //    linkModel.SourceMarker = new LinkMarker(diagramLink.SourceMarkerPath, diagramLink.SourceMarkerWidth ?? 10.0);
            //}
            //if (!string.IsNullOrEmpty(diagramLink.TargetMarkerPath))
            //{
            //    linkModel.TargetMarker = new LinkMarker(diagramLink.TargetMarkerPath, diagramLink.TargetMarkerWidth ?? 10.0);
            //}
            return linkModel;
        }
        #endregion
    }
}
