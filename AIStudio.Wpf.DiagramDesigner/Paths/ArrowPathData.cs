﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class ArrowPathData
    {
        public static readonly Dictionary<ArrowPathStyle, string> Arrow = new Dictionary<ArrowPathStyle, string>()
        {
            { ArrowPathStyle.None, "" },
            { ArrowPathStyle.Arrow, "M 0 -5 10 0 0 5 z" },
            { ArrowPathStyle.Circle, "M 0, 0 a 5,5 0 1,0 10,0 a 5,5 0 1,0 -10,0" },
            { ArrowPathStyle.Square, "M 0 -5 10 -5 10 5 0 5 z" },           
        };
    }
}
