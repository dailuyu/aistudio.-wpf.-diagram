﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace AIStudio.Wpf.DiagramDesigner.Controls
{
    public class DragThumb : Thumb
    {
        public DragThumb()
        {
            base.DragDelta += new DragDeltaEventHandler(DragThumb_DragDelta);
            base.DragStarted += DragThumb_DragStarted;
            base.DragCompleted += DragThumb_DragCompleted;
        }

        private List<SelectableDesignerItemViewModelBase> designerItems;

        private bool drag;
        private void DragThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            drag = false;
            SelectableDesignerItemViewModelBase designerItem = this.DataContext as SelectableDesignerItemViewModelBase;

            if (designerItem != null && designerItem.IsSelected)
            {
                // we only move DesignerItems
                designerItems = designerItem.Root.SelectedItems.ToList();
                if (designerItem is ConnectionViewModel connector)
                {
                    designerItems.Add(connector.SourceConnectorInfo.DataItem);
                    if (connector.IsFullConnection)
                    {
                        designerItems.Add(connector.SinkConnectorInfoFully.DataItem);
                    }
                }

                if (designerItem is PointDesignerItemViewModel)
                {
                    designerItems = new List<SelectableDesignerItemViewModelBase> { designerItem };
                }

                foreach (DesignerItemViewModelBase item in designerItems.OfType<DesignerItemViewModelBase>())
                {
                    item.BeginDo = true;
                    item.SetOldValue(item.Left, nameof(item.Left));
                    item.SetOldValue(item.Top, nameof(item.Top));
                }

                e.Handled = true;
            }
            else
            {
                designerItems = null;
            }
        }

        private void DragThumb_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            if (drag == false) return;

            if (designerItems != null)
            {
                foreach (DesignerItemViewModelBase item in designerItems.OfType<DesignerItemViewModelBase>())
                {
                    item.SetCellAlignment();
                    item.BeginDo = false;
                    item.RaiseTopLeft();
                }

                e.Handled = true;
            }
        }

        void DragThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            drag = true;
            if (designerItems != null)
            {
                double minLeft = double.MaxValue;
                double minTop = double.MaxValue;

                foreach (DesignerItemViewModelBase item in designerItems.OfType<DesignerItemViewModelBase>())
                {
                    double left = item.Left;
                    double top = item.Top;
                    minLeft = double.IsNaN(left) ? 0 : Math.Min(left, minLeft);
                    minTop = double.IsNaN(top) ? 0 : Math.Min(top, minTop);

                    double deltaHorizontal = Math.Max(-minLeft, e.HorizontalChange);
                    double deltaVertical = Math.Max(-minTop, e.VerticalChange);
                    item.Left += deltaHorizontal;
                    item.Top += deltaVertical;
                }


                e.Handled = true;
            }
        }

        private DesignerCanvas GetDesignerCanvas(DependencyObject element)
        {
            while (element != null && !(element is DesignerCanvas))
                element = VisualTreeHelper.GetParent(element);

            return element as DesignerCanvas;
        }
    }
}
