﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public enum DrawMode
    {
        Normal = 0,
        Line = 1,
        Rectangle = 2,
        Ellipse = 3,
        Polyline = 4,
        Polygon = 5,
        DirectLine = 6,
        ConnectingLineSmooth = 10,
        ConnectingLineStraight = 11,
        ConnectingLineCorner = 12,
        ConnectingLineBoundary = 13,        
        Text = 20,
    }
}
