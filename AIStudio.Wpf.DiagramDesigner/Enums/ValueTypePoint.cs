﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public enum ValueTypePoint
    {
        Real = 0,
        Int = 1,
        Bool = 2,
    }
}
