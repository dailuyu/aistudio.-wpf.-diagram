﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    /// <summary>
    /// Simple service interface
    /// </summary>
    public interface IDiagramServiceProvider : INotifyPropertyChanged
    {
        IColorViewModel ColorViewModel
        {
            get;
        }
        IFontViewModel FontViewModel
        {
            get;
        }
        IShapeViewModel ShapeViewModel
        {
            get;
        }
        IDrawModeViewModel DrawModeViewModel
        {
            get;
        }
        IQuickThemeViewModel QuickThemeViewModel
        {
            get;
        }
        ILockObjectViewModel LockObjectViewModel
        {
            get;
        }
        SelectableDesignerItemViewModelBase SelectedItem
        {
            get; set;
        }

        List<SelectableDesignerItemViewModelBase> SelectedItems
        {
            get; set;
        }

        IColorViewModel CopyDefaultColorViewModel();
        IFontViewModel CopyDefaultFontViewModel();
        IShapeViewModel CopyDefaultShapeViewModel();
    }
}
