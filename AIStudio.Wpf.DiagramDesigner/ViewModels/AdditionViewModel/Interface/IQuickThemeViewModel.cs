﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public interface IQuickThemeViewModel
    {
        QuickTheme[] QuickThemes { get; }
        QuickTheme QuickTheme { get; set; }
    }
}
