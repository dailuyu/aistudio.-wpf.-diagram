﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner.Geometrys;

namespace AIStudio.Wpf.DiagramDesigner
{
    public interface IColorViewModel
    {
        IColorObject LineColor
        {
            get; set;
        }
        IColorObject FillColor
        {
            get; set;
        }
        Color ShadowColor
        {
            get; set;
        }
        double LineWidth
        {
            get; set;
        }
        LineDashStyle LineDashStyle
        {
            get; set;
        }
        LineAnimation LineAnimation
        {
            get; set;
        }
        double LineAnimationDuration
        {
            get; set;
        }
        event PropertyChangedEventHandler PropertyChanged;
    }
}
