﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public interface IShapeViewModel
    {
        ILinkMarker SourceMarker
        {
            get; set;
        }

        ILinkMarker SinkMarker
        {
            get; set;
        }

        event PropertyChangedEventHandler PropertyChanged;
    }


}
