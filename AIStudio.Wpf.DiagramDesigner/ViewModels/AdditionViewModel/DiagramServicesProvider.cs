﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace AIStudio.Wpf.DiagramDesigner
{
    /// <summary>
    /// Simple service locator
    /// </summary>
    public class DiagramServiceProvider : BindableBase, IDiagramServiceProvider
    {
        public DiagramServiceProvider()
        {
            ColorViewModel = new ColorViewModel();
            FontViewModel = new FontViewModel();
            ShapeViewModel = new ShapeViewModel();
            LockObjectViewModel = new LockObjectViewModel();

            _drawModeViewModel = new DrawModeViewModel();
            _quickThemeViewModel = new QuickThemeViewModel();
          
            _drawModeViewModel.PropertyChanged += ViewModel_PropertyChanged;
            _quickThemeViewModel.PropertyChanged += ViewModel_PropertyChanged;

            SetOldValue<IColorViewModel>(ColorViewModel, nameof(ColorViewModel));
            SetOldValue<IFontViewModel>(FontViewModel, nameof(FontViewModel));
            SetOldValue<IShapeViewModel>(ShapeViewModel, nameof(ShapeViewModel));
            SetOldValue<ILockObjectViewModel>(LockObjectViewModel, nameof(LockObjectViewModel));
        }

        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            RaisePropertyChanged(sender, e.PropertyName);
        }

        public IColorViewModel CopyDefaultColorViewModel()
        {
            var viewModel = GetOldValue<ColorViewModel>(nameof(ColorViewModel));
            return CopyHelper.Mapper(viewModel);
        }

        public IFontViewModel CopyDefaultFontViewModel()
        {
            var viewModel = GetOldValue<FontViewModel>(nameof(FontViewModel));
            return CopyHelper.Mapper<FontViewModel, IFontViewModel>(viewModel);
        }

        public IShapeViewModel CopyDefaultShapeViewModel()
        {
            var viewModel = GetOldValue<ShapeViewModel>(nameof(ShapeViewModel));
            return CopyHelper.Mapper(viewModel);
        }

        private IColorViewModel _colorViewModel;
        public IColorViewModel ColorViewModel
        {
            get
            {
                return _colorViewModel;
            }
            set
            {
                if (_colorViewModel != null)
                {
                    _colorViewModel.PropertyChanged -= ViewModel_PropertyChanged;
                }
                SetProperty(ref _colorViewModel, value);
                if (_colorViewModel != null)
                {
                    _colorViewModel.PropertyChanged += ViewModel_PropertyChanged;
                }
            }
        }      

        private IFontViewModel _fontViewModel;
        public IFontViewModel FontViewModel
        {
            get
            {
                return _fontViewModel;
            }
            set
            {
                if (_fontViewModel != null)
                {
                    _fontViewModel.PropertyChanged -= ViewModel_PropertyChanged;
                }
                SetProperty(ref _fontViewModel, value);
                if (_fontViewModel != null)
                {
                    _fontViewModel.PropertyChanged += ViewModel_PropertyChanged;
                }
            }
        }

        private IShapeViewModel _linkMarkerViewModel;
        public IShapeViewModel ShapeViewModel
        {
            get
            {
                return _linkMarkerViewModel;
            }
            set
            {
                if (_linkMarkerViewModel != null)
                {
                    _linkMarkerViewModel.PropertyChanged -= ViewModel_PropertyChanged;
                }
                SetProperty(ref _linkMarkerViewModel, value);
                if (_linkMarkerViewModel != null)
                {
                    _linkMarkerViewModel.PropertyChanged += ViewModel_PropertyChanged;
                }
            }
        }

        private DrawModeViewModel _drawModeViewModel;
        public IDrawModeViewModel DrawModeViewModel
        {
            get { return _drawModeViewModel; }
        }

        private QuickThemeViewModel _quickThemeViewModel;
        public IQuickThemeViewModel QuickThemeViewModel
        {
            get { return _quickThemeViewModel; }
        }

        private ILockObjectViewModel _lockObjectViewModel;
        public ILockObjectViewModel LockObjectViewModel
        {
            get
            {
                return _lockObjectViewModel;
            }
            set
            {
                if (_lockObjectViewModel != null)
                {
                    _lockObjectViewModel.PropertyChanged -= ViewModel_PropertyChanged;
                }
                SetProperty(ref _lockObjectViewModel, value);
                if (_lockObjectViewModel != null)
                {
                    _lockObjectViewModel.PropertyChanged += ViewModel_PropertyChanged;
                }
            }
        }


        public List<SelectableDesignerItemViewModelBase> SelectedItems
        {
            get; set;
        }

        private SelectableDesignerItemViewModelBase _selectedItem;
        public SelectableDesignerItemViewModelBase SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {               
                if (SetProperty(ref _selectedItem, value))
                {
                    if (_selectedItem == null)
                    {
                        ColorViewModel = GetOldValue<ColorViewModel>(nameof(ColorViewModel));
                        FontViewModel = GetOldValue<FontViewModel>(nameof(FontViewModel));
                        ShapeViewModel = GetOldValue<ShapeViewModel>(nameof(ShapeViewModel));
                        LockObjectViewModel = GetOldValue<LockObjectViewModel>(nameof(LockObjectViewModel));
                    }
                    else
                    {
                        ColorViewModel = _selectedItem.ColorViewModel;
                        FontViewModel = _selectedItem.FontViewModel;
                        ShapeViewModel = _selectedItem.ShapeViewModel;
                        LockObjectViewModel = _selectedItem.LockObjectViewModel;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Simple service locator helper
    /// </summary>
    public class DiagramServicesProvider
    {
        private static Lazy<DiagramServicesProvider> instance = new Lazy<DiagramServicesProvider>(() => new DiagramServicesProvider());
        private IDiagramServiceProvider serviceProvider = new DiagramServiceProvider();

        public void SetNewServiceProvider(IDiagramServiceProvider provider)
        {
            serviceProvider = provider;
        }

        public IDiagramServiceProvider Provider
        {
            get { return serviceProvider; }
        }

        public static DiagramServicesProvider Instance
        {
            get { return instance.Value; }
        }
    }
}
