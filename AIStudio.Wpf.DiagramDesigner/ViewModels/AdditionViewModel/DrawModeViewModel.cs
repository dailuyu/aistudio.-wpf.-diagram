﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class DrawModeViewModel : BindableBase, IDrawModeViewModel
    {
        public DrawMode GetDrawMode()
        {
            if (CursorDrawModeSelected)
            {
                return CursorDrawMode;
            }
            else if (LineDrawModeSelected)
            {
                return LineDrawMode;
            }
            else if (ShapeDrawModeSelected)
            {
                return ShapeDrawMode;
            }
            else if (TextDrawModeSelected)
            {
                return TextDrawMode;
            }

            return DrawMode.Normal;
        }

        public void ResetDrawMode()
        {
            CursorDrawModeSelected = true;
            CursorDrawMode = DrawMode.Normal;
        }

        public void SetDrawMode(DrawMode drawMode)
        {
            CursorDrawMode = drawMode;
        }

        private bool _cursordrawModeSelected = true;
        public bool CursorDrawModeSelected
        {
            get
            {
                return _cursordrawModeSelected;
            }
            set
            {
                SetProperty(ref _cursordrawModeSelected, value);
            }
        }

        private bool _lineDrawModeSelected;
        public bool LineDrawModeSelected
        {
            get
            {
                return _lineDrawModeSelected;
            }
            set
            {
                SetProperty(ref _lineDrawModeSelected, value);
            }
        }

        private bool _vectorRouterModeSelected;
        public bool LineRouterModeSelected
        {
            get
            {
                return _vectorRouterModeSelected;
            }
            set
            {
                SetProperty(ref _vectorRouterModeSelected, value);
            }
        }

        private bool _shapeDrawModeSelected;
        public bool ShapeDrawModeSelected
        {
            get
            {
                return _shapeDrawModeSelected;
            }
            set
            {
                SetProperty(ref _shapeDrawModeSelected, value);
            }
        }

        private bool _textDrawModeSelected;
        public bool TextDrawModeSelected
        {
            get
            {
                return _textDrawModeSelected;
            }
            set
            {
                SetProperty(ref _textDrawModeSelected, value);
            }
        }

        private DrawMode _cursordrawMode = DrawMode.Normal;
        public DrawMode CursorDrawMode
        {
            get
            {
                return _cursordrawMode;
            }
            set
            {
                SetProperty(ref _cursordrawMode, value);
                CursorDrawModeSelected = true;
            }
        }

        private DrawMode _lineDrawMode = DrawMode.ConnectingLineSmooth;
        public DrawMode LineDrawMode
        {
            get
            {
                return _lineDrawMode;
            }
            set
            {
                SetProperty(ref _lineDrawMode, value);
                LineDrawModeSelected = true;
            }
        }

        private RouterMode _lineRouterMode = RouterMode.RouterNormal;
        public RouterMode LineRouterMode
        {
            get
            {
                return _lineRouterMode;
            }
            set
            {
                SetProperty(ref _lineRouterMode, value);
            }
        }

        private DrawMode _shapeDrawMode = DrawMode.Rectangle;
        public DrawMode ShapeDrawMode
        {
            get
            {
                return _shapeDrawMode;
            }
            set
            {
                SetProperty(ref _shapeDrawMode, value);
                ShapeDrawModeSelected = true;
            }
        }

        private DrawMode _textDrawMode = DrawMode.Text;
        public DrawMode TextDrawMode
        {
            get
            {
                return _textDrawMode;
            }
            set
            {
                SetProperty(ref _textDrawMode, value);
                TextDrawModeSelected = true;
            }
        }

        private CursorMode _cursorMode;
        public CursorMode CursorMode
        {
            get
            {
                return _cursorMode;
            }
            set
            {
                SetProperty(ref _cursorMode, value);
            }
        }

        private bool _enableSnapping;
        public bool EnableSnapping
        {
            get
            {
                return _enableSnapping;
            }
            set
            {
                SetProperty(ref _enableSnapping, value);
            }
        }

        private double _snappingRadius = 50;
        public double SnappingRadius
        {
            get
            {
                return _snappingRadius;
            }
            set
            {
                SetProperty(ref _snappingRadius, value);
            }
        }

    }
}
