﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class DiagramOption
    {
        public LayoutOption LayoutOption
        {
            get; set;
        } = new LayoutOption();

        public ShortcutOption ShortcutOption
        {
            get; set;
        } = new ShortcutOption();
    }

    public class LayoutOption
    {
    
    }

    public class ShortcutOption
    {
        [Description("Select All shortcut (CTRL+A by default)")]
        public Func<KeyEventArgs, bool> SelectAll
        {
            get; set;
        } = e => e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.A;

        [Description("Copy shortcut (CTRL+C by default)")]
        public Func<KeyEventArgs, bool> Copy
        {
            get; set;
        } = e => e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C;

        [Description("Paste shortcut (CTRL+V by default)")]
        public Func<KeyEventArgs, bool> Paste
        {
            get; set;
        } = e => e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.V;

        [Description("Cut shortcut (CTRL+X by default)")]
        public Func<KeyEventArgs, bool> Cut
        {
            get; set;
        } = e => e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X;

        [Description("Undo shortcut (CTRL+Z by default)")]
        public Func<KeyEventArgs, bool> Undo
        {
            get; set;
        } = e => e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z;

        [Description("Undo shortcut (CTRL+Y by default)")]
        public Func<KeyEventArgs, bool> Redo
        {
            get; set;
        } = e => e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Y;

        [Description("Delete shortcut (Delete by default)")]
        public Func<KeyEventArgs, bool> Delete
        {
            get; set;
        } = e => e.KeyboardDevice.Modifiers == ModifierKeys.None && e.Key == Key.Delete;

        [Description("Left Move shortcut (Left by default)")]
        public Func<KeyEventArgs, bool> LeftMove
        {
            get; set;
        } = e => e.KeyboardDevice.Modifiers == ModifierKeys.None && e.Key == Key.Left;

        [Description("Right Move shortcut (Right by default)")]
        public Func<KeyEventArgs, bool> RightMove
        {
            get; set;
        } = e => e.KeyboardDevice.Modifiers == ModifierKeys.None && e.Key == Key.Right;

        [Description("Up Move shortcut (Up by default)")]
        public Func<KeyEventArgs, bool> UpMove
        {
            get; set;
        } = e => e.KeyboardDevice.Modifiers == ModifierKeys.None && e.Key == Key.Up;

        [Description("Down Move shortcut (Down by default)")]
        public Func<KeyEventArgs, bool> DownMove
        {
            get; set;
        } = e => e.KeyboardDevice.Modifiers == ModifierKeys.None && e.Key == Key.Down;

        [Description("Group Keyboard shortcut (CTRL+Shift+G by default)")]
        public Func<KeyEventArgs, bool> Group
        {
            get; set;
        } = e => e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.G;

    }
}
