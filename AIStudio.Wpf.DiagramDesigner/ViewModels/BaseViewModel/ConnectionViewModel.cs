﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner.Controls;
using AIStudio.Wpf.DiagramDesigner.Geometrys;
using AIStudio.Wpf.DiagramDesigner.Helpers;
using SvgPathProperties;

namespace AIStudio.Wpf.DiagramDesigner
{
    /// <summary>
    /// DefaultLink
    /// </summary>
    public class ConnectionViewModel : SelectableDesignerItemViewModelBase
    {
        public ConnectionViewModel(FullyCreatedConnectorInfo sourceConnectorInfo, ConnectorInfoBase sinkConnectorInfo, DrawMode drawMode = DrawMode.ConnectingLineSmooth, RouterMode routerMode = AIStudio.Wpf.DiagramDesigner.RouterMode.RouterNormal) : this(null, sourceConnectorInfo, sinkConnectorInfo, drawMode, routerMode)
        {

        }

        public ConnectionViewModel(IDiagramViewModel root, FullyCreatedConnectorInfo sourceConnectorInfo, ConnectorInfoBase sinkConnectorInfo, DrawMode drawMode = DrawMode.ConnectingLineSmooth, RouterMode routerMode = AIStudio.Wpf.DiagramDesigner.RouterMode.RouterNormal) : base(root)
        {
            PathMode = drawMode.ToString();
            RouterMode = routerMode.ToString();
            Init(sourceConnectorInfo, sinkConnectorInfo);
        }

        public ConnectionViewModel(IDiagramViewModel root, FullyCreatedConnectorInfo sourceConnectorInfo, FullyCreatedConnectorInfo sinkConnectorInfo, ConnectionItem designer) : base(root, designer)
        {
            PathMode = designer.PathMode;
            RouterMode = designer.RouterMode;
            Init(sourceConnectorInfo, sinkConnectorInfo);
        }

        public override SelectableItemBase GetSerializableObject()
        {
            if (IsFullConnection)
            {
                ConnectionItem connection = new ConnectionItem(this);

                return connection;
            }
            else
            {
                return null;
            }
        }

        protected virtual void Init(FullyCreatedConnectorInfo sourceConnectorInfo, ConnectorInfoBase sinkConnectorInfo)
        {
            this.Root = sourceConnectorInfo.DataItem.Root;

            if (sinkConnectorInfo is FullyCreatedConnectorInfo sink && sink.DataItem.ShowArrow == false)
            {
                this.ShapeViewModel.SinkMarker = new LinkMarker("", 10, 10, ArrowPathStyle.None, ArrowSizeStyle.Middle);
            }
            this.ColorViewModel.PropertyChanged += ConnectorViewModel_PropertyChanged;
            this.ShapeViewModel.PropertyChanged += ConnectorViewModel_PropertyChanged;
            this.PropertyChanged += ConnectorViewModel_PropertyChanged;

            var routetype = GlobalType.AllTypes.Where(p => typeof(IRouter).IsAssignableFrom(p)).FirstOrDefault(p => p.Name == RouterMode);
            Router = routetype != null ? (System.Activator.CreateInstance(routetype) as IRouter) : new RouterNormal();

            var pathGeneratortype = GlobalType.AllTypes.Where(p => typeof(IPathGenerator).IsAssignableFrom(p)).FirstOrDefault(p => p.Name == PathMode);
            PathGenerator = pathGeneratortype != null ? (System.Activator.CreateInstance(pathGeneratortype) as IPathGenerator) : new ConnectingLineSmooth();

            this.SourceConnectorInfo = sourceConnectorInfo;
            this.SinkConnectorInfo = sinkConnectorInfo;
            DeleteConnectionCommand = new SimpleCommand(Command_Enable, DeleteConnection);
            AddVertexCommand = new SimpleCommand(Command_Enable, AddVertex);
            AddLabelCommand = new SimpleCommand(Command_Enable, para => AddLabel());
        }

        protected override void LoadDesignerItemViewModel(SelectableItemBase designerbase)
        {
            base.LoadDesignerItemViewModel(designerbase);

            if (designerbase is ConnectionItem designer)
            {
                Vertices = new ObservableCollection<ConnectorVertexModel>(designer.Vertices.Select(p => new ConnectorVertexModel(this.Root, this, designer)));
                Labels = new ObservableCollection<ConnectorLabelModel>(designer.Labels.Select(p => new ConnectorLabelModel(this.Root, this, designer)));
            }
        }

        #region 属性
        private string _text;
        [Browsable(true)]
        [CanDo]
        public override string Text
        {
            get
            {
                var text = _text;
                if (Labels?.Count > 0)
                {
                    text = Labels[0].Text;
                }

                if (FontViewModel.FontCase == FontCase.Upper)
                {
                    return text?.ToUpper();
                }
                else if (FontViewModel.FontCase == FontCase.Lower)
                {
                    return text?.ToLower();
                }
                else
                {
                    return text;
                }
            }
            set
            {
                if (SetProperty(ref _text, value))
                {
                    if (!string.IsNullOrEmpty(_text))
                    {
                        if (Labels?.Count > 0)
                        {
                            Labels[0].Text = _text;
                        }
                        else
                        {
                            AddLabel(_text);
                        }
                    }
                }
            }
        }

        private PointBase _sourceA;
        public PointBase SourceA
        {
            get
            {
                return _sourceA;
            }
            set
            {
                SetProperty(ref _sourceA, value);
            }
        }

        private PointBase _sourceB;
        public PointBase SourceB
        {
            get
            {
                return _sourceB;
            }
            set
            {
                SetProperty(ref _sourceB, value);
            }
        }

        private PointBase _startPoint;
        public PointBase StartPoint
        {
            get
            {
                return _startPoint;
            }
            private set
            {
                SetProperty(ref _startPoint, value);
            }
        }

        private PointBase _endPoint;
        public PointBase EndPoint
        {
            get
            {
                return _endPoint;
            }
            private set
            {
                SetProperty(ref _endPoint, value);
            }
        }

        private double _startAngle;
        public double StartAngle
        {
            get
            {
                return _startAngle;
            }
            private set
            {
                SetProperty(ref _startAngle, value);
            }
        }

        private double _endAngle;
        public double EndAngle
        {
            get
            {
                return _endAngle;
            }
            private set
            {
                SetProperty(ref _endAngle, value);
            }
        }

        private RectangleBase _startRectangle;
        public RectangleBase StartRectangle
        {
            get
            {
                return _startRectangle;
            }
            private set
            {
                SetProperty(ref _startRectangle, value);
            }
        }

        private RectangleBase _endRectangle;
        public RectangleBase EndRectangle
        {
            get
            {
                return _endRectangle;
            }
            private set
            {
                SetProperty(ref _endRectangle, value);
            }
        }

        private RectangleBase _area;
        public RectangleBase Area
        {
            get
            {
                return _area;
            }
            private set
            {
                SetProperty(ref _area, value);
            }
        }

        private string _pathMode;
        public string PathMode
        {
            get
            {
                return _pathMode;
            }
            set
            {
                SetProperty(ref _pathMode, value);
            }
        }

        private string _routerMode;
        public string RouterMode
        {
            get
            {
                return _routerMode;
            }
            set
            {
                SetProperty(ref _routerMode, value);
            }
        }

        public IRouter Router
        {
            get; set;
        }

        public IPathGenerator PathGenerator
        {
            get; set;
        }

        private PathGeneratorResult _pathGeneratorResult;
        public PathGeneratorResult PathGeneratorResult
        {
            get
            {
                return _pathGeneratorResult;
            }
            private set
            {
                SetProperty(ref _pathGeneratorResult, value);
            }
        }

        private bool _shouldInsertAnchor;
        public bool ShouldInsertAnchor
        {
            get
            {
                return _shouldInsertAnchor;
            }
            set
            {
                SetProperty(ref _shouldInsertAnchor, value);
            }
        }

        //待完善这两处
        private ObservableCollection<ConnectorVertexModel> _vertices = new ObservableCollection<ConnectorVertexModel>();
        public ObservableCollection<ConnectorVertexModel> Vertices
        {
            get
            {
                return _vertices;
            }
            private set
            {
                SetProperty(ref _vertices, value);
            }
        }

        private ObservableCollection<ConnectorLabelModel> _labels = new ObservableCollection<ConnectorLabelModel>();
        public ObservableCollection<ConnectorLabelModel> Labels
        {
            get
            {
                return _labels;
            }
            private set
            {
                SetProperty(ref _labels, value);
            }
        }

        public virtual Dictionary<string, string> PropertiesSetting
        {
            get
            {
                return new Dictionary<string, string>()
                {
                    { "Text","文本" },
                };
            }
        }

        private FullyCreatedConnectorInfo _sourceConnectorInfo;
        public FullyCreatedConnectorInfo SourceConnectorInfo
        {
            get
            {
                return _sourceConnectorInfo;
            }
            set
            {
                SetProperty(ref _sourceConnectorInfo, value);
            }
        }

        private ConnectorInfoBase _sinkConnectorInfo;
        public ConnectorInfoBase SinkConnectorInfo
        {
            get
            {
                return _sinkConnectorInfo;
            }
            set
            {
                SetProperty(ref _sinkConnectorInfo, value);

            }
        }

        public FullyCreatedConnectorInfo SinkConnectorInfoFully
        {
            get
            {
                return SinkConnectorInfo as FullyCreatedConnectorInfo;
            }
        }

        public PartCreatedConnectorInfo SinkConnectorInfoPart
        {
            get
            {
                return SinkConnectorInfo as PartCreatedConnectorInfo;
            }
        }

        public PointBase OnGoingPosition
        {
            get
            {
                return SinkConnectorInfoPart?.MiddlePosition ?? PointBase.Zero;
            }
        }

        public bool IsFullConnection
        {
            get
            {
                return SinkConnectorInfoFully != null;
            }
        }

        public void RaiseFullConnection()
        {
            RaisePropertyChanged(nameof(IsFullConnection));
        }

        public double SmoothMargin { get; set; } = 125;

        public double SmoothAutoSlope { get; set; } = 1;

        public bool IsPortless => SourceConnectorInfo.IsPortless || SinkConnectorInfoFully?.IsPortless == true;
        #endregion

        #region 方法
        public SimpleCommand DeleteConnectionCommand
        {
            get; set;
        }

        public SimpleCommand AddVertexCommand
        {
            get; set;
        }

        public SimpleCommand AddLabelCommand
        {
            get; set;
        }
        #endregion

        private void ConnectorViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (sender is ConnectionViewModel)
            {
                switch (e.PropertyName)
                {
                    case nameof(SourceA):
                    case nameof(SourceB):
                        UpdateArea();
                        break;
                    case nameof(Area):
                        UpdatePathGeneratorResult();
                        break;
                    case nameof(Vertices):
                        foreach (var vertice in Vertices)
                        {
                            vertice.PropertyChanged += new WeakINPCEventHandler(ConnectorViewModel_PropertyChanged).Handler;
                        }
                        break;
                    case nameof(Labels):
                        foreach (var label in Labels)
                        {
                            label.PropertyChanged += new WeakINPCEventHandler(ConnectorViewModel_PropertyChanged).Handler;
                        }
                        break;
                    case nameof(SourceConnectorInfo):
                        if (SourceConnectorInfo != null)
                        {
                            SourceA = PointHelper.GetPointForConnector(SourceConnectorInfo);
                            SourceConnectorInfo.DataItem.PropertyChanged += new WeakINPCEventHandler(ConnectorViewModel_PropertyChanged).Handler;
                        }
                        break;
                    case nameof(SinkConnectorInfo):
                        if (SinkConnectorInfo != null)
                        {
                            SourceB = SinkConnectorInfo.Position;
                            if (IsFullConnection)
                            {
                                SinkConnectorInfoFully.DataItem.PropertyChanged += new WeakINPCEventHandler(ConnectorViewModel_PropertyChanged).Handler;
                            }
                        }
                        break;
                    case nameof(IsSelected):
                        if (IsSelected == false)
                        {
                            Labels.FirstOrDefault()?.AddToSelection(false);
                        }
                        break;
                    case nameof(RouterMode):
                        var routetype = GlobalType.AllTypes.Where(p => typeof(IRouter).IsAssignableFrom(p)).FirstOrDefault(p => p.Name == RouterMode);
                        Router = routetype != null ? (System.Activator.CreateInstance(routetype) as IRouter) : new RouterNormal();
                        UpdatePathGeneratorResult();
                        break;
                    case nameof(PathMode):
                        var pathGeneratortype = GlobalType.AllTypes.Where(p => typeof(IPathGenerator).IsAssignableFrom(p)).FirstOrDefault(p => p.Name == PathMode);
                        PathGenerator = pathGeneratortype != null ? (System.Activator.CreateInstance(pathGeneratortype) as IPathGenerator) : new ConnectingLineSmooth();
                        UpdatePathGeneratorResult();
                        break;
                }
            }
            else if (sender is ShapeViewModel)
            {
                if (e.PropertyName == nameof(ShapeViewModel.SourceMarker) ||
                   e.PropertyName == nameof(ShapeViewModel.SinkMarker))
                {
                    UpdatePathGeneratorResult();
                }
            }
            else if (sender is DesignerItemViewModelBase)
            {
                switch (e.PropertyName)
                {
                    case nameof(DesignerItemViewModelBase.ItemHeight):
                    case nameof(DesignerItemViewModelBase.ItemWidth):
                    case nameof(DesignerItemViewModelBase.Left):
                    case nameof(DesignerItemViewModelBase.Top):
                        if (IsFullConnection)
                        {
                            //减少触发一次画线
                            SourceA = PointHelper.GetPointForConnector(this.SourceConnectorInfo);
                            SourceB = PointHelper.GetPointForConnector(this.SinkConnectorInfoFully);
                        }
                        else
                        {
                            SourceA = PointHelper.GetPointForConnector(this.SourceConnectorInfo);
                        }
                        break;
                }
            }
            else if (sender is ConnectorVertexModel)
            {
                switch (e.PropertyName)
                {
                    case nameof(ConnectorPointModel.X):
                    case nameof(ConnectorPointModel.Y):
                        UpdatePathGeneratorResult();
                        break;

                }
            }
            else if (sender is ConnectorLabelModel linkLabelModel)
            {
                switch (e.PropertyName)
                {
                    case nameof(ConnectorPointModel.X):
                        {
                            if (e is ValuePropertyChangedEventArgs valuePropertyChangedEventArgs)
                            {
                                linkLabelModel.UpdateOffsetX((double)valuePropertyChangedEventArgs.OldValue, (double)valuePropertyChangedEventArgs.NewValue);
                            }
                            break;
                        }
                    case nameof(ConnectorPointModel.Y):
                        {
                            if (e is ValuePropertyChangedEventArgs valuePropertyChangedEventArgs)
                            {
                                linkLabelModel.UpdateOffsetY((double)valuePropertyChangedEventArgs.OldValue, (double)valuePropertyChangedEventArgs.NewValue);
                            }
                            break;
                        }
                }
            }
            else if (sender is ColorViewModel)
            {
    
            }
        }

        private void UpdateArea()
        {
            Area = new RectangleBase(SourceA, SourceB);
        }

        private void UpdatePathGeneratorResult()
        {
            if (SourceConnectorInfo == null || SinkConnectorInfo == null)
                return;

            var route = Router.Get(Root, this);

            (var source, var target) = FindConnectionPoints(route);
            if (source == null || target == null)
                return;

            PathGeneratorResult = PathGenerator.Get(Root, this, route, source.Value, target.Value);

            var startMiddle = new RectangleBase
                (               
                (PathGeneratorResult.SourceMarkerPosition.X + (source.Value.X - Area.Left)) / 2,
                (PathGeneratorResult.SourceMarkerPosition.Y + (source.Value.Y - Area.Top)) / 2,
                0,
                0
                );
            var endMiddle = new RectangleBase
                (
                (PathGeneratorResult.TargetMarkerPosition.X + (target.Value.X - Area.Left)) / 2,
                (PathGeneratorResult.TargetMarkerPosition.Y + (target.Value.Y - Area.Top)) / 2,
                0,
                0
                );
            StartRectangle = startMiddle.InflateRectangle(GetSourceMarkerWidth() / 2, GetSourceMarkerHeight() / 2);
            EndRectangle = endMiddle.InflateRectangle(GetSinkMarkerWidth() / 2, GetSinkMarkerHeight() / 2);

            StartAngle = PathGeneratorResult.SourceMarkerAngle;
            EndAngle = PathGeneratorResult.TargetMarkerAngle;

            var paths = Labels.Count > 0 ? PathGeneratorResult.Paths.Select(p => new SvgPath(p)).ToArray() : Array.Empty<SvgPath>();
            foreach (var label in Labels)
            {
                label.UpdatePosition(paths);
            }
        }

        private void DeleteConnection(object args)
        {
            if (this.Root is IDiagramViewModel)
            {
                var diagramVM = this.Root as IDiagramViewModel;
                diagramVM.RemoveItemCommand.Execute(this);
            }
        }

        private (PointBase? source, PointBase? target) FindConnectionPoints(PointBase[] route)
        {
            if (IsPortless) // Portless
            {
                if (SourceConnectorInfo.DataItem == null || (IsFullConnection && SinkConnectorInfoFully.DataItem == null))
                    return (null, null);

                var sourceCenter = SourceConnectorInfo.IsPortless ? SourceConnectorInfo.DataItem.MiddlePosition : SourceConnectorInfo.MiddlePosition;
                var targetCenter = SinkConnectorInfoFully?.IsPortless == true ? SinkConnectorInfoFully?.DataItem?.MiddlePosition ?? OnGoingPosition : SinkConnectorInfoFully?.MiddlePosition ?? OnGoingPosition;
                var firstPt = route.Length > 0 ? route[0] : targetCenter;
                var secondPt = route.Length > 0 ? route[0] : sourceCenter;
                var sourceLine = new LineBase(firstPt, sourceCenter);
                var targetLine = new LineBase(secondPt, targetCenter);
                var sourceIntersections = SourceConnectorInfo.DataItem.GetShape().GetIntersectionsWithLine(sourceLine);
                var targetIntersections = SinkConnectorInfoFully.DataItem.GetShape()?.GetIntersectionsWithLine(targetLine) ?? new PointBase[] { OnGoingPosition };
                var sourceIntersection = GetClosestPointTo(sourceIntersections, firstPt);
                var targetIntersection = GetClosestPointTo(targetIntersections, secondPt);

                return (sourceIntersection ?? sourceCenter, targetIntersection ?? targetCenter);
            }
            else
            {
                var source = SourceConnectorInfo.MiddlePosition;//GetPortPositionBasedOnAlignment(SourceConnectorInfo, ColorViewModel.LeftArrowSizeStyle);
                var target = SinkConnectorInfo.MiddlePosition;// GetPortPositionBasedOnAlignment(SinkConnectorInfoFully, ColorViewModel.RightArrowSizeStyle);
                return (source, target);
            }


        }

        private PointBase? GetPortPositionBasedOnAlignment(ConnectorInfoBase port, ArrowSizeStyle marker)
        {
            if (port == null)
                return null;

            if (marker == 0)
                return port.MiddlePosition;

            var pt = port.Position;
            switch (port.Orientation)
            {
                case ConnectorOrientation.Top:
                    return new PointBase(pt.X + port.ConnectorWidth / 2, pt.Y);
                case ConnectorOrientation.TopRight:
                    return new PointBase(pt.X + port.ConnectorWidth, pt.Y);
                case ConnectorOrientation.Right:
                    return new PointBase(pt.X + port.ConnectorWidth, pt.Y + port.ConnectorHeight / 2);
                case ConnectorOrientation.BottomRight:
                    return new PointBase(pt.X + port.ConnectorWidth, pt.Y + port.ConnectorHeight);
                case ConnectorOrientation.Bottom:
                    return new PointBase(pt.X + port.ConnectorWidth / 2, pt.Y + port.ConnectorHeight);
                case ConnectorOrientation.BottomLeft:
                    return new PointBase(pt.X, pt.Y + port.ConnectorHeight);
                case ConnectorOrientation.Left:
                    return new PointBase(pt.X, pt.Y + port.ConnectorHeight / 2);
                default:
                    return pt;
            }
        }

        private PointBase? GetClosestPointTo(IEnumerable<PointBase> points, PointBase point)
        {
            var minDist = double.MaxValue;
            PointBase? minPoint = null;

            foreach (var pt in points)
            {
                var dist = pt.DistanceTo(point);
                if (dist < minDist)
                {
                    minDist = dist;
                    minPoint = pt;
                }
            }

            return minPoint;
        }

        public void SetSourcePort(FullyCreatedConnectorInfo port)
        {
            SourceConnectorInfo = port;
        }

        public void SetSinkPort(FullyCreatedConnectorInfo port)
        {
            SinkConnectorInfo = port;
        }

        public void SetVisible(bool visible)
        {
            Visible = visible;
        }

        public double GetSourceMarkerWidth()
        {
            if (!IsFullConnection || string.IsNullOrEmpty(ShapeViewModel.SourceMarker.Path))
            {
                return 0;
            }
            return ShapeViewModel.SourceMarker.Width;
        }

        public double GetSourceMarkerHeight()
        {
            if (!IsFullConnection || string.IsNullOrEmpty(ShapeViewModel.SourceMarker.Path))
            {
                return 0;
            }
            return ShapeViewModel.SourceMarker.Height;
        }

        public double GetSinkMarkerWidth()
        {
            if (!IsFullConnection || string.IsNullOrEmpty(ShapeViewModel.SinkMarker.Path))
            {
                return 0;
            }
            return ShapeViewModel.SinkMarker.Width;
        }

        public double GetSinkMarkerHeight()
        {
            if (!IsFullConnection || string.IsNullOrEmpty(ShapeViewModel.SinkMarker.Path))
            {
                return 0;
            }
            return ShapeViewModel.SinkMarker.Height;
        }

        #region 双击添加
        private void AddVertex(object parameter)
        {
            MouseButtonEventArgs mosueArg = ((EventToCommandArgs)parameter).EventArgs as MouseButtonEventArgs;
            var position = mosueArg.GetPosition(((EventToCommandArgs)parameter).Sender as IInputElement);

            AddVertex(position, false);

            if (!((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control))
            {
                ShouldInsertAnchor = false;
            }
        }

        public void AddVertex(PointBase pointBase, bool absolute = true)
        {
            if (absolute)
            {
                pointBase = new PointBase(pointBase.X - Area.Left, pointBase.Y - Area.Top);
            }
            var vertice = new ConnectorVertexModel(this, pointBase);
            vertice.PropertyChanged += new WeakINPCEventHandler(ConnectorViewModel_PropertyChanged).Handler;
            Vertices.Add(vertice);
            UpdatePathGeneratorResult();
        }

        protected override void ExecuteEditCommand(object param)
        {
            AddLabel();
        }

        public void AddLabel(string text = null, double? distance = null, PointBase? offset = null)
        {
            var label = new ConnectorLabelModel(this, text?.ToString(), distance, offset);
            label.PropertyChanged += new WeakINPCEventHandler(ConnectorViewModel_PropertyChanged).Handler;
            label.IsSelected = true;
            Labels.Add(label);

            var paths = Labels.Count > 0 ? PathGeneratorResult?.Paths.Select(p => new SvgPath(p)).ToArray() : Array.Empty<SvgPath>();
            label.UpdatePosition(paths);
        }


        #endregion
    }
}
