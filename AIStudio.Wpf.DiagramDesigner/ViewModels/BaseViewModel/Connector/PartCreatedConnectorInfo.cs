﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using AIStudio.Wpf.DiagramDesigner.Geometrys;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class PartCreatedConnectorInfo : ConnectorInfoBase
    {
        public PartCreatedConnectorInfo(double X, double Y) : this(null, X, Y)
        {

        }

        public PartCreatedConnectorInfo(IDiagramViewModel root, double X, double Y) : base(root, ConnectorOrientation.None)
        {
            this.position = new PointBase(X, Y);
        }



        private PointBase position;
        public override PointBase Position
        {
            get
            {
                return position;
            }
        }


    }
}
