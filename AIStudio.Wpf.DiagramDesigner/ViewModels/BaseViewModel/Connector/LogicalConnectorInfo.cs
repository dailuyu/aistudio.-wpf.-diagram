﻿using System;
using System.Collections.Generic;
using System.Text;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class LogicalConnectorInfo : FullyCreatedConnectorInfo
    {
        public LogicalConnectorInfo(DesignerItemViewModelBase dataItem, ConnectorOrientation orientation, bool isInnerPoint = false, bool isPortless = false, ValueTypePoint valueTypePoint = ValueTypePoint.Real) : base(dataItem, orientation, isInnerPoint, isPortless)
        {
            this.ValueTypePoint = valueTypePoint;
        }

        public LogicalConnectorInfo(IDiagramViewModel root, DesignerItemViewModelBase dataItem, ConnectorOrientation orientation, bool isInnerPoint = false, bool isPortless = false, ValueTypePoint valueTypePoint = ValueTypePoint.Real) : base(root, dataItem, orientation, isInnerPoint, isPortless)
        {
            this.ValueTypePoint = valueTypePoint;
        }     

        public LogicalConnectorInfo(IDiagramViewModel root, DesignerItemViewModelBase dataItem, SelectableItemBase designer) : base(root, dataItem, designer)
        {
        }

        public LogicalConnectorInfo(IDiagramViewModel root, DesignerItemViewModelBase dataItem, SerializableItem serializableItem, string serializableType) : base(root, dataItem, serializableItem, serializableType)
        {
        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new LogicalConnectorInfoItem(this);
        }

        protected override void LoadDesignerItemViewModel(SelectableItemBase designerbase)
        {
            base.LoadDesignerItemViewModel(designerbase);

            if (designerbase is LogicalConnectorInfoItem designer)
            {
                ConnectorValue = designer.ConnectorValue;
                ValueTypePoint = designer.ValueTypePoint;
            }
        }

        public double _connectorValue;
        public double ConnectorValue
        {
            get
            {
                return _connectorValue;
            }
            set
            {
                SetProperty(ref _connectorValue, value);
            }
        }

        public ValueTypePoint _valueTypePoint;
        public ValueTypePoint ValueTypePoint
        {
            get
            {
                return _valueTypePoint;
            }
            set
            {
                SetProperty(ref _valueTypePoint, value);
            }
        }
    }
}
