﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner.Models;
using Newtonsoft.Json;

namespace AIStudio.Wpf.DiagramDesigner
{

    public interface ISelectItems
    {
        SimpleCommand SelectItemCommand
        {
            get;
        }
    }


    public abstract class SelectableDesignerItemViewModelBase : SelectableViewModelBase, ISelectItems, ISelectable, IGroupable
    {
        public SelectableDesignerItemViewModelBase() : this(null)
        {

        }

        public SelectableDesignerItemViewModelBase(IDiagramViewModel root) :base(root)
        {
       
        }

        public SelectableDesignerItemViewModelBase(IDiagramViewModel root, SelectableItemBase designer):base(root, designer)
        {
           
        }

        public SelectableDesignerItemViewModelBase(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        protected override void Init(IDiagramViewModel root)
        {
            base.Init(root);

            SelectItemCommand = new SimpleCommand(Command_Enable, ExecuteSelectItemCommand);
            EditCommand = new SimpleCommand(Command_Enable, ExecuteEditCommand);
        }

        protected override void LoadDesignerItemViewModel(SelectableItemBase designerbase)
        {
            base.LoadDesignerItemViewModel(designerbase);
        }

        public virtual bool InitData()
        {
            return true;
        }
        public virtual bool EditData()
        {
            return true;
        }

        public SimpleCommand SelectItemCommand
        {
            get; private set;
        }
        public ICommand EditCommand
        {
            get; private set;
        }

        private bool _isReadOnlyText = false;
        public bool IsReadOnlyText
        {
            get
            {
                if (IsReadOnly)
                    return true;
                return _isReadOnlyText;
            }
            set
            {
                SetProperty(ref _isReadOnlyText, value);
            }
        }

        //自己定义文本显示，文本框不显示
        private bool _customText = false;
        public bool CustomText
        {
            get
            {
                return _customText;
            }
            set
            {
                SetProperty(ref _customText, value);
            }
        }

        private bool _showText;
        public bool ShowText
        {
            get
            {
                return _showText;
            }
            set
            {
                if (!SetProperty(ref _showText, value))
                {
                    RaisePropertyChanged(nameof(ShowText));
                }
            }
        }

        protected override void ClearText()
        {
            ShowText = false;
        }

        private void ExecuteSelectItemCommand(object param)
        {
            SelectItem((bool)param, !IsSelected);
        }

        private void SelectItem(bool newselect, bool select)
        {
            if (newselect)
            {
                foreach (var designerItemViewModelBase in Root.SelectedItems.ToList())
                {
                    designerItemViewModelBase.ClearSelected();
                }
            }

            IsSelected = select;
        }

        public override void AddToSelection(bool selected)
        {
            foreach (SelectableDesignerItemViewModelBase item in Root.SelectedItems.ToList())
                item.IsSelected = false;

            Root.SelectedItems.Clear();
            if (selected == true)
            {
                Root.SelectionService.AddToSelection(this);
            }
        }

        protected virtual void ExecuteEditCommand(object param)
        {
            if (IsReadOnly == true) return;

            ShowText = true;
        }

        
    }
}
