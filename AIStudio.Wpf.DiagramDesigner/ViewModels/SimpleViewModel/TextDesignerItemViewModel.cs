﻿using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;
using System;
using System.Windows.Media;

namespace AIStudio.Wpf.DiagramDesigner
{
    /// <summary>
    /// DefaultTextNode
    /// </summary>
    public class TextDesignerItemViewModel : DesignerItemViewModelBase
    {
        public TextDesignerItemViewModel() : this(null)
        {

        }

        public TextDesignerItemViewModel(IDiagramViewModel root) : base(root)
        {

        }

        public TextDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public TextDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new TextDesignerItem(this);
        }

        protected override void Init(IDiagramViewModel root)
        {
            base.Init(root);

            CustomText = true;

            this.ItemWidth = 150;
            this.ClearConnectors();
        }

        private string _watermark = "请输入文本";
        public string Watermark
        {
            get
            {
                return _watermark;
            }
            set
            {
                SetProperty(ref _watermark, value);
            }
        }
    }
}
