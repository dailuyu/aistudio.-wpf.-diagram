﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public abstract class LogicalGateItemViewModelBase : DesignerItemViewModelBase
    {
        public SimpleCommand AddInputCommand { get; private set; }
        public SimpleCommand AddOutputCommand { get; private set; }


        public LogicalGateItemViewModelBase(LogicalType logicalType) : this(null, logicalType)
        {

        }

        public LogicalGateItemViewModelBase(IDiagramViewModel root, LogicalType logicalType) : base(root)
        {
            this.LogicalType = logicalType;

            if (this.LogicalType == LogicalType.Input)
            {
                ClearConnectors();
                ExecuteAddOutput(null);
            }
            else if (this.LogicalType == LogicalType.Output)
            {
                ClearConnectors();
                ExecuteAddInput(null);
            }
            else if (this.LogicalType == LogicalType.Constant)
            {
                ClearConnectors();
                ExecuteAddOutput(null);
            }
            else if (this.LogicalType == LogicalType.Time)
            {
                ClearConnectors();
                ExecuteAddOutput(null);
            }
            else if (this.LogicalType == LogicalType.None)
            {
                ClearConnectors();
                ExecuteAddOutput(null);
            }
            else if (this.LogicalType == LogicalType.NOT)
            {
                ClearConnectors();
                ExecuteAddInput(null);
                ExecuteAddOutput(null);
            }
            else if (this.LogicalType == LogicalType.SEL)
            {
                ClearConnectors();
                ExecuteAddInput(null, 0);
                ExecuteAddInput(null, 1);
                ExecuteAddInput(null, 2);
                ExecuteAddOutput(null, 0);
            }
            else if (this.LogicalType >= LogicalType.ABS && this.LogicalType <= LogicalType.EXPT)
            {
                ClearConnectors();
                ExecuteAddInput(null);
                ExecuteAddOutput(null);
            }
            else
            {
                ClearConnectors();
                ExecuteAddInput(null);
                ExecuteAddInput(null);
                ExecuteAddOutput(null);
            }
            BuildMenuOptions();
        }

        public LogicalGateItemViewModelBase(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {
            BuildMenuOptions();
        }

        public LogicalGateItemViewModelBase(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {
            BuildMenuOptions();
        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new LogicalGateDesignerItemBase(this);
        }

        protected override void Init(IDiagramViewModel root)
        {
            ShowRotate = false;
            ShowArrow = false;
            AddInputCommand = new SimpleCommand(Command_Enable, para => ExecuteAddInput(para));
            AddOutputCommand = new SimpleCommand(Command_Enable, para => ExecuteAddOutput(para));

            base.Init(root);
        }

        private void BuildMenuOptions()
        {
            bool enAddInput = false;
            bool enAddOutput = false;
            if (LogicalType >= LogicalType.ADD && LogicalType <= LogicalType.AVE)
            {
                enAddInput = true;
                enAddOutput = false;
            }
            else
            {
                enAddInput = false;
                enAddOutput = false;
            }

            menuOptions = new ObservableCollection<CinchMenuItem>();
            if (enAddInput == true)
            {
                CinchMenuItem menuItem = new CinchMenuItem();
                menuItem.Text = "添加输入";
                menuItem.Command = AddInputCommand;
                menuItem.CommandParameter = menuItem;
                menuOptions.Add(menuItem);
            }
            if (enAddOutput == true)
            {
                CinchMenuItem menuItem = new CinchMenuItem();
                menuItem.Text = "添加输出";
                menuItem.Command = AddOutputCommand;
                menuItem.CommandParameter = menuItem;
                menuOptions.Add(menuItem);
            }
        }

        protected override void LoadDesignerItemViewModel(SelectableItemBase designerbase)
        {
            base.LoadDesignerItemViewModel(designerbase);

            if (designerbase is LogicalGateDesignerItemBase designer)
            {
                this.LogicalType = designer.LogicalType;
                this.OrderNumber = designer.OrderNumber;
                this.Value = designer.Value;
                this.IsEnabled = designer.IsEnabled;

                ClearConnectors();
                Input.Clear();
                Output.Clear();

                foreach (var connector in designer.Connectors)
                {
                    LogicalConnectorInfo fullyCreatedConnectorInfo = new LogicalConnectorInfo(this.Root, this, connector);

                    if (fullyCreatedConnectorInfo.Orientation == ConnectorOrientation.Left)
                    {
                        Input.Add(Input.Count, fullyCreatedConnectorInfo);
                    }
                    else if (fullyCreatedConnectorInfo.Orientation == ConnectorOrientation.Right)
                    {
                        Output.Add(Output.Count, fullyCreatedConnectorInfo);
                    }
                    AddConnector(fullyCreatedConnectorInfo);
                }
            }
        }

        private int _orderNumber;
        public int OrderNumber
        {
            get
            {
                return _orderNumber;
            }
            set
            {
                SetProperty(ref _orderNumber, value);
            }
        }

        private bool _isEnabled;
        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                SetProperty(ref _isEnabled, value);
            }
        }


        private double _value;
        public double Value
        {
            get
            {
                return _value;
            }
            set
            {
                SetProperty(ref _value, value);
            }
        }

        public LogicalType LogicalType { get; set; }


        public Dictionary<int, LogicalConnectorInfo> Input { get; set; } = new Dictionary<int, LogicalConnectorInfo>();
        public Dictionary<int, LogicalConnectorInfo> Output { get; set; } = new Dictionary<int, LogicalConnectorInfo>();

        public virtual void ExecuteAddInput(object parameter, int index = 0)
        {
            if (Input.Values.Count >= 2)
            {
                this.ItemHeight = this.ItemHeight * (Input.Values.Count + 1) / Input.Values.Count;
            }
            LogicalConnectorInfo connector = new LogicalConnectorInfo(this, ConnectorOrientation.Left, true, false, ValueTypeInput.Count > index ? ValueTypeInput[index] : ValueTypeInput[0]);
            connector.XRatio = 0;
            Input.Add(Input.Count, connector);
            for (int i = 0; i < Input.Values.Count; i++)
            {
                Input[i].YRatio = (i + 1.0) / (Input.Values.Count + 1.0);
            }
            AddConnector(connector);
        }

        public virtual void ExecuteAddOutput(object parameter, int index = 0)
        {
            LogicalConnectorInfo connector = new LogicalConnectorInfo(this, ConnectorOrientation.Right, true, false, ValueTypeOutput.Count > index ? ValueTypeOutput[index] : ValueTypeInput[0]);
            connector.XRatio = 1;
            Output.Add(Output.Count, connector);
            for (int i = 0; i < Output.Values.Count; i++)
            {
                Output[i].YRatio = (i + 1.0) / (Output.Values.Count + 1.0);
            }
            AddConnector(connector);
        }

        public List<ValueTypePoint> ValueTypeInput
        {
            get
            {
                if (LogicalType == LogicalType.NOT)
                {
                    return new List<ValueTypePoint>() { ValueTypePoint.Bool };
                }
                else if (LogicalType == LogicalType.AND || LogicalType == LogicalType.OR || LogicalType == LogicalType.XOR
                    || LogicalType == LogicalType.SHL || LogicalType == LogicalType.SHR || LogicalType == LogicalType.ROL || LogicalType == LogicalType.ROR)
                {
                    return new List<ValueTypePoint>() { ValueTypePoint.Int };
                }
                else if (LogicalType == LogicalType.SEL)
                {
                    return new List<ValueTypePoint>() { ValueTypePoint.Bool, ValueTypePoint.Real, ValueTypePoint.Real };
                }
                else
                {
                    return new List<ValueTypePoint>() { ValueTypePoint.Real };
                }
            }
        }

        public List<ValueTypePoint> ValueTypeOutput
        {
            get
            {
                if (LogicalType == LogicalType.GT || LogicalType == LogicalType.LT || LogicalType == LogicalType.GE || LogicalType == LogicalType.LE || LogicalType == LogicalType.EQ || LogicalType == LogicalType.NE
                    || LogicalType == LogicalType.NOT)
                {
                    return new List<ValueTypePoint>() { ValueTypePoint.Bool };
                }
                else if (LogicalType == LogicalType.AND || LogicalType == LogicalType.OR || LogicalType == LogicalType.XOR
                    || LogicalType == LogicalType.SHL || LogicalType == LogicalType.SHR || LogicalType == LogicalType.ROL || LogicalType == LogicalType.ROR)
                {
                    return new List<ValueTypePoint>() { ValueTypePoint.Int };
                }
                else
                {
                    return new List<ValueTypePoint>() { ValueTypePoint.Real };
                }
            }
        }
    }


}
