﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class MediaItemViewModel : DesignerItemViewModelBase
    {
        protected virtual string Filter { get; set; } = "媒体·|*.*";

        public MediaItemViewModel() : this(null)
        {
 
        }

        public MediaItemViewModel(IDiagramViewModel root) : base(root)
        {

        }

        public MediaItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public MediaItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new MediaDesignerItem(this);
        }

        protected override void Init(IDiagramViewModel root)
        {
            base.Init(root);

            BuildMenuOptions();
        }      


        protected override void LoadDesignerItemViewModel(SelectableItemBase designerbase)
        {
            base.LoadDesignerItemViewModel(designerbase);

            if (designerbase is MediaDesignerItem designer)
            {
                this.Icon = designer.Icon;

                foreach (var connector in designer.Connectors)
                {
                    FullyCreatedConnectorInfo fullyCreatedConnectorInfo = new FullyCreatedConnectorInfo(this.Root, this, connector);
                    AddConnector(fullyCreatedConnectorInfo);
                }
            }
        }

        private void BuildMenuOptions()
        {
            menuOptions = new ObservableCollection<CinchMenuItem>();
            CinchMenuItem menuItem = new CinchMenuItem();
            menuItem.Text = "更换";
            menuItem.Command = MenuItemCommand;
            menuItem.CommandParameter = menuItem;
            menuOptions.Add(menuItem);
        }

        private SimpleCommand _menuItemCommand;
        public SimpleCommand MenuItemCommand
        {
            get
            {
                return this._menuItemCommand ?? (this._menuItemCommand = new SimpleCommand(Command_Enable, ExecuteMenuItemCommand));
            }
        }

        private void ExecuteMenuItemCommand(object obj)
        {
            EditData();
        }

        public override bool InitData()
        {
            if (string.IsNullOrEmpty(Icon))
                return EditData();
            return true;
        }
        public override bool EditData()
        {
            Microsoft.Win32.OpenFileDialog openFile = new Microsoft.Win32.OpenFileDialog();
            openFile.Filter = Filter;

            if (openFile.ShowDialog() == true)
            {
                Icon = openFile.FileName;
                return true;
            }

            return false;
        }

        protected override void ExecuteEditCommand(object param)
        {
        
        }
    }
}
