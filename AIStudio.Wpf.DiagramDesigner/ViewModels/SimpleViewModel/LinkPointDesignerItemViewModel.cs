﻿using System;
using System.Windows;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class LinkPointDesignerItemViewModel : DesignerItemViewModelBase
    {
        public LinkPointDesignerItemViewModel(Point location) : this(null, location)
        {

        }

        public LinkPointDesignerItemViewModel(IDiagramViewModel root, Point location) : base(root)
        {
            Left = Math.Max(0, location.X - ItemWidth / 2);
            Top = Math.Max(0, location.Y - ItemHeight / 2);
        }

        protected override void Init(IDiagramViewModel root)
        {
            base.Init(root);

            this.ClearConnectors();
            this.AddConnector(new FullyCreatedConnectorInfo(this, ConnectorOrientation.None, true));

            ItemWidth = 5;
            ItemHeight = 5;
        }

        public Point CurrentLocation
        {
            get
            {
                return new Point() { X = Left + ItemWidth / 2, Y = Top + ItemHeight / 2 };
            }
        }
    }
}
