﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace AIStudio.Wpf.DiagramDesigner
{
    public interface IDiagramViewModel
    {
        string Name
        {
            get; set;
        }
        List<SelectableDesignerItemViewModelBase> SelectedItems
        {
            get;
        }
        SelectableDesignerItemViewModelBase SelectedItem
        {
            get;
        }
        ObservableCollection<SelectableDesignerItemViewModelBase> Items
        {
            get;
        }
        SelectionService SelectionService
        {
            get;
        }

        SimpleCommand CreateNewDiagramCommand
        {
            get;
        }
        SimpleCommand DirectAddItemCommand
        {
            get;
        }
        SimpleCommand AddItemCommand
        {
            get;
        }
        SimpleCommand RemoveItemCommand
        {
            get;
        }
        SimpleCommand DirectRemoveItemCommand
        {
            get;
        }
        SimpleCommand ClearSelectedItemsCommand
        {
            get;
        }
        SimpleCommand AlignTopCommand
        {
            get;
        }
        SimpleCommand AlignVerticalCentersCommand
        {
            get;
        }
        SimpleCommand AlignBottomCommand
        {
            get;
        }
        SimpleCommand AlignLeftCommand
        {
            get;
        }
        SimpleCommand AlignHorizontalCentersCommand
        {
            get;
        }
        SimpleCommand AlignRightCommand
        {
            get;
        }
        SimpleCommand BringForwardCommand
        {
            get;
        }
        SimpleCommand BringToFrontCommand
        {
            get;
        }
        SimpleCommand SendBackwardCommand
        {
            get;
        }
        SimpleCommand SendToBackCommand
        {
            get;
        }

        SimpleCommand DistributeHorizontalCommand
        {
            get;
        }
        SimpleCommand DistributeVerticalCommand
        {
            get;
        }
        SimpleCommand SelectAllCommand
        {
            get;
        }
        SimpleCommand SelectItemCommand
        {
            get;
        }
        SimpleCommand CopyCommand
        {
            get;
        }
        SimpleCommand PasteCommand
        {
            get;
        }
        SimpleCommand CutCommand
        {
            get;
        }
        SimpleCommand DeleteCommand
        {
            get;
        }
        SimpleCommand LeftMoveCommand
        {
            get;
        }
        SimpleCommand RightMoveCommand
        {
            get;
        }
        SimpleCommand UpMoveCommand
        {
            get;
        }
        SimpleCommand DownMoveCommand
        {
            get;
        }
        SimpleCommand CenterMoveCommand
        {
            get;
        }
        SimpleCommand SameSizeCommand
        {
            get;
        }
        SimpleCommand SameWidthCommand
        {
            get;
        }
        SimpleCommand SameHeightCommand
        {
            get;
        }
        SimpleCommand SameAngleCommand
        {
            get;
        }
        SimpleCommand GroupCommand
        {
            get;
        }
        SimpleCommand UngroupCommand
        {
            get;
        }
        SimpleCommand LockCommand
        {
            get;
        }
        SimpleCommand UnlockCommand
        {
            get;
        }

        SimpleCommand UndoCommand
        {
            get;
        }
        SimpleCommand RedoCommand
        {
            get;
        }

        event DiagramEventHandler Event;

        Func<SelectableDesignerItemViewModelBase, bool> OutAddVerify
        {
            get; set;
        }
        //void ClearSelectedItems();
        //bool BelongToSameGroup(IGroupable item1, IGroupable item2);
        //Rectangle GetBoundingRectangle(IEnumerable<DesignerItemViewModelBase> items);
        //void UpdateZIndex();

        bool IsReadOnly
        {
            get; set;
        }
        bool IsLoading
        {
            get; set;
        }
        Size PageSize
        {
            get; set;
        }
        Size PhysicalPageSize
        {
            get; set;
        }
        PageSizeType PageSizeType
        {
            get; set;
        }
        bool ShowGrid
        {
            get; set;
        }
        Size GridCellSize
        {
            get; set;
        }
        Size PhysicalGridCellSize
        {
            get; set;
        }
        PageSizeOrientation PageSizeOrientation
        {
            get; set;
        }
        CellHorizontalAlignment CellHorizontalAlignment
        {
            get; set;
        }
        CellVerticalAlignment CellVerticalAlignment
        {
            get; set;
        }
        Size GridMarginSize
        {
            get; set;
        }
        Size PhysicalGridMarginSize
        {
            get; set;
        }
        Color GridColor
        {
            get; set;
        }
        DiagramType DiagramType
        {
            get; set;
        }
        double ZoomValue
        {
            get; set;
        }
        double MaximumZoomValue
        {
            get; set;
        }
        double MinimumZoomValue
        {
            get; set;
        }
        bool DefaultZoomBox
        {
            get; set;
        }
        bool AllowDrop
        {
            get; set;
        } 
        System.Windows.Point CurrentPoint
        {
            get; set;
        }
        Color CurrentColor
        {
            get; set;
        }
        #region 如果这个赋值了，优先用这个的
        IDrawModeViewModel DrawModeViewModel
        {
            get; set;
        }
        IColorViewModel ColorViewModel
        {
            get; set;
        }
        IFontViewModel FontViewModel
        {
            get; set;
        }
        IShapeViewModel ShapeViewModel
        {
            get; set;
        }
        #endregion

        #region 设置选项
        DiagramOption DiagramOption
        {
            get; set;
        }
        #endregion

        bool ExecuteShortcut(KeyEventArgs e);

        event PropertyChangedEventHandler PropertyChanged;

    }

    public delegate void DiagramEventHandler(object sender, DiagramEventArgs e);

    public class DiagramEventArgs : PropertyChangedEventArgs
    {
        public DiagramEventArgs(string propertyName, object oldValue, object newValue, Guid? id) : base(propertyName)
        {
            OldValue = oldValue;
            NewValue = newValue;
            Id = id;
        }

        public object OldValue
        {
            get; set;
        }
        public object NewValue
        {
            get; set;
        }
        public Guid? Id
        {
            get; set;
        }

    }
}
