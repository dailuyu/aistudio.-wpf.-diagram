﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIStudio.Wpf.DiagramDesigner
{
    /// <summary>
    /// Interaction logic for DiagramControl.xaml
    /// </summary>
    public partial class DiagramControl : UserControl
    {
        public DiagramControl()
        {
            InitializeComponent();

            if (ResourceDictionary != null)
            {
                this.Resources.MergedDictionaries.Add(ResourceDictionary);
            }

            this.Focusable = true;
        }

        public static readonly DependencyProperty ResourceDictionaryProperty = DependencyProperty.Register(nameof(ResourceDictionary), typeof(ResourceDictionary), typeof(DiagramControl), new UIPropertyMetadata(null, OnResourceDictionaryChanged));

        private static void OnResourceDictionaryChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DiagramControl diagram)
            {
                if (e.NewValue is ResourceDictionary dictionary)
                {
                    diagram.Resources.MergedDictionaries.Add(dictionary);
                }
            }
        }

        public ResourceDictionary ResourceDictionary
        {
            get
            {
                return (ResourceDictionary)GetValue(ResourceDictionaryProperty);
            }
            set
            {
                SetValue(ResourceDictionaryProperty, value);
            }
        }

        private IDiagramViewModel DiagramViewModel
        {
            get
            {
                return DataContext as IDiagramViewModel;
            }
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            base.OnPreviewKeyDown(e);

            e.Handled = DiagramViewModel.ExecuteShortcut(e);
        }

        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            base.OnPreviewMouseWheel(e);

            if (Keyboard.IsKeyDown(Key.LeftCtrl) == false
               && Keyboard.IsKeyDown(Key.RightCtrl) == false)
            {
                return;
            }

            var newZoomValue = DiagramViewModel.ZoomValue + (e.Delta > 0 ? 0.1 : -0.1);

            DiagramViewModel.ZoomValue = Math.Max(Math.Min(newZoomValue, DiagramViewModel.MaximumZoomValue), DiagramViewModel.MinimumZoomValue);

            e.Handled = true;
        }
    }
}
