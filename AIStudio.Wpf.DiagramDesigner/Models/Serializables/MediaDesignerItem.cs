﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class MediaDesignerItem : DesignerItemBase
    {
        public MediaDesignerItem()
        {

        }

        public MediaDesignerItem(GifImageItemViewModel item) : base(item)
        {
            Connectors = new List<FullyCreatedConnectorInfoItem>();
            foreach (var fullyCreatedConnectorInfo in item.Connectors)
            {
                FullyCreatedConnectorInfoItem connector = new FullyCreatedConnectorInfoItem(fullyCreatedConnectorInfo);
                Connectors.Add(connector);
            }
        }

        public MediaDesignerItem(MediaItemViewModel item) : base(item)
        {
            Connectors = new List<FullyCreatedConnectorInfoItem>();
            foreach (var fullyCreatedConnectorInfo in item.Connectors)
            {
                FullyCreatedConnectorInfoItem connector = new FullyCreatedConnectorInfoItem(fullyCreatedConnectorInfo);
                Connectors.Add(connector);
            }
        }

        [XmlArray]
        public List<FullyCreatedConnectorInfoItem> Connectors { get; set; }

    }
}
