﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class LogicalGateDesignerItemBase : DesignerItemBase
    {
        public LogicalGateDesignerItemBase()
        {

        }
        public LogicalGateDesignerItemBase(LogicalGateItemViewModelBase item) : base(item)
        {
            this.Connectors = new List<LogicalConnectorInfoItem>();
            foreach (var fullyCreatedConnectorInfo in item.Connectors.OfType<LogicalConnectorInfo>())
            {
                LogicalConnectorInfoItem connector = new LogicalConnectorInfoItem(fullyCreatedConnectorInfo);
                this.Connectors.Add(connector);
            }
            this.OrderNumber = item.OrderNumber;
            this.LogicalType = item.LogicalType;
            this.Value = item.Value;
            this.IsEnabled = item.IsEnabled;
        }

        [XmlArray]
        public List<LogicalConnectorInfoItem> Connectors { get; set; }

        [XmlAttribute]
        public int OrderNumber { get; set; }

        [XmlAttribute]
        public double Value { get; set; }

        [XmlAttribute]
        public LogicalType LogicalType { get; set; }

        [XmlAttribute]
        public bool IsEnabled { get; set; }

    }
}
