﻿using AIStudio.Wpf.DiagramDesigner;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Xml.Serialization;

namespace AIStudio.Wpf.DiagramDesigner
{
    /// <summary>
    /// 连接线
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(ConnectionItem))]
    public class ConnectionItem : SelectableItemBase
    {
        public ConnectionItem()
        {

        }

        public ConnectionItem(ConnectionViewModel viewmodel) : base(viewmodel)
        {
            this.SourceId = viewmodel.SourceConnectorInfo.DataItem.Id;
            this.SourceOrientation = viewmodel.SourceConnectorInfo.Orientation;
            this.SourceType = viewmodel.SourceConnectorInfo.DataItem.GetType();
            this.SourceTypeName = viewmodel.SourceConnectorInfo.DataItem.GetType().FullName;
            this.SourceXRatio = viewmodel.SourceConnectorInfo.GetXRatioFromConnector();
            this.SourceYRatio = viewmodel.SourceConnectorInfo.GetYRatioFromConnector();
            this.SourceInnerPoint = viewmodel.SourceConnectorInfo.IsInnerPoint;
            this.SourceIsPortless = viewmodel.SourceConnectorInfo.IsPortless;

            this.SinkId = viewmodel.SinkConnectorInfoFully.DataItem.Id;
            this.SinkOrientation = viewmodel.SinkConnectorInfoFully.Orientation;
            this.SinkType = viewmodel.SinkConnectorInfoFully.DataItem.GetType();
            this.SinkTypeName = viewmodel.SinkConnectorInfoFully.DataItem.GetType().FullName;
            this.SinkXRatio = viewmodel.SinkConnectorInfoFully.GetXRatioFromConnector();
            this.SinkYRatio = viewmodel.SinkConnectorInfoFully.GetYRatioFromConnector();
            this.SinkInnerPoint = viewmodel.SinkConnectorInfoFully.IsInnerPoint;
            this.SinkIsPortless = viewmodel.SourceConnectorInfo.IsPortless;

            this.RouterMode = viewmodel.RouterMode;
            this.PathMode = viewmodel.PathMode;
            this.Vertices = viewmodel.Vertices.Select(p => new ConnectorVertexItem(p)).ToList();
            this.Labels = viewmodel.Labels.Select(p => new ConnectorLabelItem(p)).ToList();
        }


        [XmlAttribute]
        public Guid SourceId { get; set; }

        [XmlAttribute]
        public ConnectorOrientation SourceOrientation { get; set; }

        [XmlIgnore]
        public Type SourceType { get; set; }

        [XmlAttribute]
        public string SourceTypeName { get; set; }

        [XmlAttribute]
        public double SourceXRatio { get; set; }

        [XmlAttribute]
        public double SourceYRatio { get; set; }

        [XmlAttribute]
        public bool SourceInnerPoint { get; set; }

        [XmlAttribute]
        public bool SourceIsPortless
        {
            get; set;
        }

        [XmlAttribute]
        public Guid SinkId { get; set; }

        [XmlAttribute]
        public ConnectorOrientation SinkOrientation { get; set; }

        [XmlIgnore]
        public Type SinkType { get; set; }

        [XmlAttribute]
        public string SinkTypeName { get; set; }

        [XmlAttribute]
        public double SinkXRatio { get; set; }

        [XmlAttribute]
        public double SinkYRatio { get; set; }

        [XmlAttribute]
        public bool SinkInnerPoint { get; set; }

        [XmlAttribute]
        public bool SinkIsPortless
        {
            get; set;
        }

        [XmlAttribute]
        public string RouterMode
        {
            get; set;
        }

        [XmlAttribute]
        public string PathMode
        {
            get; set;
        }

        [XmlArray]
        public List<ConnectorVertexItem> Vertices
        {
            get; set;
        }

        [XmlArray]
        public List<ConnectorLabelItem> Labels
        {
            get; set;
        }
    }
}
