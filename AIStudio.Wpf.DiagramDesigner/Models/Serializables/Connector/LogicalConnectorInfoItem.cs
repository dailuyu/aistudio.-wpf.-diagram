﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace AIStudio.Wpf.DiagramDesigner
{  
    /// <summary>
     /// 完整连接点
     /// </summary>
    [Serializable]
    [XmlInclude(typeof(LogicalConnectorInfoItem))]
    public class LogicalConnectorInfoItem : FullyCreatedConnectorInfoItem
    {

        public LogicalConnectorInfoItem()
        {

        }

        public LogicalConnectorInfoItem(LogicalConnectorInfo viewmodel) : base(viewmodel)
        {
            ValueTypePoint = viewmodel.ValueTypePoint;
            ConnectorValue = viewmodel.ConnectorValue;
        }

        [XmlAttribute]
        public ValueTypePoint ValueTypePoint
        {
            get; set;
        }

        [XmlAttribute]
        public double ConnectorValue
        {
            get; set;
        }
    }

 
}
