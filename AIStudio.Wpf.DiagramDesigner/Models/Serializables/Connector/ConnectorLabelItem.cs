﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace AIStudio.Wpf.DiagramDesigner
{
    /// <summary>
    /// 连接点文字
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(ConnectorLabelItem))]
    public class ConnectorLabelItem : ConnectorPointItem
    {
        public ConnectorLabelItem()
        {

        }

        public ConnectorLabelItem(ConnectorLabelModel viewmodel) : base(viewmodel)
        {
            Distance = viewmodel.Distance;
            Offset = viewmodel.Offset;
        }

        [XmlIgnore]
        public double? Distance
        {
            get; set;
        }

        [JsonIgnore]
        [XmlAttribute("Distance")]
        public string XmlDistance
        {
            get
            {
                return SerializeHelper.SerializeDoubleNull(Distance);
            }
            set
            {
                Distance = SerializeHelper.DeserializeDoubleNull(value);
            }
        }

        [XmlIgnore]
        public Point Offset
        {
            get; set;
        }

        [JsonIgnore]
        [XmlAttribute("Offset")]
        public string XmlOffset
        {
            get
            {
                return SerializeHelper.SerializePoint(Offset);
            }
            set
            {
                Offset = SerializeHelper.DeserializePoint(value);
            }
        }
    }
}
