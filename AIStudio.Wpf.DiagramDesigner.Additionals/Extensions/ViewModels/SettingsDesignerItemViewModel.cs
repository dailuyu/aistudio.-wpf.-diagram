﻿using System;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;
using AIStudio.Wpf.DiagramDesigner.Services;
using AIStudio.Wpf.DiagramDesigner.Additionals.Extensions.Models;

namespace AIStudio.Wpf.DiagramDesigner.Additionals.Extensions.ViewModels
{
    public class SettingsDesignerItemViewModel : DesignerItemViewModelBase
    {
        private IUIVisualizerService visualiserService;

        public SettingsDesignerItemViewModel() : this(null)
        {

        }

        public SettingsDesignerItemViewModel(IDiagramViewModel root) : base(root)
        {

        }

        public SettingsDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {
           
        }

        public SettingsDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new SettingsDesignerItem(this);
        }

        protected override void Init(IDiagramViewModel root)
        {
            base.Init(root);

            visualiserService = ApplicationServicesProvider.Instance.Provider.VisualizerService;
            this.ShowConnectors = false;
        }

       protected override void LoadDesignerItemViewModel(SelectableItemBase designerbase)
        {
            base.LoadDesignerItemViewModel(designerbase);

            if (designerbase is SettingsDesignerItem designer)
            {
                this.Setting = designer.Setting;
            }
        }

        public String Setting{ get; set; }

        protected override void ExecuteEditCommand(object parameter)
        {
            SettingsDesignerItemData data = new SettingsDesignerItemData(Setting);
            if (visualiserService.ShowDialog(data) == true)
            {
                this.Setting = data.Setting1;
            }
        }


    }
}
