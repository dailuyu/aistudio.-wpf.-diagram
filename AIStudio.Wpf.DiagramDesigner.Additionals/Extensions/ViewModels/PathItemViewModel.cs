﻿using System;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;
using AIStudio.Wpf.DiagramDesigner.Additionals.Extensions.Models;

namespace AIStudio.Wpf.DiagramDesigner.Additionals.Extensions.ViewModels
{
    public class PathItemViewModel : DesignerItemViewModelBase
    {
        public PathItemViewModel() : this(null)
        {

        }

        public PathItemViewModel(IDiagramViewModel root) : base(root)
        {

        }

        public PathItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {
           
        }

        public PathItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new PathDesignerItem(this);
        }

        protected override void Init(IDiagramViewModel root)
        {
            base.Init(root);

            this.ShowConnectors = false;
        }
    }
}
