本画板在WPF-Diagram-Designer的基础上进行的开发，界面框架使用Fluent.Ribbon的框架。

先上源码地址：https://gitee.com/akwkevin/aistudio.-wpf.-diagram

### 2023年2月5号更新内容：
本次更新主要参照了一个Blazor的Diagram的画线算法，链接地址:[https://github.com/Blazor-Diagrams/Blazor.Diagrams](https://github.com/Blazor-Diagrams/Blazor.Diagrams),感谢作者。

### 1.连线改进，新增连线算法，目前共4种连线：Smooth（曲线），Straight（直线），Boundary（网格边界连接模式），Corner（折线）
### 2.序列化改进，xml与json序列化，新增自定义元素后，无需更改根元素，只需要在新增的元素上添加序列化的对象即可，扩展性更灵活了。
### 3.箭头改进，箭头按照连线的实际角度显示（即0-360度），还支持自定义的箭头path。
### 4.新增快捷键自定义扩展，用户可根据自己的习惯定义快捷键。
### 5.封装了一个标准的工作流控件FlowchartEditor，具体使用可以参照开源权限管理框架种的用法： https://gitee.com/akwkevin/aistudio.-wpf.-aclient

nuget地址：![输入图片说明](nuget.png)

### 6.连接上添加动画：路径动画效果和线条流动效果。
### 7.改变结构，使用户更容易自定义自己的样式，覆盖系统默认样式。
### 8.从Blazor.Diagrams种引入PortlessLinks（直接连接两个node，不需要port），自动连接节点Snapping，按距离最近连接ReconnectLinksToClosestPorts
### 9.新增Demo示例，帮助用户快速上手，见底部2023年2月5号更新附加说明。

### 2023年之前发布内容：
界面图：

本画板在WPF-Diagram-Designer进行的开发，界面框架使用Fluent.Ribbon的框架。

界面图：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/095141_dbfbe652_4799126.png "4.png")

1.支持字体样式，字体颜色，字体阴影，对齐方向，行间距。

2.支持复制粘贴剪贴，格式化，撤销重做。

3.支持形状绘制。

4.连接线（部分完成，完善中）

5.位置，组合，对齐

6.元素翻转，旋转。

7.填充颜色，支持线性渐变色，径向渐变色等

8.支持箭头样式（部分完成，完善中）

9.锁定与解锁

10.快速样式

11.支持矢量文本，二维码

12.支持插入图片，视频，SVG

13.支持画板大小，方向，标尺，网格是否显示，画板背景色

14.支持流程图（在文件新建下-基本绘图-流程图）

![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/095156_62819165_4799126.png "1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/095209_6cdce752_4799126.png "2.png")

15支持逻辑图（在文件新建下-基本绘图-逻辑图）

![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/095226_36478a7b_4799126.png "3.png")

16支持SFC顺序控制图（在文件新建下-基本绘图-顺序控制图）

![输入图片说明](https://images.gitee.com/uploads/images/2021/0802/215025_4aa32824_4799126.png "2 (2).png")

### 2023年2月5号更新附加说明
示例项目（AIStudio.Wpf.DiagramDesigner.Demo）目录如下：

- 1 Simple 简单示例

![输入图片说明](AIStudio.Wpf.DiagramDesigner.Demo/image.png)
- 2 Locked 锁定节点
- 3 Events 事件（暂未完成，敬请期待）
- 4 DynamicInsertions 动态插入（暂未完成，敬请期待）
- 5 Performance 性能（100个节点生成）
- 6 Zoom 放大缩小
- 7 SnapToGrid 对齐到网格

![输入图片说明](AIStudio.Wpf.DiagramDesigner.Demo/snaptogrid.png)
- 8 DragAndDrop 拖拽

![输入图片说明](AIStudio.Wpf.DiagramDesigner.Demo/draganddrop.png)
- 9 Nodes 节点示例
- - 9.1 Svg svg样式

![输入图片说明](AIStudio.Wpf.DiagramDesigner.Demo/svg.png)
- - 9.2 CustomDefinedNode 自定义节点

![输入图片说明](customdefinednode.png)
- - 9.3 PortlessLinks 无Port的node连接

![输入图片说明](portlesslink.png)
- - 9.4 GradientNode 渐变色node

![输入图片说明](gradinetnode.png)
- - 9.5 Rotate 旋转node（连接线还需要优化，还算连接在旋转之前的位置上）

![输入图片说明](rotate.png)
- 10 Links 连线示例
- - 10.1 Snapping 连接线靠近节点自动连接
- - 10.2 Labels 连接线上的文字（支持多处）

![输入图片说明](labels.png)
- - 10.3 Vertices 连接线上的中间节点

![输入图片说明](vertices.png)
- - 10.4 Markers 箭头，支持自定义

![输入图片说明](markers.png)
- - 10.5 Routers 连线模式

![输入图片说明](routers.png)
- - 10.6 PathGenerators 连线算法

![输入图片说明](pathgenerators.png)
- 11 Ports 连接点示例
- - 11.1 ColoredPort 彩色连接点，相同颜色的连接点才能连接

![输入图片说明](coloredport.png)
- - 11.2 InnerPort 内部连接点

![输入图片说明](innerport.png)
- 12 Groups 分组示例
- - 12.1 Group 分组
- - 12.2 CustomDefinedGroup 自定义分组
- - 12.3 CustomShortcutGroup 自定义分组快捷键
- 13 Texts 文本节点示例
- - 13.1 Text 文本
- - 13.2 Alignment 对齐方式
- - 13.3 FontSize 字体大小
- - 13.4 ColorText 彩色字体

![输入图片说明](colortext.png)
- - 13.5 OutlineText 轮廓文本
- 14 Customization 自定义
- - 14.1 CustomNode 覆盖默认节点样式

![输入图片说明](customnode.png)
- - 14.2 CustomLink 设置线条连接样式

![输入图片说明](customlink.png)
- - 14.3 CustomPort 覆盖默认连接点样式

![输入图片说明](customport.png)
- - 14.4 CustomGroup 覆盖默认分组样式

![输入图片说明](customgroup.png)
- 15 Algorithms 算法
- - 14.6 ReconnectLinksToClosestPorts 重新计算，按最近的连接点连接。
- 15 Animations
- - 15.1 PathAnimation 动画路径
![输入图片说明](AIStudio.Wpf.DiagramDesigner.Demo/11.gif)
- - 15.2 LineAnimation 线条流动动画
![输入图片说明](AIStudio.Wpf.DiagramDesigner.Demo/22.gif)
- 16 Editor
- - 16.1 FlowchartEditor 工作流封装控件
采用兼容主流的diagram的序列化格式

![输入图片说明](flowcharteditor.png)

`{"Nodes":[{"Kind":1,"UserIds":null,"RoleIds":null,"ActType":null,"Id":"e0f2c29c-2c89-4c0c-857e-35eb0b121d7e","ParentId":null,"Name":null,"Color":"#1890ff","Label":"开始","Width":100.0,"Height":80.0,"X":12.5,"Y":147.5,"Type":"FlowchartNode","ZIndex":0,"PortAlignmentList":["Top","Bottom","Left","Right","Top","Bottom","Left","Right"]},{"Kind":3,"UserIds":[],"RoleIds":[],"ActType":null,"Id":"716f64ec-bcdb-438c-9748-9546abf990cc","ParentId":null,"Name":null,"Color":"#1890ff","Label":"节点1","Width":100.0,"Height":80.0,"X":137.5,"Y":147.5,"Type":"FlowchartNode","ZIndex":2,"PortAlignmentList":["Top","Bottom","Left","Right","Top","Bottom","Left","Right"]},{"Kind":4,"UserIds":null,"RoleIds":null,"ActType":null,"Id":"3cd6c332-6b5b-44ef-96c4-c7aef66fd5dd","ParentId":null,"Name":null,"Color":"#1890ff","Label":"条件节点","Width":100.0,"Height":80.0,"X":262.5,"Y":147.5,"Type":"FlowchartNode","ZIndex":3,"PortAlignmentList":["Top","Bottom","Left","Right","Top","Bottom","Left","Right"]},{"Kind":3,"UserIds":[],"RoleIds":[],"ActType":null,"Id":"7d953234-ddff-4701-a52a-bf6460ffa7b9","ParentId":null,"Name":null,"Color":"#1890ff","Label":"节点2","Width":100.0,"Height":80.0,"X":387.5,"Y":22.5,"Type":"FlowchartNode","ZIndex":6,"PortAlignmentList":["Top","Bottom","Left","Right","Top","Bottom","Left","Right"]},{"Kind":3,"UserIds":[],"RoleIds":[],"ActType":null,"Id":"7dfd4102-2751-42c7-a386-adcfcca27ede","ParentId":null,"Name":null,"Color":"#1890ff","Label":"节点3","Width":100.0,"Height":80.0,"X":387.5,"Y":272.5,"Type":"FlowchartNode","ZIndex":7,"PortAlignmentList":["Top","Bottom","Left","Right","Top","Bottom","Left","Right"]},{"Kind":2,"UserIds":null,"RoleIds":null,"ActType":null,"Id":"ad57e53f-8860-4212-9afb-f67e14eecbc8","ParentId":null,"Name":null,"Color":"#1890ff","Label":"结束","Width":100.0,"Height":80.0,"X":512.5,"Y":147.5,"Type":"FlowchartNode","ZIndex":10,"PortAlignmentList":["Top","Bottom","Left","Right","Top","Bottom","Left","Right"]}],"Links":[{"Id":"65f6432f-2084-462d-93d8-a6b3ff889182","Color":"#FF808080","SelectedColor":"#FF000000","Width":2.0,"Label":null,"SourceId":"e0f2c29c-2c89-4c0c-857e-35eb0b121d7e","TargetId":"716f64ec-bcdb-438c-9748-9546abf990cc","SourcePortAlignment":"Right","TargetPortAlignment":"Left","Type":"DiagramLink","Router":null,"PathGenerator":null,"SourceMarkerPath":null,"SourceMarkerWidth":null,"TargetMarkerPath":null,"TargetMarkerWidth":null},{"Id":"7d1dcf2d-ee69-4c24-84ff-3a99b6555692","Color":"#FF808080","SelectedColor":"#FF000000","Width":2.0,"Label":null,"SourceId":"716f64ec-bcdb-438c-9748-9546abf990cc","TargetId":"3cd6c332-6b5b-44ef-96c4-c7aef66fd5dd","SourcePortAlignment":"Right","TargetPortAlignment":"Left","Type":"DiagramLink","Router":null,"PathGenerator":null,"SourceMarkerPath":null,"SourceMarkerWidth":null,"TargetMarkerPath":null,"TargetMarkerWidth":null},{"Id":"cd18c02f-0cdb-4eb5-9793-b9db87eeea09","Color":"#FF808080","SelectedColor":"#FF000000","Width":2.0,"Label":"条件1","SourceId":"3cd6c332-6b5b-44ef-96c4-c7aef66fd5dd","TargetId":"7d953234-ddff-4701-a52a-bf6460ffa7b9","SourcePortAlignment":"Top","TargetPortAlignment":"Left","Type":"DiagramLink","Router":null,"PathGenerator":null,"SourceMarkerPath":null,"SourceMarkerWidth":null,"TargetMarkerPath":null,"TargetMarkerWidth":null},{"Id":"69bbb083-8eb4-403b-937a-b0f0d3c80eb0","Color":"#FF808080","SelectedColor":"#FF000000","Width":2.0,"Label":"条件2","SourceId":"3cd6c332-6b5b-44ef-96c4-c7aef66fd5dd","TargetId":"7dfd4102-2751-42c7-a386-adcfcca27ede","SourcePortAlignment":"Bottom","TargetPortAlignment":"Left","Type":"DiagramLink","Router":null,"PathGenerator":null,"SourceMarkerPath":null,"SourceMarkerWidth":null,"TargetMarkerPath":null,"TargetMarkerWidth":null},{"Id":"d640c547-5ba8-428c-8d65-74874b1d28bd","Color":"#FF808080","SelectedColor":"#FF000000","Width":2.0,"Label":null,"SourceId":"7d953234-ddff-4701-a52a-bf6460ffa7b9","TargetId":"ad57e53f-8860-4212-9afb-f67e14eecbc8","SourcePortAlignment":"Right","TargetPortAlignment":"Top","Type":"DiagramLink","Router":null,"PathGenerator":null,"SourceMarkerPath":null,"SourceMarkerWidth":null,"TargetMarkerPath":null,"TargetMarkerWidth":null},{"Id":"74ad5635-c96d-42e8-9c0a-42c613c66b7a","Color":"#FF808080","SelectedColor":"#FF000000","Width":2.0,"Label":null,"SourceId":"7dfd4102-2751-42c7-a386-adcfcca27ede","TargetId":"ad57e53f-8860-4212-9afb-f67e14eecbc8","SourcePortAlignment":"Right","TargetPortAlignment":"Bottom","Type":"DiagramLink","Router":null,"PathGenerator":null,"SourceMarkerPath":null,"SourceMarkerWidth":null,"TargetMarkerPath":null,"TargetMarkerWidth":null}]}`



近期会持续更新，欢迎大家光临。
最后上一个动画流程图。
![输入图片说明](AIStudio.Wpf.DiagramDesigner.Demo/33.gif)

17博客园文章地址 https://www.cnblogs.com/akwkevin/p/15047453.html

    续2： https://www.cnblogs.com/akwkevin/p/17093865.html

    相关链接地址：

    Fluent.Ribbon: https://github.com/fluentribbon/Fluent.Ribbon

    WPF-Diagram-Designer:https://github.com/LinRaise/WPF-Diagram-Designer

    个人QQ:80267720

    QQ技术交流群:51286643（如果您还喜欢，帮忙点个星，谢谢）

    特别感谢：https://dotnet9.com/ 的站长：dotnet9。

    有想加入开发的朋友可以联系我。