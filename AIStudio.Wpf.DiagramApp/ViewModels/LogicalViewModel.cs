﻿using AIStudio.Wpf.DiagramApp.Models;
using AIStudio.Wpf.DiagramApp.ViewModels;
using AIStudio.Wpf.Logical.ViewModels;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner;

namespace AIStudio.Wpf.Logical
{
    public class LogicalViewModel : PageViewModel
    {
        public LogicalViewModel(string title, string status, DiagramType diagramType) : base(title, status, diagramType)
        {

        }
        public LogicalViewModel(string filename, DiagramDocument diagramDocument) : base(filename, diagramDocument)
        {
            _service.DrawModeViewModel.LineDrawMode = DrawMode.ConnectingLineSmooth;
        }

        protected override void InitDiagramViewModel()
        {
            base.InitDiagramViewModel();

            DiagramViewModel.ShowGrid = true;
            DiagramViewModel.GridCellSize = new Size(150, 100);
            DiagramViewModel.PageSizeOrientation = PageSizeOrientation.Horizontal;
            DiagramViewModel.CellHorizontalAlignment = CellHorizontalAlignment.Left;
            DiagramViewModel.CellVerticalAlignment = CellVerticalAlignment.None;

            DiagramViewModel.Items.CollectionChanged += Items_CollectionChanged;
            _service.DrawModeViewModel.LineDrawMode = DrawMode.ConnectingLineSmooth;
        }

        protected override void Init()
        {
            base.Init();

            TimerDesignerItemViewModel timer = new TimerDesignerItemViewModel() { Left = 28, Top = 28 };
            timer.Value = 1;
            DiagramViewModel.DirectAddItemCommand.Execute(timer);

            InputItemViewModel in1 = new InputItemViewModel() { Left = 28, Top = 110 };
            in1.LinkPoint = LogicalService.LinkPoint[0];
            DiagramViewModel.DirectAddItemCommand.Execute(in1);

            InputItemViewModel in2 = new InputItemViewModel() { Left = 28, Top = 300 };
            in2.LinkPoint = LogicalService.LinkPoint[1];
            DiagramViewModel.DirectAddItemCommand.Execute(in2);

            AddGateItemViewModel item1 = new AddGateItemViewModel() { Left = 178, Top = 160 };
            DiagramViewModel.DirectAddItemCommand.Execute(item1);

            ConstantDesignerItemViewModel constant = new ConstantDesignerItemViewModel() { Left = 178, Top = 300, Value = 10 };
            DiagramViewModel.DirectAddItemCommand.Execute(constant);

            GTGateItemViewModel gTGate = new GTGateItemViewModel() { Left = 328, Top = 110 };
            DiagramViewModel.DirectAddItemCommand.Execute(gTGate);

            InputItemViewModel in3 = new InputItemViewModel() { Left = 328, Top = 210 };
            in3.LinkPoint = LogicalService.LinkPoint[2];
            DiagramViewModel.DirectAddItemCommand.Execute(in3);

            InputItemViewModel in4 = new InputItemViewModel() { Left = 328, Top = 300 };
            in4.LinkPoint = LogicalService.LinkPoint[3];
            DiagramViewModel.DirectAddItemCommand.Execute(in4);

            SELGateItemViewModel sELGate = new SELGateItemViewModel() { Left = 478, Top = 160 };
            DiagramViewModel.DirectAddItemCommand.Execute(sELGate);

            OutputItemViewModel out1 = new OutputItemViewModel() { Left = 628, Top = 110 };
            out1.LinkPoint = LogicalService.LinkPoint[4];
            DiagramViewModel.DirectAddItemCommand.Execute(out1);

            ConnectionViewModel connector1 = new ConnectionViewModel(in1.Output[0], item1.Input[0], _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            connector1.ColorViewModel.FillColor.Color = Colors.Green;
            connector1.ColorViewModel.LineAnimation = LineAnimation.PathAnimation;
            DiagramViewModel.DirectAddItemCommand.Execute(connector1);

            ConnectionViewModel connector2 = new ConnectionViewModel(in2.Output[0], item1.Input[1], _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            connector2.ColorViewModel.FillColor.Color = Colors.Green;
            connector2.ColorViewModel.LineAnimation = LineAnimation.PathAnimation;
            DiagramViewModel.DirectAddItemCommand.Execute(connector2);

            ConnectionViewModel connector3 = new ConnectionViewModel(item1.Output[0], gTGate.Input[0], _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            connector3.ColorViewModel.FillColor.Color = Colors.Green;
            connector3.ColorViewModel.LineAnimation = LineAnimation.PathAnimation;
            DiagramViewModel.DirectAddItemCommand.Execute(connector3);

            ConnectionViewModel connector4 = new ConnectionViewModel(constant.Output[0], gTGate.Input[1], _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            connector4.ColorViewModel.FillColor.Color = Colors.Green;
            connector4.ColorViewModel.LineAnimation = LineAnimation.PathAnimation;
            DiagramViewModel.DirectAddItemCommand.Execute(connector4);

            ConnectionViewModel connector5 = new ConnectionViewModel(gTGate.Output[0], sELGate.Input[0], _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            connector5.ColorViewModel.FillColor.Color = Colors.Green;
            connector5.ColorViewModel.LineAnimation = LineAnimation.PathAnimation;
            DiagramViewModel.DirectAddItemCommand.Execute(connector5);

            ConnectionViewModel connector6 = new ConnectionViewModel(in3.Output[0], sELGate.Input[1], _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            connector6.ColorViewModel.FillColor.Color = Colors.Green;
            connector6.ColorViewModel.LineAnimation = LineAnimation.PathAnimation;
            DiagramViewModel.DirectAddItemCommand.Execute(connector6);

            ConnectionViewModel connector7 = new ConnectionViewModel(in4.Output[0], sELGate.Input[2], _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            connector7.ColorViewModel.FillColor.Color = Colors.Green;
            connector7.ColorViewModel.LineAnimation = LineAnimation.PathAnimation;
            DiagramViewModel.DirectAddItemCommand.Execute(connector7);

            ConnectionViewModel connector8 = new ConnectionViewModel(sELGate.Output[0], out1.Input[0], _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            connector8.ColorViewModel.FillColor.Color = Colors.Green;
            connector8.ColorViewModel.LineAnimation = LineAnimation.PathAnimation;
            DiagramViewModel.DirectAddItemCommand.Execute(connector8);

            DiagramViewModel.ClearSelectedItemsCommand.Execute(null);
        }

        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (var item in e.OldItems.OfType<TimerDesignerItemViewModel>())
                {
                    item.Do -= Do;
                }
            }
            if (e.NewItems != null)
            {
                foreach (var item in e.NewItems.OfType<TimerDesignerItemViewModel>())
                {
                    item.Do += Do;
                }
            }

            RaisePropertyChanged("Items");
        }

        protected override bool AddVerify(SelectableDesignerItemViewModelBase arg)
        {
            if (base.AddVerify(arg) == false)
                return false;

            if (arg is ConnectionViewModel connector)
            {
                if (connector.IsFullConnection)
                {
                    if (DiagramViewModel.Items.OfType<ConnectionViewModel>().Any(p => p.SinkConnectorInfo == connector.SinkConnectorInfoFully))
                    {
                        return false;
                    }
                }
            }

            if (arg is TimerDesignerItemViewModel)
            {
                if (DiagramViewModel.Items.OfType<LogicalGateItemViewModelBase>().Any(p => p.LogicalType == LogicalType.Time))
                {
                    return false;
                }
            }

            return true;
        }

        private void Do()
        {
            Random rd = new Random();
            foreach (var link in LogicalService.LinkPoint)
            {
                link.Value = rd.NextDouble() * 10;
            }
            foreach (var item in DiagramViewModel.Items.OfType<ConstantDesignerItemViewModel>())
            {
                foreach (var output in item.Output)
                {
                    output.Value.ConnectorValue = item.Value;
                    output.Value.ColorViewModel.FillColor.Color = Colors.Green;
                }
            }

            foreach (var item in DiagramViewModel.Items.OfType<InputItemViewModel>())
            {
                if (item.LinkPoint != null)
                {
                    foreach (var output in item.Output)
                    {
                        output.Value.ConnectorValue = item.LinkPoint.Value;
                        output.Value.ColorViewModel.FillColor.Color = Colors.Green;
                    }
                }
            }

            foreach (var item in DiagramViewModel.Items.OfType<LogicalGateItemViewModelBase>().OrderBy(p => p.OrderNumber))
            {
                if (item.LogicalType != LogicalType.None)
                {
                    foreach (var input in item.Input)
                    {
                        var connector = GetSourceItem(input.Value);
                        if (connector == null)
                        {
                            continue;
                        }

                        if (connector.SourceConnectorInfo.DataItem is LogicalGateItemViewModelBase)
                        {
                            input.Value.ConnectorValue = (connector.SourceConnectorInfo as LogicalConnectorInfo).ConnectorValue;

                            input.Value.ColorViewModel.FillColor.Color = connector.SourceConnectorInfo.ColorViewModel.FillColor.Color;
                            connector.ColorViewModel.LineColor.Color = connector.SourceConnectorInfo.ColorViewModel.FillColor.Color;
                            connector.ColorViewModel.FillColor.Color = connector.SourceConnectorInfo.ColorViewModel.FillColor.Color;

                            if (item.LogicalType == LogicalType.Output)
                            {
                                input.Value.ValueTypePoint = (connector.SourceConnectorInfo as LogicalConnectorInfo).ValueTypePoint;
                            }
                            else if (item.LogicalType == LogicalType.NOT)
                            {
                                input.Value.ValueTypePoint = ((connector.SourceConnectorInfo as LogicalConnectorInfo).ValueTypePoint == ValueTypePoint.Bool) ? ValueTypePoint.Bool : ValueTypePoint.Int;
                            }
                        }
                    }

                    foreach (var output in item.Output)
                    {
                        if (item.LogicalType == LogicalType.Output)
                        {
                            var first = item.Input.Values.FirstOrDefault();
                            output.Value.ConnectorValue = first.ConnectorValue;
                            output.Value.ValueTypePoint = first.ValueTypePoint;
                            (item as OutputItemViewModel).Value = first.ConnectorValue;
                            (item as OutputItemViewModel).LinkPoint.Value = first.ConnectorValue;
                        }
                        else if (item.LogicalType == LogicalType.ADD)
                        {
                            output.Value.ConnectorValue = item.Input.Values.Select(p => p.ConnectorValue).Sum();
                        }
                        else if (item.LogicalType == LogicalType.SUB)
                        {
                            var first = item.Input.Values.Select(p => p.ConnectorValue).FirstOrDefault();
                            var second = item.Input.Values.Where((value, index) => index != 0).Select(p => p.ConnectorValue).Sum();
                            output.Value.ConnectorValue = first - second;
                        }
                        else if (item.LogicalType == LogicalType.MUL)
                        {
                            double result = 0;
                            foreach (var input in item.Input.Values)
                            {
                                if (result == 0)
                                {
                                    result = 1;
                                }
                                result *= input.ConnectorValue;
                            }
                            output.Value.ConnectorValue = result;
                        }
                        else if (item.LogicalType == LogicalType.DIV)
                        {
                            double result = item.Input.Values.Select(p => p.ConnectorValue).FirstOrDefault();
                            foreach (var input in item.Input.Values.Where((value, index) => index != 0))
                            {
                                result /= input.ConnectorValue;
                            }
                            output.Value.ConnectorValue = result;
                        }
                        else if (item.LogicalType == LogicalType.AVE)
                        {
                            output.Value.ConnectorValue = item.Input.Values.Select(p => p.ConnectorValue).Average();
                        }
                        else if (item.LogicalType == LogicalType.MOD)
                        {
                            output.Value.ConnectorValue = item.Input[0].ConnectorValue % item.Input[1].ConnectorValue;
                        }
                        else if (item.LogicalType == LogicalType.AND)
                        {
                            output.Value.ConnectorValue = Convert.ToInt32(item.Input[0].ConnectorValue) & Convert.ToInt32(item.Input[1].ConnectorValue);
                        }
                        else if (item.LogicalType == LogicalType.OR)
                        {
                            output.Value.ConnectorValue = Convert.ToInt32(item.Input[0].ConnectorValue) | Convert.ToInt32(item.Input[1].ConnectorValue);
                        }
                        else if (item.LogicalType == LogicalType.XOR)
                        {
                            output.Value.ConnectorValue = Convert.ToInt32(Convert.ToInt32(item.Input[0].ConnectorValue) ^ Convert.ToInt32(item.Input[1].ConnectorValue));
                        }
                        else if (item.LogicalType == LogicalType.NOT)
                        {
                            if (item.Input[0].ValueTypePoint == ValueTypePoint.Bool)
                            {
                                output.Value.ConnectorValue = Convert.ToInt32(!Convert.ToBoolean(item.Input[0].ConnectorValue));
                                output.Value.ValueTypePoint = ValueTypePoint.Bool;
                            }
                            else
                            {
                                output.Value.ConnectorValue = ~Convert.ToInt32(item.Input[0].ConnectorValue);
                                output.Value.ValueTypePoint = ValueTypePoint.Int;
                            }

                        }
                        else if (item.LogicalType == LogicalType.SHL)
                        {
                            output.Value.ConnectorValue = Convert.ToInt32(item.Input[0].ConnectorValue) << Convert.ToInt32(item.Input[1].ConnectorValue);
                        }
                        else if (item.LogicalType == LogicalType.SHR)
                        {
                            output.Value.ConnectorValue = Convert.ToInt32(item.Input[0].ConnectorValue) >> Convert.ToInt32(item.Input[1].ConnectorValue);
                        }
                        else if (item.LogicalType == LogicalType.ROL)
                        {
                            output.Value.ConnectorValue = (Convert.ToInt32(item.Input[0].ConnectorValue) << Convert.ToInt32(item.Input[1].ConnectorValue)) | (Convert.ToInt32(item.Input[0].ConnectorValue) >> 32 - Convert.ToInt32(item.Input[1].ConnectorValue));
                        }
                        else if (item.LogicalType == LogicalType.ROR)
                        {
                            output.Value.ConnectorValue = (Convert.ToInt32(item.Input[0].ConnectorValue) >> Convert.ToInt32(item.Input[1].ConnectorValue)) | (Convert.ToInt32(item.Input[0].ConnectorValue) << 32 - Convert.ToInt32(item.Input[1].ConnectorValue));
                        }
                        else if (item.LogicalType == LogicalType.SEL)
                        {
                            output.Value.ConnectorValue = (item.Input[0].ConnectorValue == output.Key) ? item.Input[1].ConnectorValue : item.Input[2].ConnectorValue;
                        }
                        else if (item.LogicalType == LogicalType.MAX)
                        {
                            output.Value.ConnectorValue = item.Input.Values.Select(p => p.ConnectorValue).Max();
                        }
                        else if (item.LogicalType == LogicalType.MIN)
                        {
                            output.Value.ConnectorValue = item.Input.Values.Select(p => p.ConnectorValue).Min();
                        }
                        else if (item.LogicalType == LogicalType.LIMIT)
                        {
                            output.Value.ConnectorValue = (item.Input[0].ConnectorValue > item.Input[1].ConnectorValue) ? item.Input[1].ConnectorValue : item.Input[0].ConnectorValue;
                        }
                        else if (item.LogicalType == LogicalType.GT)
                        {
                            output.Value.ConnectorValue = (item.Input[0].ConnectorValue > item.Input[1].ConnectorValue) ? 1 : 0;
                        }
                        else if (item.LogicalType == LogicalType.LT)
                        {
                            output.Value.ConnectorValue = (item.Input[0].ConnectorValue < item.Input[1].ConnectorValue) ? 1 : 0;
                        }
                        else if (item.LogicalType == LogicalType.GE)
                        {
                            output.Value.ConnectorValue = (item.Input[0].ConnectorValue >= item.Input[1].ConnectorValue) ? 1 : 0;
                        }
                        else if (item.LogicalType == LogicalType.LE)
                        {
                            output.Value.ConnectorValue = (item.Input[0].ConnectorValue <= item.Input[1].ConnectorValue) ? 1 : 0;
                        }
                        else if (item.LogicalType == LogicalType.EQ)
                        {
                            output.Value.ConnectorValue = (item.Input[0].ConnectorValue == item.Input[1].ConnectorValue) ? 1 : 0;
                        }
                        else if (item.LogicalType == LogicalType.NE)
                        {
                            output.Value.ConnectorValue = (item.Input[0].ConnectorValue != item.Input[1].ConnectorValue) ? 1 : 0;
                        }
                        else if (item.LogicalType == LogicalType.ABS)
                        {
                            output.Value.ConnectorValue = Math.Abs(item.Input[0].ConnectorValue);
                        }
                        else if (item.LogicalType == LogicalType.SQRT)
                        {
                            output.Value.ConnectorValue = Math.Sqrt(item.Input[0].ConnectorValue);
                        }
                        else if (item.LogicalType == LogicalType.LN)
                        {
                            output.Value.ConnectorValue = Math.Log10(item.Input[0].ConnectorValue);
                        }
                        else if (item.LogicalType == LogicalType.LOG)
                        {
                            output.Value.ConnectorValue = Math.Log(item.Input[0].ConnectorValue);
                        }
                        else if (item.LogicalType == LogicalType.EXP)
                        {
                            output.Value.ConnectorValue = Math.Exp(item.Input[0].ConnectorValue);
                        }
                        else if (item.LogicalType == LogicalType.SIN)
                        {
                            output.Value.ConnectorValue = Math.Sin(item.Input[0].ConnectorValue);
                        }
                        else if (item.LogicalType == LogicalType.COS)
                        {
                            output.Value.ConnectorValue = Math.Cos(item.Input[0].ConnectorValue);
                        }
                        else if (item.LogicalType == LogicalType.TAN)
                        {
                            output.Value.ConnectorValue = Math.Tan(item.Input[0].ConnectorValue);
                        }
                        else if (item.LogicalType == LogicalType.ASIN)
                        {
                            output.Value.ConnectorValue = Math.Asin(item.Input[0].ConnectorValue);
                        }
                        else if (item.LogicalType == LogicalType.ACOS)
                        {
                            output.Value.ConnectorValue = Math.Acos(item.Input[0].ConnectorValue);
                        }
                        else if (item.LogicalType == LogicalType.ATAN)
                        {
                            output.Value.ConnectorValue = Math.Atan(item.Input[0].ConnectorValue);
                        }
                        else if (item.LogicalType == LogicalType.EXPT)
                        {
                            output.Value.ConnectorValue = Math.Exp(item.Input[0].ConnectorValue);
                        }

                        if (output.Value.ValueTypePoint == ValueTypePoint.Bool)
                        {
                            if (output.Value.ConnectorValue == 0)
                            {
                                output.Value.ColorViewModel.FillColor.Color = Colors.Red;
                                if (item.LogicalType == LogicalType.Output)
                                {
                                    item.ColorViewModel.FillColor.Color = Colors.Red;
                                }
                            }
                            else
                            {
                                output.Value.ColorViewModel.FillColor.Color = Colors.Green;
                                if (item.LogicalType == LogicalType.Output)
                                {
                                    item.ColorViewModel.FillColor.Color = Colors.Green;
                                }
                            }
                        }
                        else
                        {
                            output.Value.ColorViewModel.FillColor.Color = Colors.Green;
                        }
                    }
                }
            }
        }

        private ConnectionViewModel GetSourceItem(FullyCreatedConnectorInfo sinkConnector)
        {
            foreach (var connector in DiagramViewModel.Items.OfType<ConnectionViewModel>())
            {
                if (connector.SinkConnectorInfo == sinkConnector)
                {
                    return connector;
                }
            }
            return null;
        }
    }
}
