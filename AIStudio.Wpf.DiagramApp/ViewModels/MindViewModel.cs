﻿using AIStudio.Wpf.DiagramApp.Models;
using AIStudio.Wpf.DiagramApp.ViewModels;
using AIStudio.Wpf.Flowchart.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.Mind.ViewModels;
using AIStudio.Wpf.Mind;

namespace AIStudio.Wpf.Flowchart
{
    public class MindViewModel : PageViewModel
    {
        public MindViewModel(string title, string status, DiagramType diagramType) : base(title, status, diagramType)
        {
            
        }
        public MindViewModel(string filename, DiagramDocument diagramDocument) : base(filename, diagramDocument)
        {          
            
        }

        protected override void InitDiagramViewModel()
        {
            base.InitDiagramViewModel();

            DiagramViewModel.GridCellSize = new Size(100, 100);
            DiagramViewModel.ShowGrid= false;
            DiagramViewModel.AllowDrop = false;
        }

        public IMindDiagramViewModel MindDiagramViewModel
        {
            get
            {
                return DiagramViewModel as IMindDiagramViewModel;
            } 
        }

        protected override void Init()
        {
            DiagramViewModels = new ObservableCollection<IDiagramViewModel>()
            {
                new MindDiagramViewModel(){Name= "页-1", DiagramType = DiagramType},
            };
            DiagramViewModel = DiagramViewModels.FirstOrDefault();

            InitDiagramViewModel();
            var level1node = MindDiagramViewModel.RootItem;
            DiagramViewModel.CenterMoveCommand.Execute(level1node);

            MindNode level2node1_1 = new MindNode(DiagramViewModel) { Text = "分支主题1" };
            MindDiagramViewModel.AddChildCommand.Execute(new MindNode[] { level1node, level2node1_1 });

            MindNode level2node1_1_1 = new MindNode(DiagramViewModel) { Text = "分支主题1_1" };
            MindDiagramViewModel.AddChildCommand.Execute(new MindNode[] { level2node1_1, level2node1_1_1 });

            MindNode level2node1_1_2 = new MindNode(DiagramViewModel) { Text = "分支主题1_2" };
            MindDiagramViewModel.AddChildCommand.Execute(new MindNode[] { level2node1_1, level2node1_1_2 });;

            MindNode level2node1_1_3 = new MindNode(DiagramViewModel) { Text = "分支主题1_3" };
            MindDiagramViewModel.AddChildCommand.Execute(new MindNode[] { level2node1_1, level2node1_1_3 });

            MindNode level2node1_2 = new MindNode(DiagramViewModel) { Text = "分支主题2" };
            MindDiagramViewModel.AddChildCommand.Execute(new MindNode[] { level1node, level2node1_2 });

            MindNode level2node1_3 = new MindNode(DiagramViewModel) { Text = "分支主题3" };
            MindDiagramViewModel.AddChildCommand.Execute(new MindNode[] { level1node, level2node1_3 });

            DiagramViewModel.ClearSelectedItemsCommand.Execute(null);
            level1node.LayoutUpdated();
        }


        public override void Dispose()
        {
            base.Dispose();

            foreach (var viewModel in DiagramViewModels)
            {
                FlowchartService.DisposeData(viewModel);
            }
        }
    }
}
