﻿using AIStudio.Wpf.DiagramApp.Views;
using AIStudio.Wpf.Flowchart;
using AIStudio.Wpf.Logical;
using ControlzEx.Theming;
using Dragablz;
using Fluent;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Additionals;
using AIStudio.Wpf.DiagramDesigner.Additionals.Commands;

namespace AIStudio.Wpf.DiagramApp.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private IDiagramServiceProvider _service { get { return DiagramServicesProvider.Instance.Provider; } }
        private string _history = System.AppDomain.CurrentDomain.BaseDirectory + "history.json";

        public MainWindowViewModel()
        {
            ToolBoxViewModel = new ToolBoxViewModel();

            DiagramsViewModels = new ObservableCollection<PageViewModel>();
            DiagramsViewModels.Add(new PageViewModel("新建-1", "*", DiagramType.Normal));
            PageViewModel = DiagramsViewModels.FirstOrDefault();

            StandardColor = GenerateStandardGradients();

            if (File.Exists(_history))
            {
                HistoryList = JsonConvert.DeserializeObject<ObservableCollection<string>>(File.ReadAllText(_history));
            }
            else
            {
                HistoryList = new ObservableCollection<string>();
            }
            this.PropertyChanged += MainWindowViewModel_PropertyChanged;
            _service.PropertyChanged += Provider_PropertyChanged;
        }

        #region 属性
        public ToolBoxViewModel ToolBoxViewModel { get; private set; }

        private ObservableCollection<PageViewModel> _diagramsViewModels;
        public ObservableCollection<PageViewModel> DiagramsViewModels
        {
            get
            {
                return _diagramsViewModels;
            }
            set
            {
                SetProperty(ref _diagramsViewModels, value);
            }
        }

        private PageViewModel _diagramsViewModel;
        public PageViewModel PageViewModel
        {
            get
            {
                return _diagramsViewModel;
            }
            set
            {
                SetProperty(ref _diagramsViewModel, value);
            }
        }

        private Models.ColorType _colorObject;
        public Models.ColorType ColorType
        {
            get
            {
                return _colorObject;
            }
            set
            {
                SetProperty(ref _colorObject, value);
            }
        }

        private bool _isOpenBackstage;

        public bool IsOpenBackstage
        {
            get
            {
                return _isOpenBackstage;
            }
            set
            {
                SetProperty(ref _isOpenBackstage, value);
            }
        }

        private ObservableCollection<string> _historyList;
        public ObservableCollection<string> HistoryList
        {
            get
            {
                return _historyList;
            }
            set
            {
                SetProperty(ref _historyList, value);
            }
        }

        public Color[] StandardColor { get; set; }

        public IDrawModeViewModel DrawModeViewModel
        {
            get
            {
                return _service.DrawModeViewModel;
            }
        }

        public IFontViewModel FontViewModel
        {
            get
            {
                return _service.FontViewModel;
            }
        }
        public IColorViewModel ColorViewModel
        {
            get
            {
                return _service.ColorViewModel;
            }
        }
        public IShapeViewModel ShapeViewModel
        {
            get
            {
                return _service.ShapeViewModel;
            }
        }
        public IQuickThemeViewModel QuickThemeViewModel
        {
            get
            {
                return _service.QuickThemeViewModel;
            }
        }
        public ILockObjectViewModel LockObjectViewModel
        {
            get
            {
                return _service.LockObjectViewModel;
            }
        }

        public SelectableDesignerItemViewModelBase SelectedItem
        {
            get
            {
                return _service.SelectedItem;
            }
        }

        public Color ThemeColor
        {
            get => ((SolidColorBrush)Application.Current.Resources["Fluent.Ribbon.Brushes.AccentBaseColorBrush"])?.Color ?? Colors.Pink;

            set
            {
                var solidColorBrush = new SolidColorBrush(value);
                solidColorBrush.Freeze();
                Application.Current.Resources["Fluent.Ribbon.Brushes.AccentBaseColorBrush"] = solidColorBrush;
            }
        }

        public string CurrentBaseColor
        {
            get => this.CurrentTheme.BaseColorScheme;

            set
            {
                if (value is null)
                {
                    return;
                }

                ThemeManager.Current.ChangeThemeBaseColor(Application.Current, value);
                RaisePropertyChanged(nameof(this.CurrentTheme));
            }
        }

        public Theme CurrentTheme
        {
            get => ThemeManager.Current.DetectTheme(Application.Current);

            set
            {
                if (value is null)
                {
                    return;
                }

                ThemeManager.Current.ChangeTheme(Application.Current, value);
                RaisePropertyChanged(nameof(this.CurrentBaseColor));
            }
        }
        #endregion

        public Func<PageViewModel> NewItemFactory
        {
            get
            {
                return
                    () =>
                    {
                        return new PageViewModel(NewNameHelper.GetNewName(DiagramsViewModels.Select(p => p.Title), "新建-"), "*", DiagramType.Normal);
                    };
            }
        }

        #region 命令
        private ICommand _newCommand;
        public ICommand NewCommand
        {
            get
            {
                return this._newCommand ?? (this._newCommand = new DelegateCommand<string>(para => this.New_Executed(para)));
            }
        }

        private ICommand _openCommand;
        public ICommand OpenCommand
        {
            get
            {
                return this._openCommand ?? (this._openCommand = new DelegateCommand<string>(para => this.OpenExecuted(para)));
            }
        }

        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                return this._saveCommand ?? (this._saveCommand = new CanExecuteDelegateCommand(() => this.SaveExecuted(), () => this.Save_Enable()));
            }
        }

        private ICommand _saveAsCommand;
        public ICommand SaveAsCommand
        {
            get
            {
                return this._saveAsCommand ?? (this._saveAsCommand = new CanExecuteDelegateCommand(() => this.SaveAsExecuted(), () => this.Save_Enable()));
            }
        }

        private ICommand _pasteCommand;
        public ICommand PasteCommand
        {
            get
            {
                return this._pasteCommand ?? (this._pasteCommand = new CanExecuteDelegateCommand(() => this.PasteExecuted(), () => this.Paste_Enabled()));
            }
        }

        private ICommand _cutCommand;
        public ICommand CutCommand
        {
            get
            {
                return this._cutCommand ?? (this._cutCommand = new CanExecuteDelegateCommand(() => this.CutExecuted(), () => this.Cut_Enabled()));
            }
        }

        private ICommand _copyCommand;
        public ICommand CopyCommand
        {
            get
            {
                return this._copyCommand ?? (this._copyCommand = new CanExecuteDelegateCommand(() => this.CopyExecuted(), () => Copy_Enabled()));
            }
        }

        private ICommand _exitCommand;
        public ICommand ExitCommand
        {
            get
            {
                return this._exitCommand ?? (this._exitCommand = new CanExecuteDelegateCommand(() => this.ExitExecuted()));
            }
        }

        private ICommand _formatCommand;
        public ICommand FormatCommand
        {
            get
            {
                return this._formatCommand ?? (this._formatCommand = new CanExecuteDelegateCommand(() => this.FormatExecuted(), () => Format_Enabled()));
            }
        }

        private ICommand _deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return this._deleteCommand ?? (this._deleteCommand = new CanExecuteDelegateCommand(() => this.DeleteExecuted(), () => Delete_Enabled()));
            }
        }

        private ICommand _alignTopCommand;
        public ICommand AlignTopCommand
        {
            get
            {
                return this._alignTopCommand ?? (this._alignTopCommand = new DelegateCommand<object>(para => this.AlignTopExecuted(para)));
            }
        }

        private ICommand _alignVerticalCentersCommand;
        public ICommand AlignVerticalCentersCommand
        {
            get
            {
                return this._alignVerticalCentersCommand ?? (this._alignVerticalCentersCommand = new DelegateCommand<object>(para => this.AlignVerticalCentersExecuted(para)));
            }
        }

        private ICommand _alignBottomCommand;
        public ICommand AlignBottomCommand
        {
            get
            {
                return this._alignBottomCommand ?? (this._alignBottomCommand = new DelegateCommand<object>(para => this.AlignBottomExecuted(para)));
            }
        }

        private ICommand _alignLeftCommand;
        public ICommand AlignLeftCommand
        {
            get
            {
                return this._alignLeftCommand ?? (this._alignLeftCommand = new DelegateCommand<object>(para => this.AlignLeftExecuted(para)));
            }
        }

        private ICommand _alignHorizontalCentersCommand;
        public ICommand AlignHorizontalCentersCommand
        {
            get
            {
                return this._alignHorizontalCentersCommand ?? (this._alignHorizontalCentersCommand = new DelegateCommand<object>(para => this.AlignHorizontalCentersExecuted(para)));
            }
        }

        private ICommand _alignRightCommand;
        public ICommand AlignRightCommand
        {
            get
            {
                return this._alignRightCommand ?? (this._alignRightCommand = new DelegateCommand<object>(para => this.AlignRightExecuted(para)));
            }
        }

        private ICommand _groupCommand;
        public ICommand GroupCommand
        {
            get
            {
                return this._groupCommand ?? (this._groupCommand = new DelegateCommand<object>(para => this.GroupExecuted(para)));
            }
        }

        private ICommand _ungroupCommand;
        public ICommand UngroupCommand
        {
            get
            {
                return this._ungroupCommand ?? (this._ungroupCommand = new DelegateCommand<object>(para => this.UngroupExecuted(para)));
            }
        }

        private ICommand _bringForwardCommand;
        public ICommand BringForwardCommand
        {
            get
            {
                return this._bringForwardCommand ?? (this._bringForwardCommand = new DelegateCommand<object>(para => this.BringForwardExecuted(para)));
            }
        }

        private ICommand _bringToFrontCommand;
        public ICommand BringToFrontCommand
        {
            get
            {
                return this._bringToFrontCommand ?? (this._bringToFrontCommand = new DelegateCommand<object>(para => this.BringToFrontExecuted(para)));
            }
        }

        private ICommand _sendBackwardCommand;
        public ICommand SendBackwardCommand
        {
            get
            {
                return this._sendBackwardCommand ?? (this._sendBackwardCommand = new DelegateCommand<object>(para => this.SendBackwardExecuted(para)));
            }
        }

        private ICommand _sendToBackCommand;
        public ICommand SendToBackCommand
        {
            get
            {
                return this._sendToBackCommand ?? (this._sendToBackCommand = new DelegateCommand<object>(para => this.SendToBackExecuted(para)));
            }
        }

        private ICommand _distributeHorizontalCommand;
        public ICommand DistributeHorizontalCommand
        {
            get
            {
                return this._distributeHorizontalCommand ?? (this._distributeHorizontalCommand = new DelegateCommand<object>(para => this.DistributeHorizontalExecuted(para)));
            }
        }

        private ICommand _distributeVerticalCommand;
        public ICommand DistributeVerticalCommand
        {
            get
            {
                return this._distributeVerticalCommand ?? (this._distributeVerticalCommand = new DelegateCommand<object>(para => this.DistributeVerticalExecuted(para)));
            }
        }

        private ICommand _selectAllCommand;
        public ICommand SelectAllCommand
        {
            get
            {
                return this._selectAllCommand ?? (this._selectAllCommand = new DelegateCommand<object>(para => this.SelectAllExecuted(para)));
            }
        }

        private ICommand _centerCommand;
        public ICommand CenterCommand
        {
            get
            {
                return this._centerCommand ?? (this._centerCommand = new DelegateCommand<object>(para => this.CenterMoveExecuted(para)));
            }
        }

        private ICommand _sameWidthCommand;
        public ICommand SameWidthCommand
        {
            get
            {
                return this._sameWidthCommand ?? (this._sameWidthCommand = new DelegateCommand<object>(para => this.SameWidthExecuted(para)));
            }
        }

        private ICommand _sameHeightCommand;
        public ICommand SameHeightCommand
        {
            get
            {
                return this._sameHeightCommand ?? (this._sameHeightCommand = new DelegateCommand<object>(para => this.SameHeightExecuted(para)));
            }
        }

        private ICommand _sameSizeCommand;
        public ICommand SameSizeCommand
        {
            get
            {
                return this._sameSizeCommand ?? (this._sameSizeCommand = new DelegateCommand<object>(para => this.SameSizeExecuted(para)));
            }
        }

        private ICommand _sameAngleCommand;
        public ICommand SameAngleCommand
        {
            get
            {
                return this._sameAngleCommand ?? (this._sameAngleCommand = new DelegateCommand<object>(para => this.SameAngleExecuted(para)));
            }
        }

        private ICommand _lockCommand;
        public ICommand LockCommand
        {
            get
            {
                return this._lockCommand ?? (this._lockCommand = new DelegateCommand<object>(para => this.LockExecuted(para)));
            }
        }

        private ICommand _unlockCommand;
        public ICommand UnlockCommand
        {
            get
            {
                return this._unlockCommand ?? (this._unlockCommand = new DelegateCommand<object>(para => this.UnlockExecuted(para)));
            }
        }

        private ICommand _selectedColorCommand;
        public ICommand SelectedColorCommand
        {
            get
            {
                return this._selectedColorCommand ?? (this._selectedColorCommand = new DelegateCommand<object>(para => this.SelectedColorExecuted(para)));
            }
        }

        private ICommand _addPageCommand;
        public ICommand AddPageCommand
        {
            get
            {
                return this._addPageCommand ?? (this._addPageCommand = new DelegateCommand<object>(para => this.AddPageExecuted(para)));
            }
        }


        private ICommand _addCopyPageCommand;
        public ICommand AddCopyPageCommand
        {
            get
            {
                return this._addCopyPageCommand ?? (this._addCopyPageCommand = new DelegateCommand<object>(para => this.AddCopyPageExecuted(para)));
            }
        }

        private ICommand _renamePageCommand;
        public ICommand RenamePageCommand
        {
            get
            {
                return this._renamePageCommand ?? (this._renamePageCommand = new DelegateCommand<object>(para => this.RenamePageExecuted(para)));
            }
        }

        private ICommand _endRenamePageCommand;
        public ICommand EndRenamePageCommand
        {
            get
            {
                return this._endRenamePageCommand ?? (this._endRenamePageCommand = new DelegateCommand<object>(para => this.EndRenamePageExecuted(para)));
            }
        }

        private ICommand _deletePageCommand;
        public ICommand DeletePageCommand
        {
            get
            {
                return this._deletePageCommand ?? (this._deletePageCommand = new DelegateCommand<object>(para => this.DeletePageExecuted(para)));
            }
        }


        private ICommand _addImageCommand;
        public ICommand AddImageCommand
        {
            get
            {
                return this._addImageCommand ?? (this._addImageCommand = new DelegateCommand<object>(para => this.AddImageExecuted(para)));
            }
        }

        private ICommand _editImageCommand;
        public ICommand EditImageCommand
        {
            get
            {
                return this._editImageCommand ?? (this._editImageCommand = new DelegateCommand<object>(para => this.EditImageExecuted(para)));
            }
        }

        private ICommand _resizeImageCommand;
        public ICommand ResizeImageCommand
        {
            get
            {
                return this._resizeImageCommand ?? (this._resizeImageCommand = new DelegateCommand<object>(para => this.ResizeImageExecuted(para)));
            }
        }

        private ICommand _resetImageCommand;
        public ICommand ResetImageCommand
        {
            get
            {
                return this._resetImageCommand ?? (this._resetImageCommand = new DelegateCommand<object>(para => this.ResetImageExecuted(para)));
            }
        }

        private ICommand _addVideoCommand;
        public ICommand AddVideoCommand
        {
            get
            {
                return this._addVideoCommand ?? (this._addVideoCommand = new DelegateCommand<object>(para => this.AddVideoExectued(para)));
            }
        }


        private ICommand _addOutLineTextCommand;
        public ICommand AddOutLineTextCommand
        {
            get
            {
                return this._addOutLineTextCommand ?? (this._addOutLineTextCommand = new DelegateCommand<object>(para => this.AddOutLineTextExecuted(para)));
            }
        }

        private ICommand _addBarcodeCommand;
        public ICommand AddBarcodeCommand
        {
            get
            {
                return this._addBarcodeCommand ?? (this._addBarcodeCommand = new DelegateCommand<object>(para => this.AddBarcodeExecuted(para)));
            }
        }

        private ICommand _aboutCommand;
        public ICommand AboutCommand
        {
            get
            {
                return this._aboutCommand ?? (this._aboutCommand = new DelegateCommand(() => this.AboutExecuted()));
            }
        }
        #endregion

        public ItemActionCallback ClosingTabItemHandler
        {
            get { return ClosingTabItemHandlerImpl; }
        }

        /// <summary>
        /// Callback to handle tab closing.
        /// </summary>        
        private void ClosingTabItemHandlerImpl(ItemActionCallbackArgs<TabablzControl> args)
        {
            //here's how you can cancel stuff:
            //args.Cancel(); 

            if (args.DragablzItem.DataContext is PageViewModel viewModel)
            {
                viewModel.Dispose();
            }
        }

        private void MainWindowViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

        }

        private void Provider_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(DrawModeViewModel)
                || e.PropertyName == nameof(FontViewModel)
                || e.PropertyName == nameof(ColorViewModel)
                || e.PropertyName == nameof(ShapeViewModel)
                || e.PropertyName == nameof(QuickThemeViewModel)
                || e.PropertyName == nameof(LockObjectViewModel)
                || e.PropertyName == nameof(SelectedItem))
            {
                RaisePropertyChanged(e.PropertyName);
            }

            if (PageViewModel == null) return;

            if (sender is IFontViewModel)
                PageViewModel.SetFont(sender as IFontViewModel, e.PropertyName);

            if (sender is IColorViewModel)
                PageViewModel.SetColor(sender as IColorViewModel, e.PropertyName);

            if (sender is IShapeViewModel)
                PageViewModel.SetSharp(sender as IShapeViewModel, e.PropertyName);

            if (sender is IQuickThemeViewModel)
                PageViewModel.SetQuickItem(sender as IQuickThemeViewModel, e.PropertyName);

            if (sender is LockObject)
                PageViewModel.LockAction(sender as LockObject, e.PropertyName);

            if (sender is DesignerItemViewModelBase designer
                && (e.PropertyName == nameof(designer.Angle)
                   || e.PropertyName == nameof(designer.ItemWidth)
                   || e.PropertyName == nameof(designer.ItemHeight)
                   || e.PropertyName == nameof(designer.ScaleX)
                   || e.PropertyName == nameof(designer.ScaleY)))
            {
                PageViewModel.SetPropertyValue(designer, e.PropertyName);
            }
        }


        public bool KeyExecuted(KeyEventArgs e)
        {
            if (PageViewModel?.DiagramViewModel?.ExecuteShortcut(e) == true)
            {
                return true;
            }

            var para = e.KeyboardDevice.Modifiers == ModifierKeys.None ? e.Key.ToString() : e.KeyboardDevice.Modifiers.ToString() + "+" + e.Key.ToString();
            bool executed = true;
            switch (para)           
            {
                case "Control+O": OpenExecuted(); break;
                case "Control+N": New_Executed(); break;
                case "Control+S": SaveExecuted(); break;              
                default: executed = false; break;
            }

            return executed;
        }

        private void UnDoExecuted()
        {
            PageViewModel?.DiagramViewModel?.UndoCommand.Execute(null);
        }

        private void ReDoExecuted()
        {
            PageViewModel?.DiagramViewModel?.RedoCommand.Execute(null);
        }

        private void SelectedAllExecuted()
        {
            PageViewModel?.DiagramViewModel?.SelectAllCommand.Execute(null); 
        }

        private void OpenExecuted(string para = null)
        {
            string filename = string.Empty;

            if (string.IsNullOrEmpty(para))
            {
                Microsoft.Win32.OpenFileDialog openFile = new Microsoft.Win32.OpenFileDialog();
                openFile.Filter = "Designer Files (*.xml;*.json)|*.xml;*.json|All Files (*.*)|*.*";

                if (openFile.ShowDialog() == false)
                {
                    return;
                }

                filename = openFile.FileName;
            }
            else
            {
                filename = para;
            }

            var viewmodel = DiagramsViewModels.FirstOrDefault(p => p.FileName == filename);
            if (viewmodel != null)
            {
                PageViewModel = viewmodel;
                MessageBox.Show("文档已经打开");
                return;
            }

            var diagram = PageViewModel.OpenFile(filename, Path.GetExtension(filename));
            PageViewModel flow;
            if (diagram.DiagramType == DiagramType.FlowChart)
            {
                flow = new FlowchartViewModel(filename, diagram);
            }
            else if (diagram.DiagramType == DiagramType.Logical)
            {
                flow = new LogicalViewModel(filename, diagram);
            }
            else if (diagram.DiagramType == DiagramType.SFC)
            {
                flow = new SFCViewModel(filename, diagram);
            }
            else
            {
                flow = new PageViewModel(filename, diagram);
            }
            DiagramsViewModels.Add(flow);
            PageViewModel = flow;

            if (string.IsNullOrEmpty(para))
            {
                SaveHistory(PageViewModel);
            }
            else
            {
                IsOpenBackstage = false;
            }

        }

        private void SaveExecuted()
        {
            if (PageViewModel == null) return;

            if (PageViewModel.SaveFile())
            {
                SaveHistory(PageViewModel);
            }
        }

        private void SaveAsExecuted()
        {
            if (PageViewModel == null) return;

            if (PageViewModel.SaveFile(true))
            {
                SaveHistory(PageViewModel);
            }
        }

        private void SaveHistory(PageViewModel diagramsViewModel)
        {
            HistoryList.Remove(PageViewModel.FileName);
            HistoryList.Insert(0, PageViewModel.FileName);
            File.WriteAllText(_history, JsonConvert.SerializeObject(HistoryList));
        }

        private bool Save_Enable()
        {
            return PageViewModel != null;
        }

        private void PasteExecuted()
        {
            PageViewModel?.DiagramViewModel?.PasteCommand.Execute(null);
        }

        private bool Paste_Enabled()
        {
            return Clipboard.ContainsData(DataFormats.Serializable);
        }

        private void CopyExecuted()
        {
            PageViewModel?.DiagramViewModel?.CopyCommand.Execute(null);
        }

        private bool Copy_Enabled()
        {
            return PageViewModel != null && PageViewModel.DiagramViewModel != null && PageViewModel.DiagramViewModel.SelectedItems.Count() > 0;
        }

        private void DeleteExecuted()
        {
            PageViewModel?.DiagramViewModel?.DeleteCommand.Execute(null);
        }

        private bool Delete_Enabled()
        {
            return PageViewModel != null && PageViewModel.DiagramViewModel != null && PageViewModel.DiagramViewModel.SelectedItems.Count() > 0;
        }

        private void CutExecuted()
        {
            PageViewModel?.DiagramViewModel?.CutCommand.Execute(null);
        }

        private bool Cut_Enabled()
        {
            return PageViewModel != null && PageViewModel.DiagramViewModel != null && PageViewModel.DiagramViewModel.SelectedItems.Count() > 0;
        }

        private void FormatExecuted()
        {
            _service.DrawModeViewModel.CursorMode = CursorMode.Format;
        }

        private bool Format_Enabled()
        {
            return PageViewModel != null && PageViewModel.DiagramViewModel != null && PageViewModel.DiagramViewModel.SelectedItems.Count() == 1;
        }

        private void New_Executed(string type = "Normal")
        {
            IsOpenBackstage = false;
            if (type == DiagramType.FlowChart.ToString())
            {
                PageViewModel = new FlowchartViewModel(NewNameHelper.GetNewName(DiagramsViewModels.Select(p => p.Title), "新建-"), "*", (DiagramType)Enum.Parse(typeof(DiagramType), type));
            }
            else if (type == DiagramType.Logical.ToString())
            {
                PageViewModel = new LogicalViewModel(NewNameHelper.GetNewName(DiagramsViewModels.Select(p => p.Title), "新建-"), "*", (DiagramType)Enum.Parse(typeof(DiagramType), type));
            }
            else if (type == DiagramType.SFC.ToString())
            {
                PageViewModel = new SFCViewModel(NewNameHelper.GetNewName(DiagramsViewModels.Select(p => p.Title), "新建-"), "*", (DiagramType)Enum.Parse(typeof(DiagramType), type));
            }
            else if (type == DiagramType.Mind.ToString())
            {
                PageViewModel = new MindViewModel(NewNameHelper.GetNewName(DiagramsViewModels.Select(p => p.Title), "新建-"), "*", (DiagramType)Enum.Parse(typeof(DiagramType), type));
            }
            else
            {
                PageViewModel = new PageViewModel(NewNameHelper.GetNewName(DiagramsViewModels.Select(p => p.Title), "新建-"), "*", (DiagramType)Enum.Parse(typeof(DiagramType), type));
            }

            DiagramsViewModels.Add(PageViewModel);
        }

        private void ExitExecuted()
        {
            throw new NotImplementedException();
        }

        private void GroupExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.GroupCommand.Execute(para);
        }

        private void UngroupExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.UngroupCommand.Execute(para);
        }

        #region 布局
        private void AlignTopExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.AlignTopCommand.Execute(para);
        }
        private void AlignVerticalCentersExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.AlignVerticalCentersCommand.Execute(para);
        }
        private void AlignBottomExecuted(object para)
        {          
            PageViewModel?.DiagramViewModel?.AlignBottomCommand.Execute(para);
        }
        private void AlignLeftExecuted(object para)
        {           
            PageViewModel?.DiagramViewModel?.AlignLeftCommand.Execute(para);
        }
        private void AlignHorizontalCentersExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.AlignHorizontalCentersCommand.Execute(para);
        }
        private void AlignRightExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.AlignRightCommand.Execute(para);
        }
        private void BringForwardExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.BringForwardCommand.Execute(para);
        }
        private void BringToFrontExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.BringToFrontCommand.Execute(para);
        }
        private void SendBackwardExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.SendBackwardCommand.Execute(para);
        }
        private void SendToBackExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.SendBackwardCommand.Execute(para);
        }
        private void DistributeHorizontalExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.DistributeHorizontalCommand.Execute(para);
        }
        private void DistributeVerticalExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.DistributeVerticalCommand.Execute(para);
        }
        private void SelectAllExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.SelectAllCommand.Execute(para);
        }
        #endregion

        private void CenterMoveExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.CenterMoveCommand.Execute(para);
        }

        private void LeftMoveExecuted(object para = null)
        {
            PageViewModel?.DiagramViewModel?.LeftMoveCommand.Execute(para);
        }

        private void RightMoveExecuted(object para = null)
        {
            PageViewModel?.DiagramViewModel?.RightMoveCommand.Execute(para);
        }

        private void UpMoveExecuted(object para = null)
        {
            PageViewModel?.DiagramViewModel?.UpMoveCommand.Execute(para);
        }

        private void DownMoveExecuted(object para = null)
        {
            PageViewModel?.DiagramViewModel?.DownMoveCommand.Execute(para);
        }

        private void SameWidthExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.SameWidthCommand.Execute(para);
        }

        private void SameHeightExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.SameHeightCommand.Execute(para);
        }

        private void SameSizeExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.SameSizeCommand.Execute(para);
        }

        private void SameAngleExecuted(object para)
        {
            PageViewModel?.DiagramViewModel?.SameAngleCommand.Execute(para);
        }

        private void LockExecuted(object para)
        {
            LockObjectViewModel.LockObject[0].IsChecked = true;
        }

        private void UnlockExecuted(object para)
        {
            LockObjectViewModel.LockObject.ForEach(p => p.IsChecked = false);
        }

        private void AddPageExecuted(object para)
        {
            PageViewModel?.AddPageExecuted(para);
        }

        private void AddCopyPageExecuted(object para)
        {
            PageViewModel?.AddCopyPageExecuted(para);
        }

        private void RenamePageExecuted(object para)
        {
            PageViewModel?.RenamePageExecuted(para);
        }

        private void EndRenamePageExecuted(object para)
        {
            PageViewModel?.EndRenamePageExecuted(para);
        }

        private void DeletePageExecuted(object para)
        {
            PageViewModel?.DeletePageExecuted(para);
        }

        private void AddImageExecuted(object para)
        {
            PageViewModel?.AddImageExecuted(para);
        }

        private void EditImageExecuted(object para)
        {
            PageViewModel?.EditImageExecuted(PageViewModel.DiagramViewModel.SelectedItems?.FirstOrDefault());
        }

        private void ResizeImageExecuted(object para)
        {
            PageViewModel?.ResizeImageExecuted(PageViewModel.DiagramViewModel.SelectedItems?.FirstOrDefault());
        }

        private void ResetImageExecuted(object para)
        {
            PageViewModel?.ResetImageExecuted(PageViewModel.DiagramViewModel.SelectedItems?.FirstOrDefault());
        }

        private void AddVideoExectued(object para)
        {
            PageViewModel?.AddVideoExecuted(para);
        }

        private void AddOutLineTextExecuted(object para)
        {
            PageViewModel?.AddOutLineTextExecuted(para);
        }

        private void AddBarcodeExecuted(object para)
        {
            PageViewModel?.AddBarcodeExecuted(para);
        }

        private void SelectedColorExecuted(object para)
        {
            if (para == null) return;

            switch (ColorType)
            {
                case Models.ColorType.Text: PageViewModel?.SetFont(new FontViewModel() { FontColor = (Color)para }, "FontColor"); break;
                case Models.ColorType.Fill: PageViewModel?.SetColor(new ColorViewModel() { FillColor = new ColorObject() { Color = (Color)para } }, "FillColor"); break;
                case Models.ColorType.Line: PageViewModel?.SetColor(new ColorViewModel() { LineColor = new ColorObject() { Color = (Color)para } }, "LineColor"); break;
            }
        }

        private void AboutExecuted()
        {
            AboutWindow aboutWindow = new AboutWindow();
            aboutWindow.ShowDialog();
        }

        #region 方法
        private Color[] GenerateStandardGradients()
        {
            var count = ColorGallery.StandardThemeColors.Length;
            List<Color> result = new List<Color>();
            for (var i = 0; i < count; i++)
            {
                //var colors = ColorGallery.GetGradient(ColorGallery.StandardThemeColors[i], 10);
                //for (var j = 9; j >= 0; j--)
                //{
                //    result.Add(colors[j]);
                //}
            }
            {
                //var colors = ColorGallery.GetGradient(Colors.Black, 10);
                //for (var j = 9; j >= 0; j--)
                //{
                //    result.Add(colors[j]);
                //}
            }
            return result.ToArray();
        }
        #endregion
    }
}
