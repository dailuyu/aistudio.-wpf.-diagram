﻿using AIStudio.Wpf.DiagramApp.Models;
using AIStudio.Wpf.DiagramApp.ViewModels;
using AIStudio.Wpf.Flowchart.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner;

namespace AIStudio.Wpf.Flowchart
{
    public class FlowchartViewModel : PageViewModel
    {
        public FlowchartViewModel(string title, string status, DiagramType diagramType) : base(title, status, diagramType)
        {
            
        }
        public FlowchartViewModel(string filename, DiagramDocument diagramDocument) : base(filename, diagramDocument)
        {          
            if (DiagramViewModel != null)
            {
                FlowchartService.InitData(DiagramViewModel.Items.OfType<FlowNode>().ToList(), DiagramViewModel.Items.OfType<ConnectionViewModel>().ToList(), DiagramViewModel);
            }
            _service.DrawModeViewModel.LineDrawMode = DrawMode.ConnectingLineSmooth;
        }

        protected override void InitDiagramViewModel()
        {
            base.InitDiagramViewModel();

            DiagramViewModel.ShowGrid = true;
            DiagramViewModel.GridCellSize = new Size(100, 100);
            DiagramViewModel.CellHorizontalAlignment = CellHorizontalAlignment.Center;
            DiagramViewModel.CellVerticalAlignment = CellVerticalAlignment.Center;
            _service.DrawModeViewModel.LineDrawMode = DrawMode.ConnectingLineSmooth;
        }

        protected override void Init()
        {
            base.Init();

            DesignerItemViewModelBase start = new StartFlowNode() { Left = 100, Top = 0, ItemWidth = 80, ItemHeight = 40, Color = Colors.Yellow.ToString() };
            DiagramViewModel.DirectAddItemCommand.Execute(start);

            DesignerItemViewModelBase middle1 = new MiddleFlowNode() { Left = 100, Top = 100, ItemWidth = 80, ItemHeight = 40, Color = Colors.Yellow.ToString(), Text = "主管审批", UserIds= new List<string> { "操作员1", "操作员2" }, ActType = "or" };
            DiagramViewModel.DirectAddItemCommand.Execute(middle1);

            DesignerItemViewModelBase decide = new DecideFlowNode() { Left = 100, Top = 200, ItemWidth = 80, ItemHeight = 40, Color = Colors.Yellow.ToString(), Text = "5" };
            DiagramViewModel.DirectAddItemCommand.Execute(decide);

            DesignerItemViewModelBase middle2 = new MiddleFlowNode() { Left = 200, Top = 300, ItemWidth = 80, ItemHeight = 40, Color = Colors.Yellow.ToString(), Text = "分管领导", UserIds = new List<string> { "操作员1", "操作员2" }, ActType = "and", DoubleApprove = true };
            DiagramViewModel.DirectAddItemCommand.Execute(middle2);

            DesignerItemViewModelBase cobegin = new COBeginFlowNode() { Left = 100, Top = 400, ItemWidth = 80, ItemHeight = 40, Color = Colors.Yellow.ToString() };
            DiagramViewModel.DirectAddItemCommand.Execute(cobegin);

            DesignerItemViewModelBase middle3 = new MiddleFlowNode() { Left = 100, Top = 500, ItemWidth = 80, ItemHeight = 40, Color = Colors.Yellow.ToString(), Text = "财务审批", UserIds = new List<string> { "Admin" }, ActType = "or", DoubleApprove = true };
            DiagramViewModel.DirectAddItemCommand.Execute(middle3);

            DesignerItemViewModelBase middle4 = new MiddleFlowNode() { Left = 200, Top = 500, ItemWidth = 80, ItemHeight = 40, Color = Colors.Yellow.ToString(), Text = "人力审批", RoleIds = new List<string> { "操作员", "管理员" }, ActType = "or", DoubleApprove = true };
            DiagramViewModel.DirectAddItemCommand.Execute(middle4);

            DesignerItemViewModelBase coend = new COEndFlowNode() { Left = 100, Top = 600, ItemWidth = 80, ItemHeight = 40, Color = Colors.Yellow.ToString() };
            DiagramViewModel.DirectAddItemCommand.Execute(coend);

            DesignerItemViewModelBase end = new EndFlowNode() { Left = 100, Top = 700, ItemWidth = 80, ItemHeight = 40, Color = Colors.Yellow.ToString() };
            DiagramViewModel.DirectAddItemCommand.Execute(end);

            ConnectionViewModel connector1 = new ConnectionViewModel(start.BottomConnector, middle1.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);     
            DiagramViewModel.DirectAddItemCommand.Execute(connector1);

            ConnectionViewModel connector2 = new ConnectionViewModel(middle1.BottomConnector, decide.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.DirectAddItemCommand.Execute(connector2);

            ConnectionViewModel connector3 = new ConnectionViewModel(decide.RightConnector, middle2.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.DirectAddItemCommand.Execute(connector3);
            connector3.AddLabel(">=3");

            ConnectionViewModel connector4 = new ConnectionViewModel(middle2.BottomConnector, cobegin.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.DirectAddItemCommand.Execute(connector4);

            ConnectionViewModel connector5 = new ConnectionViewModel(decide.BottomConnector, cobegin.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.DirectAddItemCommand.Execute(connector5);
            connector5.AddLabel("<3");

            ConnectionViewModel connector6 = new ConnectionViewModel(cobegin.BottomConnector, middle3.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.DirectAddItemCommand.Execute(connector6);

            ConnectionViewModel connector7 = new ConnectionViewModel(cobegin.BottomConnector, middle4.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.DirectAddItemCommand.Execute(connector7);

            ConnectionViewModel connector8 = new ConnectionViewModel(middle3.BottomConnector, coend.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.DirectAddItemCommand.Execute(connector8);

            ConnectionViewModel connector9 = new ConnectionViewModel(middle4.BottomConnector, coend.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.DirectAddItemCommand.Execute(connector9);

            ConnectionViewModel connector10 = new ConnectionViewModel(coend.BottomConnector, end.TopConnector, _service.DrawModeViewModel.LineDrawMode, _service.DrawModeViewModel.LineRouterMode);
            DiagramViewModel.DirectAddItemCommand.Execute(connector10);

            DiagramViewModel.ClearSelectedItemsCommand.Execute(null);

            FlowchartService.InitData(DiagramViewModel.Items.OfType<FlowNode>().ToList(), DiagramViewModel.Items.OfType<ConnectionViewModel>().ToList(), DiagramViewModel);
        }

        public override void Dispose()
        {
            base.Dispose();

            foreach (var viewModel in DiagramViewModels)
            {
                FlowchartService.DisposeData(viewModel);
            }
        }
    }
}
